CONFIG += c++11

QT += quick multimedia concurrent widgets qml network

CONFIG += c++latest openssl-linked resources_big

TEMPLATE = app
TARGET = 'ATEN Stream to USB Capture'
VERSION = 1.0.071
RC_ICONS = src/images/icon_256.ico
QMAKE_TARGET_COMPANY = 'ATEN International Co., Ltd.'
QMAKE_TARGET_DESCRIPTION = $${TARGET}
QMAKE_TARGET_COPYRIGHT = '(c) ATEN International Co., Ltd. All rights reserved.'
QMAKE_TARGET_PRODUCT = $${TARGET}

DEFINES += APP_NAME=\"\\\"$${TARGET}\\\"\"
DEFINES += APP_VERSION=\"\\\"$${VERSION}\\\"\"

DEFINES += DEBUG_LOG_AUDIO_PLAYER=0
DEFINES += DEBUG_LOG_VCAM_WRITER=0
DEFINES += DEBUG_LOG_PACKET_DECODER=0
SOURCES += \
        main.cpp \
        src/core/i18n.cpp \
        src/core/Restful/httpapibase.cpp \
        src/core/Restful/httprequest.cpp \
        src/core/System/clipboard.cpp \
        src/core/System/platformcontrol.cpp \
        src/core/System/systemtrayicon.cpp

RESOURCES += qml.qrc \
    other_resorce.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    src/core/i18n.h \
    src/core/Restful/httpapibase.h \
    src/core/Restful/httprequest.h \
    src/core/System/clipboard.h \
    src/core/System/platformcontrol.h \
    src/core/System/systemtrayicon.h

