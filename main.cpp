#include "./src/core/Restful/httpapibase.h"
#include "./src/core/Restful/httprequest.h"
#include "./src/core/System/clipboard.h"
#include "./src/core/System/platformcontrol.h"
#include "./src/core/System/systemtrayicon.h"
#include "./src/core/i18n.h"
#include <QApplication>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSharedMemory>

void myLogHandler(QtMsgType type, const QMessageLogContext&, const QString& msg);
void registerSystemTrayIcon();
void registerOverlayInfo();
void initApplicationSetting(QApplication& app);
void setTranlaste(QApplication& app);
//void registerVcamMain(QQmlApplicationEngine& engine, VcamMain& vcam);
void registerHTTPRequest();
void registerClipboard();
void registerPlatfromControl(PlatformControl& platformControl, QQmlApplicationEngine& engine, int argc, char* argv[]);

int main(int argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    qInstallMessageHandler(myLogHandler);
    app.setQuitOnLastWindowClosed(false);

    QSharedMemory shared(QString("check-") + APP_NAME);
    if (!shared.create(1, QSharedMemory::ReadOnly))
        return -1;

    QQmlApplicationEngine engine;
    //    VcamMain vcam;
    PlatformControl platformControl = PlatformControl(APP_NAME);

    initApplicationSetting(app);
    setTranlaste(app);
    registerSystemTrayIcon();
    //    registerOverlayInfo();
    //    registerVcamMain(engine, vcam);
    registerHTTPRequest();
    registerClipboard();
    registerPlatfromControl(platformControl, engine, argc, argv);

    //    NginxManager nginxMgr;
    //    nginxMgr.start();

    engine.rootContext()->setContextProperty("ExePath",
        QDir::toNativeSeparators(QCoreApplication::applicationDirPath()));
    engine.rootContext()->setContextProperty("CamName", "ATEN stream to USB video device");

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

void myLogHandler(QtMsgType type, const QMessageLogContext&, const QString& msg)
{
    const QByteArray localMsg = msg.toUtf8();
    switch (type) {
    case QtDebugMsg: //for qml
        fprintf(stdout, "[Debug] %s\n", localMsg.constData());
        fflush(stdout);
        break;
    case QtInfoMsg:
        fprintf(stderr, "[Info] %s\n", localMsg.constData());
        fflush(stderr);
        break;
    case QtWarningMsg:
        fprintf(stderr, "[Warning] %s\n", localMsg.constData());
        fflush(stderr);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "[Critical] %s\n", localMsg.constData());
        fflush(stderr);
        break;
    case QtFatalMsg:
        fprintf(stderr, "[Fatal] %s\n", localMsg.constData());
        fflush(stderr);
        break;
    }
}

// 設定Window APP的資訊
void initApplicationSetting(QApplication& app)
{
    app.setApplicationName(APP_NAME);
    app.setOrganizationName("ATEN");
    app.setOrganizationDomain("https://www.aten.com");
    app.setApplicationVersion(APP_VERSION);
    app.setWindowIcon(QIcon(":/images/icon_32.png"));
}

// 用這個來做測試多國語言
//de_DE
//en_US
//es_ES
//fr_FR
//it_IT
//ja_JP
//ko_KR
//pl_PL
//pt_BR
//ru_RU
//tr_TR
//zh_CN
//zh_TW
void setTranlaste(QApplication& app)
{
    I18n* i18n = new I18n();
    i18n->load(QLocale::system().name());
    app.installTranslator(&i18n->getTranslator());
}

// 註冊SystemTrayIcon給QML使用
void registerSystemTrayIcon()
{
    qmlRegisterType<MyMenu>("an.utility", 1, 0, "MyMenu"); //注册到qml中
    qmlRegisterType<MyAction>("an.utility", 1, 0, "MyAction");
    qmlRegisterType<MySeparator>("an.utility", 1, 0, "MySeparator");
    qmlRegisterType<SystemTrayIcon>("an.utility", 1, 0, "SystemTrayIcon");
}

//// 註冊OverlayInfo給QML使用
//void registerOverlayInfo()
//{
//    qmlRegisterInterface<OverlayInfo>("OverlayInfo", 1);
//}

//// 註冊Vcam給QML使用
//void registerVcamMain(QQmlApplicationEngine& engine, VcamMain& vcam)
//{
//    qmlRegisterUncreatableType<VcamMain>("VcamMain.CamState", 1, 0, "CamState", "Not creatable as it is an enum type.");
//    qmlRegisterUncreatableType<VcamMain>("VcamMain.CamErrorCode", 1, 0, "CamErrorCode", "Not creatable as it is an enum type.");
//    engine.rootContext()->setContextProperty("Vcam", &vcam);
//}

void registerHTTPRequest()
{
    qmlRegisterType<HttpRequest>("aten.com.HttpRequest", 1, 0, "HttpRequest");
}

void registerClipboard()
{
    qmlRegisterType<Clipboard>("Clipboard", 1, 0, "Clipboard");
}

// 註冊PlatfromControl給QML使用
void registerPlatfromControl(PlatformControl& platformControl, QQmlApplicationEngine& engine, int argc, char* argv[])
{
    if (argc > 1 && (argv[1] == QString("autoStart"))) { //注意tr函数中没有空格
        platformControl.setShowWindow(false);
    } else {
        platformControl.setShowWindow(true);
    }
    qmlRegisterUncreatableType<PlatformControl>("PlatformControlModule", 1, 0, "Platform", "");
    engine.rootContext()->setContextProperty("PlatformControl", &platformControl);
}
