import QtQuick 2.14;
import QtQuick.Window 2.14;
import QtQuick.Controls 2.14;
import QtGraphicalEffects 1.15
import Qt.labs.settings 1.0;
import an.utility 1.0;
import "./src/themes/home-page/" as Home;
import "./src/models/" as Models;
import "./src/utils/";
import "./src/utils/QMLObject.js" as QMLObject;

import "./src/models/DeviceDataTest.js" as DeviceDataTest;
import "./src/constants/"

ApplicationWindow {
    id: win;
    width: WindowConfig.winWidth;
    height: WindowConfig.winHeigh;
    visible: true;
    flags: Qt.Window |
           Qt.FramelessWindowHint |
           Qt.WindowMinimizeButtonHint;
    color: "transparent";
    property int previousX;
    property int previousY;
    property var deviceDatas: [];

    Models.RootModelManager {
        id: rootModelManager;
    }


    Rectangle {
        id: winRoot;
        anchors.fill: parent;
        color: "transparent";
        radius: 5;
        layer.enabled: true;
        layer.effect: OpacityMask {
            maskSource: Rectangle {
                width: winRoot.width;
                height: winRoot.height;
                radius: winRoot.radius;
            }
        }

        Rectangle {
            id: borderRect;
            z: 2;
            anchors.fill: parent;
            border.width: 1;
            border.color: "#292929";
            color: "transparent";
            radius: parent.radius;
        }

        StackView {
            id: stackView;
            initialItem: mainView;
            anchors.fill: parent;
        }

        Home.Index {
            id: mainView;
//            anchors.fill: parent;
        }
    }

    SystemTrayIcon {
        id: systemTray
        icon: "qrc:/src/images/icon_32.png"
        visible: true
        menu: menu
        toolTip: Language.get(Lang.Tag.APP_FULL_NAME);

        // 顯示nitification的提示功能
        millisecondsTimeoutHint: 1000; // 使用後沒什麼效果
        notificationTitle: Language.get(Lang.Tag.SYSTEM_TRAY_NOTIFICATION_TITLE);
        notificationMessage: Language.get(Lang.Tag.SYSTEM_TRAY_NOTIFICATION_MESSAGE);

        onTrigger: {
            console.log("onTrigger...");
            win.requestActivate();
            win.show();
        }

        MyMenu {
            id: menu

            MyAction {
                text: Language.get(Lang.Tag.SYSTEM_TRAY_ICON_VERSION) +". " + Qt.application.version;
                font.family: Fonts.regular
                font.pixelSize: 14
                enabled: false
            }

            MyAction {
                text: Language.get(Lang.Tag.SYSTEM_TRAY_ICON_QUIT);
                font.family: Fonts.regular;
                font.pixelSize: 14;

                onTriggered: {
                    console.log("system tray - Quit");
//                    WindowControl.isCloseWindow = true;
                    Qt.quit();
                }
            }
        }

        Component.onCompleted: {
//            WindowControl.windowSystemTrayIcon = systemTray;
        }
    }

    Component.onCompleted: {
        console.log("WindowConfig.winWidth: "+ WindowConfig.winWidth);

//        const points = [40, 100, 1, 5, 25, 10];
//        points.sort(function(a, b){return a - b});
//        console.log("points: "+points);
    }

    onWidthChanged: {
        width = WindowConfig.winWidth;
    }

    onHeightChanged: {
        height = WindowConfig.winHeight;
    }
}
