#include "HTTPAPIBase.h"
#include <QDebug>
#include <QSslConfiguration>

HTTPAPIBase::HTTPAPIBase(QObject* parent)
    : QObject(parent)
{
    setKnownHeaderName(KnownHeaders::ContentType, "Content-Type");
    setKnownHeaderName(KnownHeaders::Accept, "Accept");
    setKnownHeaderName(KnownHeaders::Authorization, "Authorization");

    connect(&m_nam, &QNetworkAccessManager::finished, this, &HTTPAPIBase::replyFinished);
}

HTTPAPIBase::~HTTPAPIBase()
{
}

QNetworkRequest HTTPAPIBase::createRequest(const QUrl& url) const
{
    QSslConfiguration config = QSslConfiguration::defaultConfiguration();
    config.setProtocol(QSsl::TlsV1_2);
    config.setPeerVerifyMode(QSslSocket::VerifyNone);

    QNetworkRequest request = QNetworkRequest(url);
    request.setSslConfiguration(config);
    return request;
}

void HTTPAPIBase::replyFinished(QNetworkReply* reply)
{

    if (reply->error() != QNetworkReply::NoError) {
        qInfo() << "HTTPAPIBase::replyFinished " << reply->url() << " " << reply->error() << reply->errorString();
        emit replyError(reply, reply->error(), reply->errorString());
    }
}

void HTTPAPIBase::handleReplyError(QNetworkReply::NetworkError error)
{
    qInfo() << "HTTPAPIBase::handleReplyError" << error;
}

void HTTPAPIBase::handleSslErrors(QList<QSslError> errors)
{
    qInfo() << "HTTPAPIBase::handleSslErrors" << errors;
}

void HTTPAPIBase::setExtraHeaderValue(QByteArray header, QByteArray token)
{
    m_extraHeadersValues[header] = token;
}

void HTTPAPIBase::setRequestHeaders(QNetworkRequest* request)
{
    QMapIterator<KnownHeaders, QByteArray> it(m_knownHeadersValues);
    while (it.hasNext()) {
        it.next();
        request->setRawHeader(m_knownHeadersNames.value(it.key()), it.value());
    }

    QMapIterator<QByteArray, QByteArray> jt(m_extraHeadersValues);
    while (jt.hasNext()) {
        jt.next();
        request->setRawHeader(jt.key(), jt.value());
    }
}

void HTTPAPIBase::
    connectReplyToErrors(QNetworkReply* reply)
{
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(handleReplyError(QNetworkReply::NetworkError)));
    connect(reply, &QNetworkReply::sslErrors, this, &HTTPAPIBase::handleSslErrors);
}

bool HTTPAPIBase::checkReplyIsError(QNetworkReply* reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qInfo() << reply->rawHeaderList();
        qInfo() << reply->bytesAvailable() << reply->errorString();
        return true;
    } else {
        return false;
    }
}

QByteArray HTTPAPIBase::baseUrl() const
{
    return m_baseUrl;
}

QByteArray HTTPAPIBase::knownHeaderValue(HTTPAPIBase::KnownHeaders code)
{
    if (!m_knownHeadersValues.contains(code))
        return QByteArray();

    return m_knownHeadersValues[code];
}

void HTTPAPIBase::removeKnownHeaderValue(HTTPAPIBase::KnownHeaders code)
{
    m_knownHeadersValues.remove(code);
}

void HTTPAPIBase::removeExtraHeaderValue(QByteArray header)
{
    m_extraHeadersValues.remove(header);
}

void HTTPAPIBase::setBaseUrl(QByteArray baseUrl)
{
    if (m_baseUrl == baseUrl)
        return;

    m_baseUrl = baseUrl;
    emit baseUrlChanged(baseUrl);
}

void HTTPAPIBase::setKnownHeaderName(HTTPAPIBase::KnownHeaders code, QByteArray name)
{
    m_knownHeadersNames.insert(code, name);
}

void HTTPAPIBase::setKnownHeaderValue(HTTPAPIBase::KnownHeaders code, QByteArray value)
{
    m_knownHeadersValues.insert(code, value);
}

QNetworkReply* HTTPAPIBase::get(QUrl url)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.get(request);

    //    /*阻塞loop请求:同步处理*/
    //    QEventLoop eventLoop;
    //    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    //    eventLoop.exec(QEventLoop::ExcludeUserInputEvents);

    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::post(QUrl url)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.sendCustomRequest(request, "POST");

    //    /*阻塞loop请求:同步处理*/
    //    QEventLoop eventLoop;
    //    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    //    eventLoop.exec(QEventLoop::ExcludeUserInputEvents);

    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::post(QUrl url, QIODevice* data)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.post(request, data);

    //    /*阻塞loop请求:同步处理*/
    //    QEventLoop eventLoop;
    //    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    //    eventLoop.exec(QEventLoop::ExcludeUserInputEvents);

    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::post(QUrl url, const QByteArray& data)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.post(request, data);
    qDebug() << "post1........ C+++";
    //    /*阻塞loop请求:同步处理*/
    //    QEventLoop eventLoop;
    //    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    //    eventLoop.exec(QEventLoop::ExcludeUserInputEvents);

    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::post(QUrl url, QHttpMultiPart* multiPart)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.post(request, multiPart);
    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::put(QUrl url)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.sendCustomRequest(request, "PUT");
    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::put(QUrl url, QIODevice* data)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.put(request, data);

    //    /*阻塞loop请求:同步处理*/
    //    QEventLoop eventLoop;
    //    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    //    eventLoop.exec(QEventLoop::ExcludeUserInputEvents);

    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::put(QUrl url, const QByteArray& data)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.put(request, data);
    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::put(QUrl url, QHttpMultiPart* multiPart)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.put(request, multiPart);
    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::patch(QUrl url)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.sendCustomRequest(request, "PATCH");
    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::patch(QUrl url, QIODevice* data)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.sendCustomRequest(request, "PATCH", data);

    //    /*阻塞loop请求:同步处理*/
    //    QEventLoop eventLoop;
    //    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    //    eventLoop.exec(QEventLoop::ExcludeUserInputEvents);

    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::patch(QUrl url, const QByteArray& data)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.sendCustomRequest(request, "PATCH", data);

    //    /*阻塞loop请求:同步处理*/
    //    QEventLoop eventLoop;
    //    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    //    eventLoop.exec(QEventLoop::ExcludeUserInputEvents);

    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::patch(QUrl url, QHttpMultiPart* multiPart)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.sendCustomRequest(request, "PATCH", multiPart);
    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::deleteResource(QUrl url)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.deleteResource(request);
    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::head(QUrl url)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.head(request);
    connectReplyToErrors(reply);
    return reply;
}

QNetworkReply* HTTPAPIBase::options(QUrl url)
{
    QNetworkRequest request = createRequest(url);
    setRequestHeaders(&request);

    QNetworkReply* reply = m_nam.sendCustomRequest(request, "OPTIONS");
    connectReplyToErrors(reply);
    return reply;
}
