#include "httprequest.h"
#include <QDebug>
#include <QQmlEngine>

HttpRequest::HttpRequest(HTTPAPIBase* parent)
    : HTTPAPIBase(parent)
{
    // 設定Timer out為3秒
    //    m_nam.setTransferTimeout(3000);
    connect(&m_nam, &QNetworkAccessManager::finished, this, &HttpRequest::replyRequestFinished);
}

void HttpRequest::removeHeader(QByteArray header)
{
    removeExtraHeaderValue(header);
}

void HttpRequest::replyRequestFinished(QNetworkReply* reply)
{
    auto engine = qjsEngine(this);
    QJSValueList response;

    // timeout的時候，因不從HTTP server接收的值是，所以會是空代表轉為整數status code就為0
    QVariant status_code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);

    if (reply->error() != QNetworkReply::NoError) {
        // response << 加入進去裡面
        response << engine->toScriptValue(reply->errorString());
        response << engine->toScriptValue(status_code.toInt());
        this->failureCallback.call(response);
    } else {
        QString data = reply->readAll();
        response << engine->toScriptValue(data);
        response << engine->toScriptValue(status_code.toInt());
        this->successjsCallback.call(response);
    }
}

void HttpRequest::get(QUrl url, QJSValue successjsCallback, QJSValue failureCallback)
{
    this->successjsCallback = successjsCallback;
    this->failureCallback = failureCallback;
    HTTPAPIBase::get(url);
}

void HttpRequest::post(QUrl url, QJSValue successjsCallback, QJSValue failureCallback)
{
    this->successjsCallback = successjsCallback;
    this->failureCallback = failureCallback;
    HTTPAPIBase::post(url);
}

void HttpRequest::post(QUrl url, const QByteArray& data, QJSValue successjsCallback, QJSValue failureCallback)
{
    this->successjsCallback = successjsCallback;
    this->failureCallback = failureCallback;

    qDebug() << "data: " << data;
    HTTPAPIBase::post(url, data);
}

void HttpRequest::patch(QUrl url, const QByteArray& data, QJSValue successjsCallback, QJSValue failureCallback)
{
    this->successjsCallback = successjsCallback;
    this->failureCallback = failureCallback;
    HTTPAPIBase::patch(url, data);
}
