#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H

#include "httpapibase.h"
#include <QDebug>
#include <QNetworkConfigurationManager>
#include <QString>
#include <QtNetwork/QtNetwork>
#include <QtQml>

class QQmlEngine;
class QJSEngine;

class HttpRequest : public HTTPAPIBase {
    Q_OBJECT
    //    Q_DISABLE_COPY(HttpRequest)

public:
    explicit HttpRequest(HTTPAPIBase* parent = nullptr);
    QJSValue successjsCallback;
    QJSValue failureCallback;

public:
public slots:
    void replyRequestFinished(QNetworkReply* reply);

public:
    Q_INVOKABLE void get(QUrl url, QJSValue successjsCallback, QJSValue failureCallback);
    Q_INVOKABLE void post(QUrl url, QJSValue successjsCallback, QJSValue failureCallback);
    Q_INVOKABLE void post(QUrl url, const QByteArray& data, QJSValue successjsCallback, QJSValue failureCallback);
    Q_INVOKABLE void patch(QUrl url, const QByteArray& data, QJSValue successjsCallback, QJSValue failureCallback);

    Q_INVOKABLE void removeHeader(QByteArray header);

signals:
    void respone(QString reply);

private:
};

#endif // HTTPREQUEST_H
