#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include <QClipboard>
#include <QGuiApplication>
#include <QObject>

class Clipboard : public QObject {
    Q_OBJECT
public:
    explicit Clipboard(QObject* parent = nullptr);
    Q_INVOKABLE void setText(QString text);

private:
    QClipboard* clipboard;
};

#endif // CLIPBOARD_H
