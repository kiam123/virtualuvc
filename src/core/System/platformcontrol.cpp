#include "platformcontrol.h"
#include <QApplication>
#include <QCoreApplication>
#include <QDir>
#include <QOperatingSystemVersion>
#include <QSettings>

PlatformControl::PlatformControl(QString appName)
{
    mAppName = appName;
}

bool PlatformControl::autoStart() const
{
    return mIsAutoStart;
}

void PlatformControl::setAutoStart(bool isAutoStart)
{
    QSettings reg("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);

    mIsAutoStart = isAutoStart;
    if (getPlatForm() == "Windows") {
        if (isAutoStart) {
            QString strAppPath = QDir::toNativeSeparators(QCoreApplication::applicationFilePath());
            reg.setValue(mAppName, strAppPath + " autoStart");
        } else {
            reg.remove(mAppName);
        }
    } else if (getPlatForm() == "IOS") {
        // TODO 目前無支援
    }
}

bool PlatformControl::isShowWindow() const
{
    return mIsShowWindow;
}

void PlatformControl::setShowWindow(bool showWindow)
{
    mIsShowWindow = showWindow;
}

QString PlatformControl::getPlatForm()
{
    if (QOperatingSystemVersion::Windows == QOperatingSystemVersion::currentType()) {
        return "Windows";
    } else if (QOperatingSystemVersion::IOS == QOperatingSystemVersion::currentType()) {
        return "IOS";
    }
    return "";
}
