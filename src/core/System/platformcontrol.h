#ifndef PLATFORMCONTROL_H
#define PLATFORMCONTROL_H

#include <QObject>

class PlatformControl : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString platform READ getPlatForm);
    Q_PROPERTY(bool isShowWindow READ isShowWindow WRITE setShowWindow);
    Q_PROPERTY(bool autoStart READ autoStart WRITE setAutoStart NOTIFY autoStartChanged)

public:
    explicit PlatformControl(QString appName);

    bool autoStart() const;
    bool isShowWindow() const;
    QString getPlatForm();

signals:
    void autoStartChanged();

public slots:
    void setAutoStart(bool isAutoStart);
    void setShowWindow(bool isShowWindow);

private:
    QString mAppName;
    QString mPlatform;
    bool mIsAutoStart;
    bool mIsShowWindow;
};

#endif // PLATFORMCONTROL_H
