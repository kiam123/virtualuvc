#include "i18n.h"
#include <QDebug>

I18n::I18n()
{
    language = "en";
    translator.load(":/translations/" + language + ".qm");
}

bool I18n::load(const QString& defaultLang)
{
    QString lang = defaultLang.split("_")[0];
    QString country = defaultLang.split("_")[1];

    return languageMatch(lang, country);
}

bool I18n::languageMatch(const QString& lang, const QString& country)
{
    if (lang == "zh" && country == "TW") {
        language = "zh_TW";
    } else if (lang == "zh" && country == "CN") {
        language = "zh_CN";
    } else if (lang == "ja") {
        language = "ja";
    } else if (lang == "de") {
        language = "de";
    } else if (lang == "ko") {
        language = "ko";
    } else if (lang == "ru") {
        language = "ru";
    } else if (lang == "fr") {
        language = "fr";
    } else if (lang == "es") {
        language = "es";
    } else if (lang == "pt") {
        language = "pt";
    } else if (lang == "it") {
        language = "it";
    } else if (lang == "tr") {
        language = "tr";
    } else if (lang == "pl") {
        language = "pl";
    }

    return translator.load(":/translations/" + language + ".qm");
}

QTranslator& I18n::getTranslator()
{
    return translator;
}

QString I18n::getLanguage()
{
    qInfo() << "translator.language(): " << language;
    return language;
}
