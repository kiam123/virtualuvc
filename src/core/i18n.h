#ifndef I18N_H
#define I18N_H

#include <QObject>
#include <QTranslator>
class I18n : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString language READ getLanguage);

public:
    explicit I18n();

    bool load(const QString& defaultLang);
    bool languageMatch(const QString& lang, const QString& country);
    QString getLanguage();
    QTranslator& getTranslator();
    QTranslator translator;
    QString language;

protected:
};

#endif // I18N_H
