import QtQuick 2.0

QtObject {
    property int id;
    property string deviceName: "";
    property string modelName: "";
    property string mIPV4Address: "";
}
