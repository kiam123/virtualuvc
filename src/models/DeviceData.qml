import QtQuick 2.12

DeviceBase {
    property int id;
    property string deviceName: "";
    property string modelName: "";
    property string userName: "administrator";
    property string password: "";
    property string streamKey: "";
    property string mIPV4Address: "";
    property string networkInterfaceName: "ethernet_32769";
    property string networkInterfaceHumanReadableName: "乙太網路";
    property string networkInterfaceIPV4Address: "10.3.56.123";
    property string networkInterfaceMacAddress: "1C:1B:0D:7F:59:6D";

//    property bool isItemHover: false;
//    property bool isPlaying: false;
//    property bool isPreviewHover: false;
//    property bool isDeleteHover: false;

    function setObejct(deviceObject) {
        this.id = deviceObject.id;
        this.deviceName = deviceObject.deviceName;
        this.modelName = deviceObject.modelName;
        this.userName = deviceObject.userName;
        this.password = deviceObject.password;
        this.streamKey = deviceObject.streamKey;
        this.mIPV4Address = deviceObject.IPV4Address;
        this.networkInterfaceName = deviceObject.networkInterfaceName;
        this.networkInterfaceHumanReadableName = deviceObject.networkInterfaceHumanReadableName;
        this.networkInterfaceIPV4Address = deviceObject.networkInterfaceIPV4Address;
        this.networkInterfaceMacAddress = deviceObject.networkInterfaceMacAddress;
    }

    function setAll(deviceName,
                    modelName,
                    userName,
                    password,
                    streamKey,
                    IPV4Address,
                    networkInterfaceName,
                    networkInterfaceHumanReadableName,
                    networkInterfaceIPV4Address,
                    networkInterfaceMacAddress) {
        this.deviceName = deviceName;
        this.modelName = modelName;
        this.userName = userName;
        this.password = password;
        this.streamKey = streamKey;
        this.mIPV4Address = IPV4Address;
        this.networkInterfaceName = networkInterfaceName;
        this.networkInterfaceHumanReadableName = networkInterfaceHumanReadableName;
        this.networkInterfaceIPV4Address = networkInterfaceIPV4Address;
        this.networkInterfaceMacAddress = networkInterfaceMacAddress;
    }
}
