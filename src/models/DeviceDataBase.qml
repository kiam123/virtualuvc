import QtQuick 2.0;
import QtQuick.LocalStorage 2.12;
import "../utils/" as Model;
import "../utils/QMLObject.js" as QMLObject;

// C:\Users\<username>AppData\Local\<program name>\QML\OfflineStorage\Databases
Item {
    property var db: null;
    property var dbName: "AtenDB"
    property var dbVersion: "1.0.0"
    property var dbDescription: "Store Device Infomation"
    property var dbSize: 100000

    function init(dbName = this.dbName,
                  dbVersion = this.dbVersion,
                  dbDescription = this.dbDescription,
                  dbSize = this.dbSize) {

        console.log(
            "db:"               , dbName,
            "dbVersion:"        , dbVersion,
            "dbDescription:"    , dbDescription,
            "dbSize:"           , dbSize
        );

        // initialize the database object
        db = LocalStorage.openDatabaseSync(dbName, dbVersion, dbDescription, dbSize);
        db.transaction(function(tx) {
            var sql = 'CREATE TABLE IF NOT EXISTS device '
                    + '(id INTEGER PRIMARY KEY AUTOINCREMENT, '
                    + 'deviceName TEXT NOT NULL, '
                    + 'IPV4Address TEXT NOT NULL, '
                    + 'userName TEXT NOT NULL, '
                    + 'password TEXT NOT NULL, '
                    + 'streamKey TEXT NOT NULL, '
                    + 'modelName TEXT NOT NULL, '
                    + 'networkInterfaceName TEXT NOT NULL, '
                    + 'networkInterfaceHumanReadableName TEXT NOT NULL,'
                    + 'networkInterfaceIPV4Address TEXT NOT NULL,'
                    + 'networkInterfaceMacAddress TEXT NOT NULL)';

            var ret = tx.executeSql(sql);
            console.log("ret:", ret);
        });
    }

    function insertData(deviceDataItem) {
        return new Promise((resolve, reject)=> {
            db.transaction(function(tx) {
                var sqlAllDevice = "SELECT * FROM device";
                var sqlCountTable = "SELECT * FROM device WHERE IPV4Address = '" + deviceDataItem.mIPV4Address + "'";
                var sql = "";
                var rs = null;
                var isExistSameIPV4Address = false;
                var response;

                try {
                    console.log("return reponse1");
                    rs = tx.executeSql(sqlAllDevice);
                    for(var i = 0; i < rs.rows.length; i++) {
                        console.log("return reponse1-1");
                        var tmpIPV4Address = rs.rows.item(i).IPV4Address;
                        if(Model.NetworkInterface.isSameIPV4Address(tmpIPV4Address, deviceDataItem.mIPV4Address)) {
                            isExistSameIPV4Address = true;
                            console.log("DB exist the ip: ", tmpIPV4Address+" "+deviceDataItem.mIPV4Address);
                        }
                    }
                    console.log("return reponse2");

                    if(isExistSameIPV4Address === false) {
                        console.log("testings");
                        sql = "INSERT INTO device ( deviceName,
                                                    modelName,
                                                    userName,
                                                    password,
                                                    streamKey,
                                                    IPV4Address,
                                                    networkInterfaceName,
                                                    networkInterfaceHumanReadableName,
                                                    networkInterfaceIPV4Address,
                                                    networkInterfaceMacAddress) " +
                                "VALUES (\'"+ deviceDataItem.deviceName                           + "\', " +
                                        "\'"+ deviceDataItem.modelName                            + "\', " +
                                        "\'"+ deviceDataItem.userName                             + "\', " +
                                        "\'"+ deviceDataItem.password                             + "\', " +
                                        "\'"+ deviceDataItem.streamKey                            + "\', " +
                                        "\'"+ deviceDataItem.mIPV4Address                         + "\', " +
                                        "\'"+ deviceDataItem.networkInterfaceName                 + "\', " +
                                        "\'"+ deviceDataItem.networkInterfaceHumanReadableName    + "\', " +
                                        "\'"+ deviceDataItem.networkInterfaceIPV4Address          + "\', " +
                                        "\'"+ deviceDataItem.networkInterfaceMacAddress           + "\' " +
                                        ")";
                        var resultInsert = tx.executeSql(sql);
                        var countTable = tx.executeSql(sqlCountTable);
                        deviceDataItem.id = resultInsert.insertId;

                        console.log("tx2: "+ JSON.stringify(countTable));
                        console.log("SUCCESS INSERT");
                        console.log("countTable: "+ countTable.length);
                        response = {
                            "status": 200,
                            "message": "SUCCESS INSERT"
                        };
                        resolve(response);
                    } else {
                        console.log("throw reponse............");
                        throw {
                            "status": 500,
                            "errorMessage": "Fail INSERT, exist same ip address"
                        };
                    }
                } catch(error) {
                    console.log("error testing: "+ error["errorMessage"]);
                    reject(error);
                }
            });
        });
    }

    // TODO
    function readDatas() {
        return new Promise((resolve, reject)=> {
            // reads and applies data from DB
            db.transaction(function(tx) {
                var response;
                console.log("response.....");
                try {
                    var sql = 'SELECT * FROM device';
                    var deviceDatas = [];
                    var objQML = new QMLObject.QMLObject();
                    var deviceDataItem;
                    var myId;
                    var i, j;

                    console.log("response1.....");
                    var rs = tx.executeSql(sql);
                    console.log("rs: "+ rs.rows.length);
                    for(i = 0; i < rs.rows.length; i++) {
                        console.log("readDatas["+i+"]: " + rs.rows.item(i).deviceName);
                        deviceDataItem = objQML.createQMLObject("qrc:/src/models/DeviceDataItem.qml", this);
                        deviceDataItem.id = rs.rows.item(i).id;
                        deviceDataItem.deviceName = rs.rows.item(i).deviceName;
                        deviceDataItem.modelName = rs.rows.item(i).modelName;
                        deviceDataItem.userName = rs.rows.item(i).userName;
                        deviceDataItem.password = rs.rows.item(i).password;
                        deviceDataItem.streamKey = rs.rows.item(i).streamKey;
                        deviceDataItem.mIPV4Address = rs.rows.item(i).IPV4Address;
                        deviceDataItem.networkInterfaceName = rs.rows.item(i).networkInterfaceName;
                        deviceDataItem.networkInterfaceHumanReadableName = rs.rows.item(i).networkInterfaceHumanReadableName;
                        deviceDataItem.networkInterfaceIPV4Address = rs.rows.item(i).networkInterfaceIPV4Address;
                        deviceDataItem.networkInterfaceMacAddress = rs.rows.item(i).networkInterfaceMacAddress;

                        deviceDatas.push(deviceDataItem);
                    }
                    console.log("readDatas: "+ deviceDatas.length);
                    response = {
                        "status": 200,
                        "deviceDatas": deviceDatas,
                        "message": "SUCCESS INSERT"
                    };
                    resolve(response);
                } catch(error) {
                    response = {
                        "status": 500,
                        "errorMessage": error
                    };
                    reject(response);
                }
                
            });
        });
    }

    // TODO
    function deleteData(deviceDataItem) {
        return new Promise((resolve, reject)=> {
            // del data from DB
            db.transaction(function(tx) {
               console.log("delete Data start");
               console.log("device:", deviceDataItem.deviceName, " IPV4Address:", deviceDataItem.IPV4Address,
                           " userName:", deviceDataItem.userName, " password:", deviceDataItem.password,
                           " streamKey:", deviceDataItem.streamKey, " modelName:", deviceDataItem.modelName,
                           " networkInterfaceName:", deviceDataItem.networkInterfaceName,
                           " networkInterfaceHumanReadableName:", deviceDataItem.networkInterfaceHumanReadableName,
                           " networkInterfaceIPV4Address:", deviceDataItem.networkInterfaceIPV4Address,
                           " networkInterfaceMacAddress:", deviceDataItem.networkInterfaceMacAddress);

               var sql = "DELETE FROM device " +
                         "WHERE id = "         + deviceDataItem.id;
               var response = {
                   "status": 200,
                   "message": "SUCCESS INSERT"
               };

               try {
                   console.log("delete sql: ",sql);

                   var rs = tx.executeSql(sql);
                   console.log("delete Data rs.rowsAffected:", rs.rowsAffected);
                   if(rs.rowsAffected === 0) {
                       throw {
                           "status": 500,
                           "errorMessage": "ERROR INSERT"
                       };
                   }
                   resolve(response);
               } catch(error) {
                   console.log("delete Data error:", JSON.stringify(error));
                   reject(error);
               }
               console.log("delete Data end");
            });
        });
    }

    // TODO
    function updateData(deviceDataItem) {
        return new Promise((resolve, reject)=> {
            // update data from DB
            var id = deviceDataItem.id;
            deviceDataItem.deviceName = sqliteEscape(deviceDataItem.deviceName);
            deviceDataItem.password = sqliteEscape(deviceDataItem.password);

            console.log("updateData function")
            console.log("deviceName: ", deviceDataItem.deviceName, " modelName: ",deviceDataItem.modelName,
                       "streamKey: ", deviceDataItem.streamKey, " IPV4Address: ", deviceDataItem.mIPV4Address,
                       " password: ", deviceDataItem.password, "networkInterfaceName:", deviceDataItem.networkInterfaceName,
                       " networkInterfaceHumanReadableName:", deviceDataItem.networkInterfaceHumanReadableName,
                       " networkInterfaceIPV4Address:", deviceDataItem.networkInterfaceIPV4Address,
                       " networkInterfaceMacAddress:", deviceDataItem.networkInterfaceMacAddress);

            db.transaction(function(tx) {
               var sql = "UPDATE device SET
                          deviceName=\'" + deviceDataItem.deviceName + "\',
                          modelName=\'" + deviceDataItem.modelName + "\',
                          streamKey=\'" + deviceDataItem.streamKey + "\',
                          IPV4Address=\'" + deviceDataItem.mIPV4Address + "\',
                          password=\'" + deviceDataItem.password   + "\',
                          networkInterfaceName=\'" + deviceDataItem.networkInterfaceName + "\',
                          networkInterfaceHumanReadableName=\'" + deviceDataItem.networkInterfaceHumanReadableName + "\',
                          networkInterfaceIPV4Address=\'" + deviceDataItem.networkInterfaceIPV4Address + "\',
                          networkInterfaceMacAddress=\'" + deviceDataItem.networkInterfaceMacAddress + "\'
                          WHERE id=" + id;
               var response = {
                   "status": 200,
                   "message": "SUCCESS UPDATE"
               };

               console.log("sql: "+ sql);
               try {
                   var rs = tx.executeSql(sql);
                   console.log("modify Data rs.rowsAffected:", rs.rowsAffected);
                   if(rs.rowsAffected === 0) {
                       throw {
                           "status": 500,
                           "errorMessage": "UPDATE ERROR"
                       };
                   }
                   resolve(response);
               } catch(error) {
                   var err = error;
                   try {
                       err = JSON.parse(err);
                   } catch (e) {
                       err = {
                           "status": 500,
                           "errorMessage": "UPDATE ERROR " + err
                       }
                   }
                   console.log("error: "+ JSON.stringify(err));
                   reject(err);
               }
            });
        });
    }

    // 因為加入單引號'的時候會和原先的sql語法的單引號'有衝突，所以會導致語法會錯
    function sqliteEscape(keyWord){
        keyWord = keyWord.replace(/'/g, "''");

        return keyWord;
    }

//    function dataLength(count) {
//        db.transaction(function(tx) {
//            var sql = 'SELECT * FROM device';
//            var rs = tx.executeSql(sql);
//            count.count = rs.rows.length;
//        })
//    }

//    // 因為加入單引號'的時候會和原先的sql語法的單引號'有衝突，所以會導致語法會錯
//    function sqliteEscape(keyWord){
//        keyWord = keyWord.replace(/'/g, "''");

//        return keyWord;
//    }

//    //TODO 實作一個callback
//    /*, callback*/
//    function storeData(deviceName, IPV4Address, userName, password, streamKey, modelName,
//                       networkInterfaceName, networkInterfaceHumanReadableName, networkInterfaceIPV4Address,
//                       networkInterfaceMacAddress, callback) {
//        deviceName = sqliteEscape(deviceName)
//        password = sqliteEscape(password)

//        console.log("storeData function");
//        console.log("device:",deviceName, " IPV4Address:", IPV4Address,
//                    " userName:", userName, " password:", password,
//                    " streamKey:", streamKey, " modelName:", modelName,
//                    " networkInterfaceName:", networkInterfaceName,
//                    " networkInterfaceHumanReadableName:", networkInterfaceHumanReadableName,
//                    " networkInterfaceIPV4Address:", networkInterfaceIPV4Address,
//                    " networkInterfaceMacAddress:", networkInterfaceMacAddress);
//        // 插入資料
//        db.transaction(function(tx) {
//            var allDevice = 'SELECT * FROM device';
//            var rs = tx.executeSql(allDevice);
//            var isExist = false;

//            for(var i = 0; i < rs.rows.length; ++i){
//                var tmpIPV4Address = rs.rows.item(i).IPV4Address;
//                if(tmpIPV4Address == IPV4Address) {
//                    isExist = true;
//                    console.log("DB exist the ip: ", tmpIPV4Address);
//                }
//            }

//            if(!isExist) {
//                var sql = "INSERT INTO device (deviceName, IPV4Address, userName, password, streamKey, modelName,
//                                               networkInterfaceName, networkInterfaceHumanReadableName,
//                                               networkInterfaceIPV4Address, networkInterfaceMacAddress) " +
//                          'VALUES (\'' + deviceName            + '\', \'' + IPV4Address                        + '\', ' +
//                                  '\'' + userName              + '\', \'' + password                           + '\', ' +
//                                  '\'' + streamKey             + '\', \'' + modelName                          + '\', ' +
//                                  '\'' + networkInterfaceName  + '\', \'' + networkInterfaceHumanReadableName  + '\', ' +
//                                  '\'' + networkInterfaceIPV4Address  + '\', \'' + networkInterfaceMacAddress  + '\')';
//                console.log(sql)
//                try {
//                    tx.executeSql(sql);
//                    console.log("INSERT Succuss");
//                } catch(err) {
//                    console.log("INSERT Error:", err);
//                }
//            }
//            if(callback != undefined) {
//                callback(isExist);
//            }
//        });
//    }

//    function readDatas(control, table, callback) {
//        // reads and applies data from DB
//        db.transaction(function(tx) {
//            var sql = 'SELECT * FROM device';
//            var rs = tx.executeSql(sql);
//            var myId;
//            var deviceName;
//            var IPV4Address;
//            var userName;
//            var password;
//            var streamKey;
//            var modelName;
//            var networkInterfaceName;
//            var networkInterfaceHumanReadableName;
//            var networkInterfaceIPV4Address;
//            var networkInterfaceMacAddress;
//            var i, j;
//            var tempDatas = [];

//            // 如果原本列表有資料，更新裡面的資料
//            if(control.datas != undefined && control.datas.length > 0) {
//                for(i = 0; i < rs.rows.length; i++) {
//                    myId = rs.rows.item(i).id;
//                    deviceName = rs.rows.item(i).deviceName;
//                    IPV4Address = rs.rows.item(i).IPV4Address;
//                    userName = rs.rows.item(i).userName;
//                    password = rs.rows.item(i).password;
//                    streamKey = rs.rows.item(i).streamKey;
//                    modelName = rs.rows.item(i).modelName;
//                    networkInterfaceName = rs.rows.item(i).networkInterfaceName;
//                    networkInterfaceHumanReadableName = rs.rows.item(i).networkInterfaceHumanReadableName;
//                    networkInterfaceIPV4Address = rs.rows.item(i).networkInterfaceIPV4Address;
//                    networkInterfaceMacAddress = rs.rows.item(i).networkInterfaceMacAddress;

//                    tempDatas.push({
//                                       "id": myId,
//                                       "isPlay": false,
//                                       "isPrevOpen": false,
//                                       "deviceName": deviceName,
//                                       "IPV4Address": IPV4Address,
//                                       "userName": userName,
//                                       "password": password,
//                                       "streamKey": streamKey,
//                                       "modelName": modelName,
//                                       "networkInterfaceName": networkInterfaceName,
//                                       "networkInterfaceHumanReadableName": networkInterfaceHumanReadableName,
//                                       "networkInterfaceIPV4Address": networkInterfaceIPV4Address,
//                                       "networkInterfaceMacAddress": networkInterfaceMacAddress,
//                                       "componentItem": [],
//                                       "isHovered": false,
//                                       "prevComponent": null
//                                   });
//                }

//                for(i = 0; i < tempDatas.length; i++) {
//                    for(j = 0; j < control.datas.length; j++) {
//                        if(tempDatas[i].id == control.datas[j].id && control.datas[j].isPlay == true) {
//                            tempDatas[i].isPlay = true;
//                            tempDatas[i].isPrevOpen = control.datas[j].isPrevOpen;
//                            tempDatas[i].prevComponent = control.datas[j].prevComponent;
//                        }
//                    }
//                }
//            } else {
//                // 如果資料庫裡面有資料，則會將他加入列表內
//                for(i = 0; i < rs.rows.length; i++) {
//                    myId = rs.rows.item(i).id;
//                    deviceName = rs.rows.item(i).deviceName;
//                    IPV4Address = rs.rows.item(i).IPV4Address;
//                    userName = rs.rows.item(i).userName;
//                    password = rs.rows.item(i).password;
//                    streamKey = rs.rows.item(i).streamKey;
//                    modelName = rs.rows.item(i).modelName;
//                    networkInterfaceName = rs.rows.item(i).networkInterfaceName;
//                    networkInterfaceHumanReadableName = rs.rows.item(i).networkInterfaceHumanReadableName;
//                    networkInterfaceIPV4Address = rs.rows.item(i).networkInterfaceIPV4Address;
//                    networkInterfaceMacAddress = rs.rows.item(i).networkInterfaceMacAddress;

//                    tempDatas.push({
//                                       "id": myId,
//                                       "isPlay": control.datas[i] != undefined? control.datas[i].isPlay : false,
//                                       "isPrevOpen": control.datas[i] != undefined? control.datas[i].isPrevOpen : false,
//                                       "deviceName": deviceName,
//                                       "IPV4Address": IPV4Address,
//                                       "userName": userName,
//                                       "password": password,
//                                       "streamKey": streamKey,
//                                       "modelName": modelName,
//                                       "networkInterfaceName": networkInterfaceName,
//                                       "networkInterfaceHumanReadableName": networkInterfaceHumanReadableName,
//                                       "networkInterfaceIPV4Address": networkInterfaceIPV4Address,
//                                       "networkInterfaceMacAddress": networkInterfaceMacAddress,
//                                       "componentItem": [],
//                                       "componentItem": [],
//                                       "isHovered": false,
//    //                                   "prevComponent": control.datas[i] != undefined? control.datas[i].prevComponent : null
//                                       "prevComponent": null
//                                   });
//                }
//            }

//            table.model = 0;
//            control.datas = []
//            control.datas = tempDatas;
//            table.model = control.datas.length;

//            if(callback != null) {
//                callback(tempDatas);
//            }

//        });
//    }

//    function removeArrayIndex(array, index) {
//        if (index > -1) {
//          array.splice(index, 1);
//        }
//    }

//    function delData(control, table, index) {
//        console.log("delete Data start");
//        console.log("device:", control.datas[index].deviceName, " IPV4Address:", control.datas[index].IPV4Address,
//                    " userName:", control.datas[index].userName, " password:", control.datas[index].password,
//                    " streamKey:", control.datas[index].streamKey, " modelName:", control.datas[index].modelName,
//                    " networkInterfaceName:", control.datas[index].networkInterfaceName,
//                    " networkInterfaceHumanReadableName:", control.datas[index].networkInterfaceHumanReadableName,
//                    " networkInterfaceIPV4Address:", control.datas[index].networkInterfaceIPV4Address,
//                    " networkInterfaceMacAddress:", control.datas[index].networkInterfaceMacAddress);
//        // del data from DB
//        var id = control.datas[index].id;
//        var i;
//        var j;
//        control.datas[index].componentItem.destroy();
//        removeArrayIndex(control.datas, index);

//        for(var i=0;i< control.datas.length;i++) {
//            if(control.datas[i] != undefined && control.datas[i].isPlay == true) {
//                control.prevPlayIndex = i;
//            }
//        }

//        console.log("control.datas.length: ",control.datas.length)
//        db.transaction(function(tx) {
//            var sql = 'DELETE FROM device '+
//                      'WHERE id = ' + id;
//            console.log("delete sql: ",sql)
//            var rs = tx.executeSql(sql);
//            table.model = control.datas.length;
//            console.log("table.model: "+table.model)
//            gc();
//        });
//        console.log("delete Data end");
//    }



//    function updateData(deviceObject, callback) {
//        // update data from DB
//        var id = deviceObject.id;
//        deviceObject.deviceName = sqliteEscape(deviceObject.deviceName)
//        deviceObject.password = sqliteEscape(deviceObject.password)

//        console.log("updateData function")
//        console.log("deviceName: ",deviceObject.deviceName, " modelName: ",deviceObject.modelName,
//                    "streamKey: ", deviceObject.streamKey, " IPV4Address: ", deviceObject.IPV4Address,
//                    " password: ", deviceObject.password, "networkInterfaceName:", deviceObject.networkInterfaceName,
//                    " networkInterfaceHumanReadableName:", deviceObject.networkInterfaceHumanReadableName,
//                    " networkInterfaceIPV4Address:", deviceObject.networkInterfaceIPV4Address,
//                    " networkInterfaceMacAddress:", deviceObject.networkInterfaceMacAddress)
//        db.transaction(function(tx) {
//            var sql = 'UPDATE device '+
//                      'SET deviceName=\'' + deviceObject.deviceName + '\', modelName=\'' + deviceObject.modelName + '\', ' +
//                      'streamKey=\'' + deviceObject.streamKey + '\', IPV4Address=\'' + deviceObject.IPV4Address + '\', '+
//                      'password=\'' + deviceObject.password   + '\', networkInterfaceName=\'' + deviceObject.networkInterfaceName + '\', '+
//                      'networkInterfaceHumanReadableName=\'' + deviceObject.networkInterfaceHumanReadableName + '\', '+
//                      'networkInterfaceIPV4Address=\'' + deviceObject.networkInterfaceIPV4Address + '\', '+
//                      'networkInterfaceMacAddress=\'' + deviceObject.networkInterfaceMacAddress + '\' '+
//                      'WHERE id=' + id;
//            console.log("sql: "+ sql)
//            try {
//                var rs = tx.executeSql(sql);
//                console.log("UPDATE Succuss");
//                callback();
//            } catch(err) {
//                console.log("INSERT Error: ", err);
//            }
//        });
//    }


//    function findIPV4Address(control, table, list, recNotice, successCallback) {
//        db.transaction(function(tx) {
//            var sql = 'SELECT * FROM device';
//            var rs = tx.executeSql(sql);
//            var i;
//            var tmpObejct = {};
//            for(i = 0; i < rs.rows.length; ++i) {
//                var IPV4Address = rs.rows.item(i).IPV4Address;
//                tmpObejct[IPV4Address] = 1;
//            }
//            control.preRadioButtonClickedIndex = -1;
//            control.datas = [];
//            for(let i=0; i < list.length; i++) {
//                if (!(list[i].ip4Addr in tmpObejct)) {
//                    let tempNetworkInterface = []
//                    for(let j=0; j < list[i].netItfList.length; j++) {
//                        console.log("name:", list[i].netItfList[j].uniqueName,
//                                    "humanReadableName:", list[i].netItfList[j].name,
//                                    "IPV4Address:", list[i].netItfList[j].ip4Addr,
//                                    "MacAddress:", list[i].netItfList[j].macAddr)
//                        tempNetworkInterface.push({
//                                                      "name": list[i].netItfList[j].uniqueName,
//                                                      "humanReadableName": list[i].netItfList[j].name,
//                                                      "IPV4Address": list[i].netItfList[j].ip4Addr,
//                                                      "MacAddress": list[i].netItfList[j].macAddr,
//                                                  })
//                    }
//                    control.datas.push(
//                        {
//                            "checked":false,
//                            "deviceName": list[i].deviceName,
//                            "modelName": list[i].modelName,
//                            "IPV4Address": list[i].ip4Addr,
//                            "networkInterface": tempNetworkInterface,
//                            "componentItem": []
//                        });
//                }
//            }

////            //==================== 測試使用的
////            control.datas.push({"checked":false, "deviceName": "UC9020", "modelName": "UC9020", "IPV4Address": "10.3.56.1",
////                                    "networkInterface": [{ "name": "testName", "humanReadableName": "humanReadableName", "IPV4Address": "IPV4Address", "MacAddress": "MacAddress"}],
////                                    "componentItem": []
////                                });
////            control.datas.push({ "checked":false, "deviceName": "UC9040", "modelName": "UC9020", "IPV4Address": "1.1.1.1",
////                                    "networkInterface": [{ "name": "testName", "humanReadableName": "humanReadableName", "IPV4Address": "IPV4Address", "MacAddress": "MacAddress" }],
////                                    "componentItem": []
////                                });
////            control.datas.push({ "checked":false, "deviceName": "UC9020", "modelName": "UC9020", "IPV4Address": "10.3.56.3",
////                                    "networkInterface": [{ "name": "testName", "humanReadableName": "humanReadableName", "IPV4Address": "IPV4Address", "MacAddress": "MacAddress" }],
////                                    "componentItem": []
////                                });
////            //=================

//            if(control.datas.length > 0) {
//                table.visible = true;
//                recNotice.visible = false;
//                table.model = control.datas.length
//            } else {
//                table.visible = false;
//                recNotice.visible = true;
//            }
//            if(successCallback !== null && successCallback !== undefined){
//                successCallback();
//            }
//        })
//    }


//    function checkSameIPV4Address(IPv4Address, callback) {
//        db.transaction(function(tx) {
//            var sql = 'SELECT * FROM device WHERE IPV4Address=\''+IPv4Address+"\'";
//            var rs = tx.executeSql(sql);
//            var isSameAddress = false;

//            if(rs.rows.length > 0) {
//                isSameAddress = true;
//            }

//            callback(isSameAddress);
//        })
//    }

//    function getData(id, callback) {
//        db.transaction(function(tx) {
//            var sql = 'SELECT * FROM device WHERE id=\''+id+"\'";
//            var rs = tx.executeSql(sql);

//            if(rs.rows.length <= 0)
//                return callback(null);

//            var data = {
//                "id": "",
//                "deviceName": "",
//                "IPV4Address": "",
//                "userName": "",
//                "password": "",
//                "streamKey": "",
//                "modelName": "",
//                "networkInterfaceName": "",
//                "networkInterfaceHumanReadableName": "",
//                "networkInterfaceIPV4Address": "",
//                "networkInterfaceMacAddress": "",
//            }

//            data.id = rs.rows.item(0).id;
//            data.deviceName = rs.rows.item(0).deviceName;
//            data.IPV4Address = rs.rows.item(0).IPV4Address;
//            data.userName = rs.rows.item(0).userName;
//            data.password = rs.rows.item(0).password;
//            data.streamKey = rs.rows.item(0).streamKey;
//            data.modelName = rs.rows.item(0).modelName;
//            data.networkInterfaceName = rs.rows.item(0).networkInterfaceName;
//            data.networkInterfaceHumanReadableName = rs.rows.item(0).networkInterfaceHumanReadableName;
//            data.networkInterfaceIPV4Address = rs.rows.item(0).networkInterfaceIPV4Address;
//            data.networkInterfaceMacAddress = rs.rows.item(0).networkInterfaceMacAddress;


//            return callback(data);
//        })
//    }
}
