import QtQuick 2.0

DeviceData {
    property bool isItemHover: false;
    property bool isPlaying: false;
    property bool isPreviewHover: false;
    property bool isDeleteHover: false;
}
