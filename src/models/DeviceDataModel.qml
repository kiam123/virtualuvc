import QtQuick 2.0

Item {
    property var deviceDatas: [];
    property int playIndex: -1;

    // action = {
    //     "type": "ADD",
    //     "deviceData": deviceData
    //     "index": 0  // 不一定需要
    // }
    function reducer(action) {
        switch(action.type) {
            case "ADD":
                addDeviceData(action.deviceData);
                break;
            case "MODIFY":
                modifyDeviceData(action.deviceData, action.index);
                break;
            case "DELETE_INDEX":
                delDeviceData(action.index);
                break;
            case "IS_HOVER":
                deviceDatas[action.index].isItemHover = action.isItemHover;
                deviceDatasChange(deviceDatas);
                break;
            case "SET_PLAY_INDEX":
                playIndex = action.playIndex;
                console.log("action.playIndex : " + action.playIndex);
                playIndexChange(playIndex);
                break;
            case "SORTING":
                sortDeviceData(action.index);
                break;
        }
    }

    function addDeviceData(deviceData) {
        console.log("Store add : " + deviceData.deviceName);
        deviceDatas.push(deviceData);
        deviceDatasChange(deviceDatas);
    }

    function modifyDeviceData(deviceData, index) {
        console.log("Modify DeviceData: "+ index);
        deviceDatas[index] = deviceData;

        deviceDatasChange(deviceDatas);
    }

    function delDeviceData(index) {
        console.log("Delete DeviceData: "+ index);
        deviceDatas.splice(index, 1);
        deviceDatasChange(deviceDatas);
    }

    function sortDeviceData(index) {
        console.log("Sort DeviceData: "+ index);

        if(index === 0) {
            if(playIndex !== -1) {
                console.log("1");
                let tempDeviceDatas = JSON.parse(JSON.stringify(deviceDatas));
                tempDeviceDatas.sort(sortPlay);

                let tempDeviceDatas2 = JSON.parse(JSON.stringify(tempDeviceDatas));
                tempDeviceDatas2.splice(0, 1);
                tempDeviceDatas2.sort(sortModelName);

                tempDeviceDatas.splice(1, tempDeviceDatas.length);

                let result = tempDeviceDatas.concat(tempDeviceDatas2);
                deviceDatas = result;
            } else {
                console.log("2");
                deviceDatas.sort(sortModelName);
            }
        } else if(index === 1) {
            deviceDatas.sort(sortDeviceName);
        } else if(index === 2) {
            deviceDatas.sort(sortModelName);
        }
        deviceDatasChange(deviceDatas);
    }

    function sortPlay(a, b) {
        var isAPlaying = a.isPlaying;

        if(isAPlaying === true) {
            return -1;
        }
    }

    function sortDeviceName(a, b) {
        var isBiggerModel = a.modelName.toLowerCase() > b.modelName.toLowerCase();
        var isSmallModel = a.modelName.toLowerCase() < b.modelName.toLowerCase();

        var isBiggerDevice = a.deviceName.toLowerCase() > b.deviceName.toLowerCase();
        var isSmallDevice = a.deviceName.toLowerCase() < b.deviceName.toLowerCase();

        var isBiggerIpAddress = ipToNum(a.mIPV4Address) > ipToNum(b.mIPV4Address);
        var isSmallIpAddress = ipToNum(a.mIPV4Address) < ipToNum(b.mIPV4Address);

        if(isBiggerDevice) {
            return 1;
        }
        if(isSmallDevice) {
            return -1;
        }

        if(isBiggerModel) {
            return 1;
        }
        if(isSmallModel) {
            return -1;
        }

        if(isBiggerIpAddress) {
            return 1;
        }
        if(isSmallIpAddress) {
            return -1;
        }
    }

    function sortModelName(a, b) {
        var isBiggerModel = a.modelName.toLowerCase() > b.modelName.toLowerCase();
        var isSmallModel = a.modelName.toLowerCase() < b.modelName.toLowerCase();

        var isBiggerDevice = a.deviceName.toLowerCase() > b.deviceName.toLowerCase();
        var isSmallDevice = a.deviceName.toLowerCase() < b.deviceName.toLowerCase();

        var isBiggerIpAddress = ipToNum(a.mIPV4Address) > ipToNum(b.mIPV4Address);
        var isSmallIpAddress = ipToNum(a.mIPV4Address) < ipToNum(b.mIPV4Address);

        if(isBiggerModel) {
            return 1;
        }
        if(isSmallModel) {
            return -1;
        }

        if(isBiggerDevice) {
            return 1;
        }
        if(isSmallDevice) {
            return -1;
        }

        if(isBiggerIpAddress) {
            return 1;
        }
        if(isSmallIpAddress) {
            return -1;
        }
    }

    function ipToNum(arg) {
        return parseInt(arg.replace(/\./g, ""), 10);
    }
}
