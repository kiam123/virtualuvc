function DeviceDataTest() {
    this.id = -1;
    this.deviceName = "";
    this.modelName = "";
    this.userName = "";
    this.password = "";
    this.streamKey = "";
    this.mIPV4Address = "";
    this.networkInterfaceName = "";
    this.networkInterfaceHumanReadableName = "";
    this.networkInterfaceIPV4Address = "";
    this.networkInterfaceMacAddress = "";
}

DeviceDataTest.prototype.setObejct = function(deviceObject) {
    this.id = deviceObject.id;
    this.deviceName = deviceObject.deviceName;
    this.modelName = deviceObject.modelName;
    this.userName = deviceObject.userName;
    this.password = deviceObject.password;
    this.streamKey = deviceObject.streamKey;
    this.mIPV4Address = deviceObject.IPV4Address;
    this.networkInterfaceName = deviceObject.networkInterfaceName;
    this.networkInterfaceHumanReadableName = deviceObject.networkInterfaceHumanReadableName;
    this.networkInterfaceIPV4Address = deviceObject.networkInterfaceIPV4Address;
    this.networkInterfaceMacAddress = deviceObject.networkInterfaceMacAddress;
}

DeviceDataTest.prototype.setAll = function( id,
                                            deviceName,
                                            modelName,
                                            userName,
                                            password,
                                            streamKey,
                                            IPV4Address,
                                            networkInterfaceName,
                                            networkInterfaceHumanReadableName,
                                            networkInterfaceIPV4Address,
                                            networkInterfaceMacAddress) {
    this.id = id;
    this.deviceName = deviceName;
    this.modelName = modelName;
    this.userName = userName;
    this.password = password;
    this.streamKey = streamKey;
    this.mIPV4Address = IPV4Address;
    this.networkInterfaceName = networkInterfaceName;
    this.networkInterfaceHumanReadableName = networkInterfaceHumanReadableName;
    this.networkInterfaceIPV4Address = networkInterfaceIPV4Address;
    this.networkInterfaceMacAddress = networkInterfaceMacAddress;
}
