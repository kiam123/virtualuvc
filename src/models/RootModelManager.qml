import QtQuick 2.0;
import Qt.labs.settings 1.0;
import "../utils/QMLObject.js" as QMLObject;

Item {
    // DB table
    property var deviceDataBase: _deviceDataBase;

    // Settings
    property var settings: _settings;

    // listen device datas changed
    property var deviceDatas: JSON.parse(JSON.stringify(_deviceDataModel.deviceDatas));
    property int playIndex: _deviceDataModel.playIndex;
    signal deviceDatasChange(var deviceDatas);
    signal playIndexChange(var playIndex);

    DeviceDataBase {
        id: _deviceDataBase;
        Component.onCompleted: {
            console.log("deviceDataBase init.......");
            init();

            new Promise((resolve, reject) => {
                console.log("test........");
                var objQML = new QMLObject.QMLObject();
                var temp1 = objQML.createQMLObject("qrc:/src/models/DeviceDataItem.qml", this);
                readDatas().then((res) => {
                    console.log("test3-2........");
                    if(res.status === 200) {
                        console.log("test4-2: " + res.deviceDatas.length);
                        for(var i = 0;i < res.deviceDatas.length;i++) {
                            console.log("res.deviceDatas["+i+"] = " + res.deviceDatas[i].deviceName);
                            var action = {
                                "type": "ADD",
                                "deviceData": res.deviceDatas[i]
                            };
                            dispatch("DeviceDataModel", action);
                        }
                    }
                });
            });
        }
    }

    DeviceDataModel {
        id: _deviceDataModel;

        Component.onCompleted: {
            deviceDatasChange.connect(getDeviceDatas);
            playIndexChange.connect(getPlayIndex);
        }
    }

    Settings {
        id: _settings;
        category: 'setting';
        property string settingDefaultIdleImg: ExePath + "\\..\\vcamdll\\no-source-16-9.png";
        property bool settingStopStreamWarningCheck: true;
        property bool settingSwitchStreamWarningCheck: true;
        property bool settingSwitchMirrorCheck: false;
        property bool settingSwitchAutoStartCheck: true;
    }

    function dispatch(type, action) {
//        console.log("type: " + type);
        if(type === "DeviceDataModel") {
            _deviceDataModel.reducer(action);
        }
    }

    function getDeviceDatas(newDeviceDatas) {
//        console.log("getDeviceDatas update newDeviceDatas length: " + newDeviceDatas.length);
        deviceDatas = JSON.parse(JSON.stringify(_deviceDataModel.deviceDatas));
    }

    function getPlayIndex(newPlayIndex) {
        console.log("newPlayIndex: " + newPlayIndex);
        playIndex = _deviceDataModel.playIndex;
    }
}
