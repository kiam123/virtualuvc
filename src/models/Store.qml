import QtQuick 2.12

Item {
    readonly property alias deviceDataModel: _deviceDataModel;
    readonly property alias deviceLogic: _deviceLogic;

    DeviceDataModel {
        id: _deviceDataModel;
        dispatcher: _deviceLogic;
    }

    DeviceLogic {
        id: _deviceLogic;
    }
}
