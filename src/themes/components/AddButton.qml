import QtQuick 2.0
import QtQuick.Layouts 1.11

Rectangle {
    id: root;
    implicitHeight: 32;
    implicitWidth: 118;
    radius: 2;
    color: baseColor;

    property string baseColor: "#008cff";
    property string hoveredColor: "#61adfd";
    property string originPressedColor: "#008cff";
    property string pressedColor: "#0e62cc";
    property string textColor: "white";

    property string image: "qrc:/src/images/icon_add_2021-04-20/icon_add@3x.png";
    property string text: "Add New";


    Rectangle {
        id: addButton;
        width: parent.width - root.radius;
        height: parent.height;
        color: baseColor;
        anchors.left: parent.left;
        anchors.leftMargin: 2;

        Image {
            id: add;
            height: 32;
            width: 32;
            source: image;
        }

        Text {
            text: root.text;
            color: textColor;
            font.pixelSize: 15;
            anchors.left: add.right;
            anchors.top: parent.top;
            anchors.topMargin: parent.height / 2 - height / 2;
            font.bold: true;
        }
    }

    MouseArea {
        anchors.fill: parent;
        MouseArea {
            anchors.fill: parent;
            hoverEnabled: true;

            onEntered: {
                root.color = hoveredColor;
                addButton.color = hoveredColor;
            }
            onExited: {
                root.color = baseColor;
                addButton.color = baseColor;
            }

            onPressed: {
                root.color = pressedColor;
                addButton.color = pressedColor;
            }
            onReleased: {
                root.color = originPressedColor;
                addButton.color = originPressedColor;
            }
        }
    }
}
