import QtQuick 2.0
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "../../utils"

ComboBox {
    id: root;

    property color checkedColor: "#ebebeb";

    // 下拉式選單點下後裡面的Item
    delegate: ItemDelegate {
        width: root.width

        contentItem: Text {
            text: modelData
            color: "#292929"
            font.family: Fonts.regular
            font.pixelSize: 12
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }

        background: Rectangle {
            id: back
             width: parent.width
             height: parent.height
             color: index == currentIndex? "#f8f8f8": "#ffffff"
//             color: "red"

             MouseArea {
                 anchors.fill: parent
                 hoverEnabled: true

                 onClicked: {
                     currentIndex = index;
                     popup.close();
                 }

                 onEntered: {
                     back.color = root.checkedColor;
                 }

                 onExited: {
                     if(root.highlightedIndex === currentIndex) {
                         back.color = "#f8f8f8";
                     } else {
                         back.color = "#ffffff";
                     }
                 }
             }
         }
    }

    indicator: Canvas {
        id: canvas;
        x: root.width - width - 10;
        y: (root.availableHeight - height) / 2;
        width: 12;
        height: 8;
        contextType: "2d";

        Connections {
            target: root;

            function onPressedChanged() {
                canvas.requestPaint();
            }
        }

        onPaint: {
            context.reset();
            context.moveTo(0, 0);
            context.lineTo(width, 0);
            context.lineTo(width / 2, height);
            context.closePath();
            context.fillStyle = root.down ? Qt.rgba(0, 0, 0, 0.6):"black"
            context.fill();
        }
    }

    // 下拉式選單呈現的內容
    contentItem: Item {
        width: root.background.width - root.indicator.width - 10;
        height: root.background.height;

        Text {
            text: root.displayText;
            color: root.down ? Qt.rgba(0, 0, 0, 0.6) : "#292929";
            x: 10;
            font.pixelSize: 15;
            elide: Text.ElideRight;

            anchors.verticalCenter: parent.verticalCenter;
        }
    }

    // 下拉式選單的背景
    background: Rectangle {
        implicitWidth: 102
        implicitHeight: 41
        color: root.down ? "#d6d6d6" : "#f8f8f8"
        radius: 5

        layer.enabled: root.hovered | root.down
        layer.effect: DropShadow {
            transparentBorder: true
            color: "#f8f8f8"
            samples: 10 /*20*/
        }
    }

    popup: Popup {
        y: root.height
        width: root.width
        implicitHeight: contentItem.implicitHeight
        padding: 0

        contentItem: ListView {
            implicitHeight: contentHeight
            model: root.popup.visible ? root.delegateModel : null
            clip: true
            currentIndex: root.highlightedIndex

            ScrollIndicator.vertical: ScrollIndicator { }
        }

        background: Rectangle {
            color: "#F3F4F5";
            radius: 5;
            clip: true;

            layer.enabled: root.hovered | root.down;
            layer.effect: DropShadow {
                transparentBorder: true;
                color: "#F3F4F5";
                samples: 10;
            }
        }
    }
}
