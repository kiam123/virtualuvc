import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.14
import "../../utils"

Window {
    id: root;
    height: 194;
    width: 400;
    color: "transparent";
    visible: true;
    flags: Qt.Window | Qt.FramelessWindowHint;
//    flags: Qt.Window | Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowMinimizeButtonHint | Qt.WindowMaximizeButtonHint | Qt.WindowStaysOnTopHint
    modality: Qt.ApplicationModal;
    property int previousX;
    property int previousY;

    Rectangle {
        id: rootBlock;
        height: root.height;
        width: 400;
        radius: 4;
        border.color: "black";

        Rectangle {
            id: header;
            width: root.width;
            height: 64;
            anchors.top: parent.top;
            anchors.topMargin: 2;
            anchors.left: parent.left;
            anchors.leftMargin: 2;
            anchors.right: parent.right;
            anchors.rightMargin: 2;

            Text {
                id: txtWarning;
                text: Language.get(Lang.Tag.DISCONNECT_WARNING_TITLE);
                color: "black";
                font.pixelSize: 17;
                font.family: Fonts.bold;
                font.bold: true;
                anchors.verticalCenter: parent.verticalCenter;
                anchors.left: parent.left;
                anchors.leftMargin: 39;
            }

            Image {
                id: imgClose;
                width: 32;
                height: 32;
                anchors.verticalCenter: parent.verticalCenter;
                anchors.right: parent.right;
                anchors.rightMargin: 19.2;
                source: "qrc:/src/images/icon_x_2021-04-29/icon_x@3x.png";
                z: 10;

                MouseArea {
                    anchors.fill: parent;
                    hoverEnabled: true;
                    onClicked: root.destroy();

                    onEntered: {
                        imgClose.opacity = 0.5;
                    }

                    onExited: {
                        imgClose.opacity = 1;
                    }
                }

            }

            // 讓視窗移動
            MouseArea {
                anchors.fill: parent;

                onPressed: {
                    previousX = mouseX;
                    previousY = mouseY;
                }

                onMouseXChanged: {
                    var dx = mouseX - previousX;
                    root.setX(root.x + dx);
                }

                onMouseYChanged: {
                    var dy = mouseY - previousY;
                    root.setY(root.y + dy);
                }
            }

        }

        Rectangle {
            id: txtContent;
            height: 42;
            width: 320;
            anchors.top: header.bottom;
            anchors.left: parent.left;
            anchors.leftMargin: 42;
            anchors.right: parent.right;
            anchors.rightMargin: 42;

            Text {
                text: Language.get(Lang.Tag.DISCONNECT_WARNING_MESSAGE);
                color: "#292929";
                height: parent.height;
                width: parent.width;
                font.pixelSize: 15;
                font.family: Fonts.regular;
                wrapMode: Text.WordWrap;
                anchors.top: parent.top;
                anchors.left: parent.left;
                anchors.leftMargin: 3;
            }
        }

        Button {
            id: next
            text: Language.get(Lang.Tag.DISCONNECT_WARNING_OK_TEXT);
            font.pixelSize: 15;
            font.family: Fonts.bold;
            font.bold: true;
            width: 102;
            height: 32;
            z:4;
            anchors.bottom: rootBlock.bottom;
            anchors.bottomMargin: 24;
            anchors.right: parent.right;
            anchors.rightMargin: 42;


            contentItem: Text {
                text: next.text;
                font: next.font;
                opacity: enabled ? 1 : 0.3;
                color: "#ffffff";
                horizontalAlignment: Text.AlignHCenter;
                verticalAlignment: Text.AlignVCenter;
                elide: Text.ElideRight;
            }

            background: Rectangle {
                color: next.down ? "#0e62cc" : "#008cff";
                radius: 2;
                implicitWidth: 100;
                implicitHeight: 40;
            }

            onClicked: {
                root.destroy();
            }
        }
    }

}
