import QtQuick 2.14
import "../../utils"

Text {
    id: txtName;
    width: 140;
    text: "NAME";
    font.pixelSize: 15;
    font.family: Fonts.regular;
    color: "#292929";
}
