import QtQuick 2.14;
import QtQuick.Controls 2.14;
import "../../utils"

TextField {
    id: txtInputName;
    color: "black";
    width: 342
    height: 30
    maximumLength:30;

    horizontalAlignment: TextInput.AlignLeft;
    verticalAlignment: TextInput.AlignVCenter;
    echoMode: TextInput.Normal;

    selectByMouse: true;
    selectedTextColor: "white";
    selectionColor: "black";

    autoScroll: true;
    passwordMaskDelay: 1000;
    font.family: Fonts.bold;
    font.pixelSize: 15;
    clip: true;
    focus: true;

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.IBeamCursor
        acceptedButtons: Qt.NoButton
    }
}
