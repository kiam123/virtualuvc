import QtQuick 2.14;
import QtQuick.Controls 2.14;

Item {
    //密码输入框
    TextField {
        id: txtInputPassword;
        color: "black";
        text: "password";
        width: 342;
        height: 30;
        maximumLength:30;
        font.family: Fonts.regular;
        font.pixelSize: 15;

        anchors.left: txtPassword.right;
        anchors.leftMargin: 4;
        anchors.verticalCenter: parent.verticalCenter;
        rightPadding: 65;

        horizontalAlignment: TextInput.AlignLeft;
        verticalAlignment: TextInput.AlignVCenter;

        selectByMouse: true;
        selectedTextColor: "white";
        selectionColor: "black";
        autoScroll: true;
        echoMode: TextInput.Password;
        passwordMaskDelay: 1000;
        clip: true;
        focus: true;
        KeyNavigation.tab: txtInputName;
//                    placeholderText: app.get(Appication.TEXT.PASSWORD)
        placeholderText: "PASSWORD"
        MouseArea {
            anchors.fill: parent;
            cursorShape: Qt.IBeamCursor;
            acceptedButtons: Qt.NoButton;
        }

//        onTextChanged: {
//            checkFinishInput();
//        }
    }

    Image {
        id: imgShowPassword
        width: 32
        height: 32
        anchors.right: txtInputPassword.right
        source: "qrc:/src/images/icon_eye_2021-05-04/icon_eye@3x.png"
        z:10

        ToolTip {
            id: toolTipPassword

            contentItem: Text {
                text: ""
                font.pixelSize: 12
                font.family: Fonts.regular
                wrapMode: Text.WordWrap
                color: "#292929"
                verticalAlignment: Text.AlignVCenter
            }
        }

//        MouseArea {
//            anchors.fill: parent
//            hoverEnabled: true

//            onClicked: {
//                if(!recPassword.isShowPassword) {
//                    imgShowPassword.source = "qrc:/src/images/icon_eye_2021-05-04 (1)/icon_eye@3x.png"
//                    txtInputPassword.echoMode = TextInput.Normal
//                } else {
//                    imgShowPassword.source = "qrc:/src/images/icon_eye_2021-05-04/icon_eye@3x.png"
//                    txtInputPassword.echoMode = TextInput.Password
//                }

//                recPassword.isShowPassword = !recPassword.isShowPassword;
//            }

//            onEntered: {
//                toolTipPassword.visible = true
//                imgShowPassword.opacity = 0.5;
//            }

//            onExited: {
//                toolTipPassword.visible = false
//                imgShowPassword.opacity = 1;
//            }
//        }
    }

}
