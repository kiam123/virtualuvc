import QtQuick 2.14;
import QtQuick.Controls 2.14;
//import QtQuick.Layouts 1.11
import QtGraphicalEffects 1.0
import "../components"
import "../../utils"
import "../../utils/uuid_v4.js" as UUID;
import "../../utils/QMLObject.js" as QMLObject;
import "../../models"

Rectangle {
    id: root
    height: win.height;
    width: win.width;
    color: "transparent";

    property var objQML: new QMLObject.QMLObject();
    property var deviceDatas: rootModelManager.deviceDatas;
    property var deviceDatasLength: rootModelManager.deviceDatas.length;
    property var manualNewStreamDeviceControl: objQML.createQMLObject(
        "qrc:/src/themes/home-page/ManualNewStreamDeviceControl.qml",
        this
    );
    property var columnHover: [];

    AddButton {
        id: addButton;
        width: 118;
        height: 32;
        anchors.top: parent.top;
        anchors.topMargin: 20;
        anchors.left: parent.left;
        anchors.leftMargin: 8;

        MouseArea {
            anchors.fill: parent;
            onClicked: {
                console.log("Click.......");
                changeAutoNewStreamDeviceView();
            }
        }
    }

    Rectangle {
        id: arrowButton;
        width: 36;
        height: 32;
        anchors.left: addButton.right;
        anchors.leftMargin: 2;
        anchors.top: content.top;
        anchors.topMargin: 20;
        radius: 2;
        color: "#008cff";

        Rectangle {
            width: 34;
            height: 32;
            color: "#008cff";

            Image {
                source: "qrc:/src/images/icon_dropdown_2021-04-01/icon_dropdown@3x.png";
                anchors.fill: parent;
                anchors.centerIn: parent;
            }
        }
        MouseArea {
            anchors.fill: parent;
            hoverEnabled: true;
            onClicked: {
                arrowButton.color = "#0e62cc";
                arrowButton.children[0].color = "#0e62cc";
                menu.open();
            }

            onEntered: {
                arrowButton.color = "#61adfd";
                arrowButton.children[0].color = "#61adfd";
            }
            onExited: {
                arrowButton.color = "#008cff";
                arrowButton.children[0].color = "#008cff";
            }

            onPressed: {
                arrowButton.color = "#0e62cc";
                arrowButton.children[0].color = "#0e62cc";
            }

            onReleased: {
                arrowButton.color = "#008cff";
                arrowButton.children[0].color = "#008cff";
            }
        }
    }

    Menu {
        id: menu
        property var marginLeft: 16
        property var marginRight: 16
        x: addButton.x
        y: addButton.y + addButton.height + 4
        width: 190

        height: 120
        topPadding: 8
        background: Rectangle {
            id: bgRectangle
            clip: true
            border.color: "transparent"

            layer.enabled: true
            layer.effect: Rectangle {
                id: recParent
                anchors.fill: bgRectangle

                Rectangle {
                    id: rec
                    anchors.top: recParent.top
                    anchors.bottom: recParent.bottom
                    anchors.left: recParent.left
                    anchors.right: recParent.right
                    z: 1
                }

                DropShadow {
                    anchors.fill: rec;
                    horizontalOffset: 1;
                    verticalOffset: 1;
                    radius: 15;
                    samples: 16;
                    source: rec;
                    color: "#000000";
                    opacity: 0.16;
                }
            }
        }

        MenuItem {
            id: menuItemAutoAdd
            height: 52

            Text {
                id: txtAutoAdd
//                text: app.get(Appication.TEXT.AUTO_ADD_TEXT)
                text: "自動新增串流裝置";
                font.pixelSize: 15
                font.family: Fonts.regular
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: menu.marginLeft
            }

            onClicked: {
                changeAutoNewStreamDeviceView();
            }
        }

        MenuItem {
            id: menuManualAdd
            height: 52

            Text {
                id: txtManualAdd
//                text: app.get(Appication.TEXT.MANUAL_ADD_TEXT)
                text: "手動新增串流裝置";
                font.pixelSize: 15;
                font.family: Fonts.regular;
                anchors.verticalCenter: parent.verticalCenter;
                anchors.left: parent.left;
                anchors.leftMargin: menu.marginLeft;
            }

            onClicked: {
                var manualNewStreamView = objQML.createQMLObject(
                    "qrc:/src/themes/home-page/ManualNewStreamDeviceView.qml",
                    root,
                    {
                        "deviceItemSettingControl": manualNewStreamDeviceControl,
                    }
                );
                manualNewStreamDeviceControl.rootViewId = manualNewStreamView;
            }
        }

        Component.onCompleted: {
            if(txtAutoAdd.width > txtManualAdd.width && (txtAutoAdd.width + menu.marginLeft) > menu.width) {
                menu.width = txtAutoAdd.width + marginLeft + marginRight
            } else if(txtManualAdd.width > txtAutoAdd.width && (txtManualAdd.width + menu.marginLeft) > menu.width) {
                menu.width = txtManualAdd.width + marginLeft + marginRight
            }
        }
    }

    Rectangle {
        height: 32;
        width: 36;
        color: "#008cff";
        radius: 2;
        anchors {
            top: content.top;
            topMargin: 20;
            right: content.right;
            rightMargin: 10;
        }

        Image {
            id: iconSetting;
            width: 32;
            height: 32;
            anchors.horizontalCenter: parent.horizontalCenter;
            z: 10;
            source: "qrc:/src/images/icon_tune_dark_2021-05-03/icon_tune_dark@3x.png"

            ToolTip {
                id: tooltipSetting;

                contentItem: Text {
//                    text: app.get(Appication.TEXT.TOOLTIP_SETTINGS);
                    text: "TOOLTIP_SETTINGS";
                    font.pixelSize: 12;
                    font.family: Fonts.regular;
                    wrapMode: Text.WordWrap;
                    color: "#292929";
                    verticalAlignment: Text.AlignVCenter;
                }
            }
        }

        MouseArea {
            anchors.fill: parent;
            hoverEnabled: true;

            onClicked: {
                var settingPage = objQML.createQMLObject(
                    "qrc:/src/themes/setting-page/index.qml",
                    winRoot,
                );
                settingPage.visible = true;
                settingPage.stackView = stackView;

                stackView.push(settingPage);
            }

            onEntered: {
                parent.color = "#61adfd";
                tooltipSetting.visible = true;
            }
            onExited: {
                parent.color = "#008cff";
                tooltipSetting.visible = false;
            }

            onPressed: {
                parent.color = "#0e62cc";
            }

            onReleased: {
                parent.color = "#008cff";
            }
        }
    }

    ModelTableView {
        width: 532;
        height: 318;
        headerColumnWidthArr: [40, 220, 120, 152];
        columnWidthArr: [40, 220, 120, 152];
        headerTitle: ["", "Name", "Model", "Function"];
        deviceDatas: rootModelManager.deviceDatas;

        anchors.top: addButton.bottom;
        anchors.topMargin: 12;
        anchors.bottom: parent.bottom;
        anchors.left: parent.left;
        anchors.leftMargin: 8;
        anchors.right: parent.right;
        anchors.rightMargin: 8;

        handleColumDoubleClicked: modelTableControl.handleColumDoubleClicked;
        handlePlayClicked: modelTableControl.handlePlayClicked;
        handlePreviewClicked: modelTableControl.handlePreviewClicked;
        handleSettingClicked: modelTableControl.handleSettingClicked;
        handleDeletedClicked: modelTableControl.handleDeletedClicked;
        handlerItemHoved: modelTableControl.handlerItemHoved;
        handlerSorting: modelTableControl.handlerSorting;
        getModelItemBackgroundColor: modelTableControl.getModelItemBackgroundColor;
        getModelItemOpacity: modelTableControl.getModelItemOpacity;
    }

    function changeAutoNewStreamDeviceView() {
        var newStreamPage = objQML.createQMLObject(
            "qrc:/src/themes/search-page/index.qml",
            this,
        );
    }
}
