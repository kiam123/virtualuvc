import QtQuick 2.14;
import QtQuick.Controls 2.14;
import "../../utils";

Rectangle {
    id: root;
    implicitHeight: 64;
    implicitWidth: win.width;
    color: "#094576";


    Image {
        id: imgLogo;
        width: 65;
        height: 18;
        anchors.left: root.left;
        anchors.leftMargin: 16;
        anchors.verticalCenter: root.verticalCenter;
    }

    Text {
        text: "Virtual Webcam";
        font.pixelSize: 17;
        font.bold: true;
        anchors.left: imgLogo.right;
        anchors.leftMargin: 10;
        anchors.verticalCenter: root.verticalCenter;
        color: "white";
    }

    Image {
        id: iconMinimized
        width: 32
        height: 32
        z: 10
        anchors {
            top: root.top
            topMargin: (root.height - height) / 2
            right: iconClose.left
            rightMargin: 8
        }
        source: "qrc:/src/images/icon_sm_dark_2021-06-08/icon_sm_dark@3x.png"

        ToolTip {
            id: tooltipMinimized

            contentItem: Text {
//                text: app.get(Appication.TEXT.TOOLTIP_MINIMIZED)
                text: "TOOLTIP_MINIMIZED";
                font.pixelSize: 12;
                font.family: Fonts.regular;
                wrapMode: Text.WordWrap;
                color: "#292929";
                verticalAlignment: Text.AlignVCenter;
            }
        }

        MouseArea {
            anchors.fill: iconMinimized;
            hoverEnabled: true;
            onClicked: {
                win.visibility = "Minimized";
            }

            onEntered: {
                iconMinimized.opacity = 0.5;
                tooltipMinimized.visible = true;
            }
            onExited: {
                iconMinimized.opacity = 1;
                tooltipMinimized.visible = false;
            }
        }
    }

    Image {
        id: iconClose;
        property int notifyTime: 0;
        width: 32;
        height: 32;
        z: 10;
        anchors {
            top: root.top;
            topMargin: (root.height - height) / 2;
            right: root.right;
            rightMargin: 8;
        }
        source: "qrc:/src/images/icon_x_2021-04-01/icon_x@3x.png";

        ToolTip {
            id: tooltipClose;

            contentItem: Text {
//                text: app.get(Appication.TEXT.TOOLTIP_CLOSE)
                text: "TOOLTIP_CLOSE";
                font.pixelSize: 12;
                font.family: Fonts.regular;
                wrapMode: Text.WordWrap;
                color: "#292929";
                verticalAlignment: Text.AlignVCenter;
            }
        }

        MouseArea {
            anchors.fill: iconClose;
            hoverEnabled: true;
            onClicked: {
//                if(iconClose.notifyTime != 1) {
//                    WindowControl.windowSystemTrayIcon.showNotificationMessage();
//                    iconClose.notifyTime = 1;
//                }

                win.hide();
            }

            onEntered: {
                iconClose.opacity = 0.5;
                tooltipClose.visible = true;
            }
            onExited: {
                iconClose.opacity = 1;
                tooltipClose.visible = false;
            }
        }
    }


}
