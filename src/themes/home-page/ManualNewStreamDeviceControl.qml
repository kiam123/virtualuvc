import QtQuick 2.14;
import "../../utils/";
import "../../utils/QMLObject.js" as QMLObject;
import "../../utils/Restful.js" as Restful;
import "../../utils/uuid_v4.js" as UUID;
Item {
    property var objQML: new QMLObject.QMLObject();
    property var httpRequest: new Restful.Http();
    property var rootViewId: null;
    property var networkInterfaceItem: [];
    property var potocol: "http";
    property var version: "";

    function handlerAddButtonClicked(deviceData) {
        console.log("addNewDevice1.....");
        addNewDevice(deviceData).then((res) => {
            console.log("test.....");
            if(res["status"] === 200) {
                rootViewId.destroy();
            }
        }).catch((error) => {
            console.log("error: addNewDevice1.....");
            console.log("deviceDataItem root:" + root);
            deviceDataItem = objQML.createQMLObject(
                "qrc:/src/themes/components/DisconnectIPConnenct.qml",
                root
            );
            console.log("deviceDataItem:" + deviceDataItem);
            deviceDataItem.show();
        });
    }

    function handlerStreamKeyRefeshClick(deviceData) {

    }

    function handlerStreamKeyRefeshEnter(deviceData) {

    }

    function handlerStreamKeyRefeshRotation(deviceData) {

    }

    function handlerStreamKeyRefeshExit(deviceData) {

    }

    function addNewDevice(deviceData) {
        var data = "{\"authorization\": \""+deviceData.password+"\"}";
        return new Promise((resolve, reject) => {
            getVersion().then((res) => {
                version = res;
                return getInfo();
            }).then((res) => {
                console.log("res..");
                console.log("res:", JSON.stringify(res));
                if(version === "v1.0") {
                    console.log("version1.....");
                    deviceData.modelName = res["response"]["modelName"]
                } else {
                    console.log("version2-1.....");
                    console.log("res1:", JSON.stringify(res));
                    console.log("res2:", JSON.parse(res["response"])["deviceName"]);
                    deviceData.modelName = JSON.parse(res["response"])["deviceName"]
                    console.log("version2-2.....");
                }
                login(data).then((res) => {
                    console.log("login1.....");
                    if(res.status === 200) {
                        console.log("login2.....");
                        rootModelManager.deviceDataBase.insertData(deviceData).then((res) => {
                            if(res.status === 200) {
                                console.log("addNewDevice.....");
                                var action = {
                                    "type": "ADD",
                                    "deviceData": deviceData
                                };
                                rootModelManager.dispatch("DeviceDataModel", action);
                                resolve({
                                    "status": 200,
                                    "response": "addNewDevice Success",
                                })
                            } else {
                                reject({
                                    "status": 500,
                                    "response": "addNewDevice Failed",
                                })
                            }
                        });
                    } else {
                        console.log("fail login......");
                        reject({
                            "status": 500,
                            "response": "Login Failed",
                        })
                    }
                })
                .catch((error) => {
                    console.log("test fail login......");
                    reject(error);
                });
            })
        });
    }

    function getVersion() {
        return new Promise((resolve, reject) => {
            httpRequest.get(potocol + "://" + "127.0.0.1:8000" + "/api/version",
                function(response, status) {
                    var apiVersion = "v" + response.split(".")[0] + ".0";
                    resolve(apiVersion);
                },
                function(response, status) {
                    console.log("status: ",status);
                    console.log("failure: ",response);
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    reject(error);
                }
            )
        });
    }

    function getInfo() {
        console.log("testing1...");
        return new Promise((resolve, reject) => {
            console.log("testing2...");
            httpRequest.get(potocol + "://" + "127.0.0.1:8000" + "/api/" + version + "/system/info",
                function(response, status) {
                    console.log("status: ", status);
                    console.log("response: ", response);
                    var res = {
                        "status": status,
                        "response": response
                    }
                    resolve(res);
                },
                function(response, status) {
                    console.log("status: ", status);
                    console.log("failure: ", response);
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    reject(error);
                }
            )
        });
    }

    function login(data) {
        return new Promise((resolve, reject) => {
            var url = "http://127.0.0.1:8000/api/v2.0/auth/login";
            // var url = "https://" + deviceData.mIPV4Address;
            console.log("data:", data);
            httpRequest.post(
                // url + "/auth/login",
                url,
                data,
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    console.log("res:", JSON.stringify(res));
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    console.log("error:", JSON.stringify(error));
                    reject(error);
                }
            )
        });
    }


//     NumberAnimation {
//         id: test2
// //        target: blockAnimation
//         property: "y"
//         duration: 200

// //            Component.onCompleted: {
// //                if(devicePageType !== DeviceEnum.DevicePageType.AutoAdd) {
// //                    test2.from = WindowControl.windowRootBlock.height - footerBlock.height
// //                    test2.to = 0
// //                    test2.running = true
// //                }
// //            }
//     }
}
