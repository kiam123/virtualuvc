import QtQuick 2.12;
import QtQuick.Controls 2.12;
import Qt.labs.qmlmodels 1.0;
import QtQuick.Layouts 1.4;
import QtQuick.LocalStorage 2.12;
import Qt.labs.settings 1.0;
//import aten.com.HttpRequest 1.0;
import "../../utils";
import "../../utils/uuid_v4.js" as UUID;
import "../../utils/QMLObject.js" as QMLObject;
import "../../utils/Restful.js" as Restful;
import "../../models";

Item {
    property var objQML: new QMLObject.QMLObject();
    property int playIndex: rootModelManager.playIndex;
    property var deviceDatas: rootModelManager.deviceDatas;
    property var deviceDatasLength: rootModelManager.deviceDatas.length;
    property var columnHover: [];

    property var deviceData;
    property var potocol: "http"
    property var ipaddress: "127.0.0.1:8000";
    property var version: "";
    property var url: "";
    property var httpRequest: new Restful.Http();

    property var deviceItemSettingControl: objQML.createQMLObject(
        "qrc:/src/themes/home-page/DeviceItemSettingControl.qml",
        this
    );

    Timer {
        id: timer;
    }

    function delay(delayTime, cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }

    function handlePlayClicked(position) {
        console.log("handlePlayClicked...........", position);
        run(position);
    }

    function handleColumDoubleClicked(position) {
        console.log("handleColumDoubleClicked...........", position);
        run(position);
    }

    function handlerItemHoved(position) {
        var action;
        if(deviceDatas[position].isItemHover === false) {
            action = {
                "type": "IS_HOVER",
                "index": position,
                "isItemHover": true
            };

            rootModelManager.dispatch("DeviceDataModel", action);
//            console.log("deviceDatas[position].isItemHover: "+deviceDatas[position].isItemHover);
        } else {
            action = {
                "type": "IS_HOVER",
                "index": position,
                "isItemHover": false
            };

            rootModelManager.dispatch("DeviceDataModel", action);
//            console.log("deviceDatas[position].isItemHover: "+deviceDatas[position].isItemHover);
        }
    }

    function handlePreviewClicked(position) {
        console.log("handlePreviewClicked...........", position);
//        var win = objQML.createQMLObject("qrc:/src/themes/home-page/PrevComponent.qml",
//                                         this,
//                                         {
//                                             "control": control,
//                                             "deviceObject": control.datas[row],
//                                             "row": row
//                                         });
        var win = objQML.createQMLObject(
            "qrc:/src/themes/home-page/PrevComponent.qml",
            this
        );
        win.show();
    }

    function handleSettingClicked(position) {
        var settingView = objQML.createQMLObject(
            "qrc:/src/themes/home-page/DeviceItemSettingView.qml",
            content,
            {
                "deviceData": deviceDatas[position],
                "deviceItemSettingControl": deviceItemSettingControl,


                "onPasswordChange": null,
                "onShowPasswordClick": null,
                "onShowPasswordEnter": null,
                "onShowPasswordExit": null,

                "onStreamKeyRefeshClick": null,
                "onStreamKeyRefeshEnter": null,
                "onStreamKeyRefeshExit": null,
                "onStreamKeyRefeshRotation": null,
            }
        );
        deviceItemSettingControl.position = position;
        deviceItemSettingControl.rootViewId = settingView;
        console.log("handleSettingClicked...........", position);
    }

    function handleDeletedClicked(position) {
        console.log("handleDeletedClicked...........", position);
        var deviceData = rootModelManager.deviceDatas[position];
        rootModelManager.deviceDataBase.deleteData(deviceData).then(value => {
            console.log("deleteData...........");
            var action = {
                "type": "DELETE_INDEX",
                "index": position
            }
            rootModelManager.dispatch("DeviceDataModel", action);
        });

    }

    function handlerSorting(index) {
        var action = {
            "type": "SORTING",
            "index": index
        }
        rootModelManager.dispatch("DeviceDataModel", action);
    }

    function run(position) {
        deviceData = deviceDatas[position];
        url = potocol + "://" + deviceData.mIPV4Address + "/api/";

        console.log("playIndex:", playIndex);
        if(playIndex === position) {
            stop(position);
        } else if(playIndex !== -1) {
            stop(playIndex).then(value => {
                play(position);
            });
        } else {
            play(position);
        }
    }

    function play(position) {
        var authorization = "123";
        getVersion().then(value => {
            console.log("version:", value);
            url += value;
            return login(
                "{\"authorization\": \""+deviceData.password+"\"}"
            );
        }).then(value => {
            return stopStreamer();
        }).then(() => {
            console.log("tryStartStreamer........");
            return tryStartStreamer();
        }).then(value => {
            return getAllStreams();
        }).then(res => {
            return settingStreamer(res);
        }).then(value => {
            console.log("start setCodecConfig1");
            return setCodecConfig("{ \"tv_format\": \"NTSC\" }");
        }).then(value => {
            console.log("start setCodecConfig2");
            return setCodecConfig("{ \"tv_format\": \"NTSC\", \"hdcp\": false }");
        }).then(value => {
            console.log("start setCodecVideo");
            return setCodecVideo(
                1,
                "{
                     \"quality\": \"fluent\",
                     \"frameRate\": 30,
                     \"resolutionIdx\": 0
                }"
            );
        }).then(value => {
            console.log("start startStreaming");
            return startStreaming();
        }).then(value => {
            console.log("restful ending.....");
            var action;
            var playAction;
            var deviceData = deviceDatas[position];
            deviceData.isPlaying = true;
            action = {
                "type": "MODIFY",
                "deviceData": deviceData,
                "index": position
            };

            playAction = {
                "type": "SET_PLAY_INDEX",
                "playIndex": position
            };

            rootModelManager.dispatch("DeviceDataModel", action);
            rootModelManager.dispatch("DeviceDataModel", playAction);
        });
    }

    function stop(position) {
        return new Promise((resolve, reject) => {
            stopStreamer().then((value) => {
                return logout(
                    "{\"authorization\": \""+deviceData.password+"\"}"
                )
            }).then(value => {
                var deviceData = deviceDatas[position];
                var playAction = {
                    "type": "SET_PLAY_INDEX",
                    "playIndex": -1
                };
                deviceData.isPlaying = false;
                var action = {
                    "type": "MODIFY",
                    "deviceData": deviceData,
                    "index": position
                };
                rootModelManager.dispatch("DeviceDataModel", playAction);
                rootModelManager.dispatch("DeviceDataModel", action);
                resolve();
            });
        });
    }

    function getVersion() {
        return new Promise((resolve, reject) => {
//            httpRequest.get(potocol + "://" + deviceData.mIPV4Address + "/api/version",
            httpRequest.get(potocol + "://" + "127.0.0.1:8000" + "/api/version",
                function(response, status) {
                    var apiVersion = "v" + response.split(".")[0] + ".0";
                    resolve(apiVersion);
                },
                function(response, status) {
                    console.log("status: ",status);
                    console.log("failure: ",response);
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    reject(error);
                }
            )
        });
    }

    function login(data) {
        return new Promise((resolve, reject) => {
            console.log("login: " + potocol + "://" + "127.0.0.1:8000/api/v2.0" + "/auth/login")
            httpRequest.post(
//                url + "/auth/login",
//                potocol + "://" + "127.0.0.1:8000/api/v2.0/" + "/auth/login",
                "http://127.0.0.1:8000/api/v2.0/auth/login",
                data,
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    console.log("res:", JSON.stringify(res));
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    console.log("error:", JSON.stringify(error));
                    reject(error);
                }
            )
        });
    }

    function stopStreamer() {
        return new Promise((resolve, reject) => {
            console.log("stopStreamer....");
            httpRequest.post(
//                url + "/streamer/stop",
//                potocol + "://" + "127.0.0.1:8000/api/v2.0/" + "/streamer/stop",
                "http://127.0.0.1:8000/api/v2.0/streamer/stop",
                "",
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    console.log("res:", JSON.stringify(res));
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    console.log("res:", JSON.stringify(error));
                    reject(error);
                }
            )
        });
    }

    function getStreamer() {
        console.log("getStreamer", potocol + "://" + "127.0.0.1:8000/api/v2.0/" + "/streamer/status");
        console.log("getStreamer", "http://127.0.0.1:8000/api/v2.0/streamer/stop");
        return new Promise((resolve, reject) => {
            httpRequest.get(
//                url + "/streamer/status",
                potocol + "://" + "127.0.0.1:8000/api/v2.0/" + "/streamer/status",
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    reject(error);
                }
            )
        });
    }

    function getAllStreams() {
        return new Promise((resolve, reject) => {
            httpRequest.get(
//                url + "/streams",
                potocol + "://" + "127.0.0.1:8000/api/v2.0/" + "/streams",
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    reject(error);
                }
            )
        });
    }

    function setStreams(index, data) {
        return new Promise((resolve, reject) => {
            console.log("setStreams res:", index);
            httpRequest.patch(
//                url + "/streams/" + index,
                potocol + "://" + "127.0.0.1:8000/api/v2.0" + "/streams/" + index,
                data,
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    console.log("setStreams res:", JSON.stringify(res));
                    resolve(res);
                },
                function(response, status) {
                    console.log("error", JSON.stringify(response));
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    reject(error);
                }
            )
        });
    }

    function setCodecConfig(data) {
        return new Promise((resolve, reject) => {
            httpRequest.patch(
//                url + "/codec/config",
                potocol + "://" + "127.0.0.1:8000/api/v2.0" + "/codec/config",
                data,
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    reject(error);
                }
            )
        });
    }

    function setCodecVideo(index, data) {
        return new Promise((resolve, reject) => {
            httpRequest.patch(
//                url + "/codec/video/" + index,
                potocol + "://" + "127.0.0.1:8000/api/v2.0" + "/codec/video/" + index,
                data,
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    reject(error);
                }
            );
        });
    }

    function startStreaming(data = "") {
        return new Promise((resolve, reject) => {
            httpRequest.post(
//                url + "/streamer/start",
                potocol + "://" + "127.0.0.1:8000/api/v2.0" + "/streamer/start",
                data,
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    reject(error);
                }
            );
        });
    }

    function tryStartStreamer() {
        return new Promise((resolve, reject) => {
            function repeat(count) {
                getStreamer().then(value => {
                    var obj = JSON.parse(value["response"]);
//                    console.info("obj[\"overallStatusIdx\"]:", obj["overallStatusIdx"]);
                    if(obj["overallStatusIdx"] === 1) {
                        return stopStreamer();
                    } else {
                        return obj;
                    }
                }).then(obj => {
                    if(count === 0 || obj["overallStatusIdx"] === 0) {
                        resolve("finish");
                    } else {
//                        console.info("in 2-" + count);
                        delay(3000, function run() {
                            console.info("call repeat......");
                            timer.stop();
                            timer.triggered.disconnect((run));
                            repeat(count - 1);
                        });
                    }
                });
            }
            repeat(4);
        })
    }

    function settingStreamer(res) {
//        console.log("settingStreamer:"+ JSON.stringify(res));
        var response = JSON.parse(res["response"]);
        var stmArr = response["streams"];
        let streamsId = [-1, -1, -1]; // streaming1, streaming2, recording

        console.log("streamsId....." + stmArr.length);
        for(let i = 0; i < stmArr.length; i++) {
            console.log("i: ", i);
            let stmObj = stmArr[i];
            let stmType = stmObj["type"];
            let id = stmObj["id"];
            if(stmObj["id"] === 2 && stmType === "rtmp") {
                console.log("found target...................");
                stmObj["name"] = "Custom RTMP";
                stmObj["isEnabled"] = true;
                stmObj["destination"] = "rtmp://10.3.56.121/live";
                stmObj["authentication"] = "123";
                streamsId[0] = i;
                console.log("streamsId[0].......................",streamsId[0]);
            } else if (stmObj["id"] === 3 && stmType === "rtmp") {
                streamsId[1] = i;
                console.log("streamsId[0].......................",streamsId[1]);
            } else if (stmObj["id"] === 4 && stmType === "usb") {
                streamsId[2] = i;
                console.log("streamsId[0].......................",streamsId[2]);
            }
        }

        let streamingObj1 = stmArr[streamsId[0]];
        let streamingObj2 = stmArr[streamsId[1]];

        var streamingUrl1 = "http://" + ipaddress + "/api/" + version + "/streams/" + streamingObj1["id"];
        var streamingUrl2 = "http://" + ipaddress + "/api/" + version + "/streams/" + streamingObj2["id"];

        console.log("ending1.......................",streamingUrl1);
        console.log("ending2.......................",streamingUrl2);

        if(stmArr[streamsId[1]]["isEnabled"] && stmArr[streamsId[2]]["isEnabled"]) {
            streamingObj2["isEnabled"] = false;
            setStreams(streamingObj2["id"], JSON.stringify(streamingObj2)).then(value => {
                return setStreams(streamingObj1["id"], JSON.stringify(streamingObj1));
            })
        } else {
            return setStreams(streamingObj1["id"], JSON.stringify(streamingObj1));
        }
    }

    function logout(data) {
        return new Promise((resolve, reject) => {
            console.log("logout: " + potocol + "://" + "127.0.0.1:8000/api/v2.0" + "/auth/logout")
            httpRequest.post(
//                url + "/auth/login",
//                potocol + "://" + "127.0.0.1:8000/api/v2.0/" + "/auth/logout",
                "http://127.0.0.1:8000/api/v2.0/auth/logout",
                data,
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    console.log("res:", JSON.stringify(res));
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    console.log("error:", JSON.stringify(error));
                    reject(error);
                }
            )
        });
    }

    function getModelItemBackgroundColor(isPlaying, isItemHover) {
        if(isPlaying === true) {
            return "#4287f5";
        } else if(isItemHover) {
            // return "#292929";
            return "red";
        } else { // normal status
            return "white";
        }
    }

    function getModelItemOpacity(isPlaying, isItemHover) {
        if(isPlaying === true) {
            return 0.1;
        } else if(isItemHover) {
            return 1;
        } else {
            return 1;
        }
    }

//    // 遞迴從最後一個刪除component
//    function removeComponent(item) {
//        for(var i = item.children.length-1;i >= 0;i--) {
//            // 定義所有Component的objectName為modelItemObject，
//            // 用於刪除這些Component而定義的
//            if(item.children[i].objectName !== "" &&
//                item.children[i].objectName === item.children[i].modelObejctName) {
//                removeComponent(item.children[i]);
//            }
//        }
//        item.destroy();
//    }



    function resetToDefault() {

    }
}
