import QtQuick 2.12;
import QtQuick.Controls 2.12;
import "../../models/" as Models;
import "../../utils/";

//Item {
Rectangle {
    id: table;
    implicitHeight: 388;
    implicitWidth: win.width;

    property var deviceDatas;

    // table
    property var headerTitle;
    property var headerColumnWidthArr;
    property var columnWidthArr;

    property var modelObejctName: "modelItemObject";

    property var handleColumDoubleClicked;
    property var handlePlayClicked;
    property var handlePreviewClicked;
    property var handleSettingClicked;
    property var handleDeletedClicked;
    property var handlerItemHoved;
    property var handlerSorting;
    property var getModelItemBackgroundColor;
    property var getModelItemOpacity;

    // Header
    Rectangle {
        id: headerComponent;
        height: 48;
        z: 2;
        anchors {
            left: parent.left;
            right: parent.right;
        }

        Row {
            anchors.fill: parent;
            clip: true;
            spacing: 0;

            Repeater {
                id: rectRepeater;
                model: 4;

                Rectangle {
                    id: recHeader;
                    width: headerColumnWidthArr[index];
                    height: 48;
                    color: "#E5E5E5";

                    Text {
                        text: headerTitle[index];
                        color: "#292929";
                        font.pixelSize: 18;
                        font.family: Fonts.bold;
                        font.bold: true;
                        anchors.left: parent.left;
                        anchors.leftMargin: 10;
                        anchors.verticalCenter: parent.verticalCenter;
                    }

                    Image {
                        id: imgArrow;
                        width: 32;
                        height: 32;
                        anchors {
                            right: parent.right;
                            rightMargin: 8;
                            verticalCenter: parent.verticalCenter;
                        }
                        Component.onCompleted: {
                            source = index == 0? "": source;
                        }
                    }

                    Rectangle {
                        height: 1;
                        width: parent.width;
                        anchors.bottom: parent.bottom;
                        color: "#E5E5E5";
                    }

                    Rectangle {
                        height: 1;
                        width: parent.width;
                        anchors.top: parent.top;
                        color: "#E5E5E5";
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(index != 3) {
                                for(var i = 0; i < rectRepeater.count; i++) {
                                    if(rectRepeater.itemAt(i).data[1].source !== "") {
                                        rectRepeater.itemAt(i).data[1].source = "";
                                    }
                                }
                                rectRepeater.itemAt(index).data[1].source = "qrc:/src/images/control_order_z-a_2021-04-07/z-a@3x.png";
                                handlerSorting(index);
                            }
                        }
                    }

                    Component.onCompleted: {
                        console.log("mHeaderColumnWidthArr[index]:", headerColumnWidthArr[index], ".............");
                    }
                }
            }
        }
    }

    // Item
    ListView {
        id: mListView;
        width: parent.width;
        height: 318;
        anchors.top: headerComponent.bottom;
        ScrollBar.vertical: ScrollBar {}
        clip: true;
        model: deviceDatas.length;

        delegate: Rectangle {
            id: recItem;
            height: 48;
            color: "transparent";
            // color: "red"

            property bool hasDeviceData: deviceDatas !== undefined && deviceDatas.length > 0 && deviceDatas[index];
            property bool isItemHover: deviceDatas[index].isItemHover;
            property bool isPlaying: deviceDatas[index].isPlaying;

            property string deviceName: hasDeviceData ? deviceDatas[index].deviceName : "";
            property string modelName: hasDeviceData ? deviceDatas[index].modelName : "";
            property string imgPlayOrStop: isPlaying
                ? "qrc:/src/images/media_play_2021-04-01 (1)/play@3x.png"
                : "qrc:/src/images/media_play_2021-04-01/play@3x.png";
            property string modelItemBackgroundColor: getModelItemBackgroundColor(isPlaying, isItemHover);
            property double modelItemOpacity: getModelItemOpacity(isPlaying, isItemHover);
            // property double modelItemOpacity: 1;

            Rectangle {
                id: modelItem;
                width: mListView.width;
                height: 48;
                color: modelItemBackgroundColor;
                // opacity: modelItemOpacity;

                Rectangle {
                    id: recPlay;
                    width: headerColumnWidthArr[0];
                    height: parent.height;
                    // color: modelItem.color;
                    color: modelItemBackgroundColor;
                    opacity: modelItemOpacity;
                    z: 2;
                    anchors.left: parent.left;

                    Rectangle {
                        id: colorBlock;
                        width: 4;
                        height: 48;
                        color: "#008cff";
                        visible: isPlaying;
                        anchors.top: recPlay.top;
                        anchors.left: recPlay.left;
                    }

                    Image {
                        id: mImgPlay;
                        width: 36;
                        height: 36;
                        anchors.verticalCenter: recPlay.verticalCenter;
                        source: imgPlayOrStop;

                        MouseArea {
                            anchors.fill: mImgPlay;
                            onClicked: {
                                console.log("mImgPlay.........");
                                handlePlayClicked(index);
                            }
                        }
                    }
                }

                Rectangle {
                    id: recDeviceName;
                    width: headerColumnWidthArr[1];
                    height: parent.height;
                    anchors.left: recPlay.right;
                    color: modelItem.color;
                    // color: modelItemBackgroundColor;
                    // opacity: modelItemOpacity;

                    Text {
                        id: mTextDeviceName;
                        font.pixelSize: 14;
                        anchors.left: recDeviceName.left;
                        anchors.leftMargin: 5;
                        anchors.verticalCenter: recDeviceName.verticalCenter;
                        text: deviceName;

                        Component.onCompleted: {
                            console.log("deviceDatas["+index+"].deviceName: "+ deviceName);
                        }
                    }
                }

                Rectangle {
                    id: recModelName;
                    width: headerColumnWidthArr[2];
                    height: parent.height;
                    anchors.left: recDeviceName.right;
                    color: modelItem.color;
                    // color: modelItemBackgroundColor;
                    // opacity: modelItemOpacity;

                    Text {
                        id: mTextModelName;

                        font.pixelSize: 14;
                        anchors.left: recModelName.left;
                        anchors.leftMargin: 12;
                        anchors.verticalCenter: recModelName.verticalCenter;
                        text: modelName;
                    }
                }

                Rectangle {
                    id: recFuncstion;
                    width: headerColumnWidthArr[3];
                    height: parent.height;
                    color: modelItem.color;
                    // color: modelItemBackgroundColor;
                    // opacity: modelItemOpacity;
                    z: 2;
                    anchors.left: recModelName.right;

                    Image {
                        id: mImgPreview;
                        width: 36;
                        height: 36;
                        anchors.left: recFuncstion.left;
                        anchors.verticalCenter: recFuncstion.verticalCenter;
                        source: "qrc:/src/images/icon_open_2021-06-15/icon_open@3x.png"

                        MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                console.log("mImgPreview.........");
                                handlePreviewClicked(index);
                            }
                        }
                    }

                    Image {
                        id: mImgSetting;
                        width: 36;
                        height: 36;
                        anchors.left: mImgPreview.right;
                        anchors.verticalCenter: recFuncstion.verticalCenter;
                        source: "qrc:/src/images/icon_setting_dark_2021-06-15/icon_setting_dark@3x.png"

                        MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                console.log("mImgSetting.........");
                                handleSettingClicked(index);
                            }
                        }
                    }

                    Image {
                        id: mImgDel;
                        width: 36;
                        height: 36;
                        anchors.left: mImgSetting.right;
                        anchors.verticalCenter: recFuncstion.verticalCenter;
                        source: "qrc:/src/images/icon_delete_2021-06-15/icon_delete@3x.png";

                        MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                console.log("mImgDel.........");
                                handleDeletedClicked(index);
                            }
                        }
                    }
                }

                Rectangle {
                    height: 1;
                    width: parent.width;
                    color: "#E5E5E5";

                    anchors.bottom: parent.bottom;
                    z: 2;
                }

                MouseArea {
                    anchors.fill: parent;
                    hoverEnabled: true;
                    onDoubleClicked: {
                        handleColumDoubleClicked(index);
                    }
                    onHoveredChanged: {
                        handlerItemHoved(index);
                    }
                }
            }
        }
    }
}
