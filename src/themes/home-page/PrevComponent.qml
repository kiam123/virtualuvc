import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.14
import QtMultimedia 5.14

Window {
    id: root;
    height: 405 + footer.height;
    width: 720;
    minimumHeight: 360 + footer.height;
    minimumWidth: 640;
    flags: mFlags;

    property var mFlags: Qt.Dialog |
                         Qt.Window |
                         Qt.CustomizeWindowHint |
                         Qt.WindowTitleHint |
                         Qt.WindowMinimizeButtonHint |
                         Qt.WindowMaximizeButtonHint |
                         Qt.WindowCloseButtonHint;

    property bool isFullScreen: false;
    property var windowHeight: 800;
    property var windowWidth: 900;

    property var control: null;
    property var deviceObject: null;
    property var row: null;
    title: " ";
    visible: false;

    Rectangle {
        id: content;
        width: root.width;
        height: root.height - footer.height;
        color: "#000000";
        focus: true;
        anchors.top: root.top;
        anchors.left: root.left;
        anchors.right: root.right;

        Camera {
            id: camera;
            cameraState: Camera.UnloadedState;

            Component.onCompleted: {
                for(let i = 0; i < QtMultimedia.availableCameras.length; i++) {
                    let tmpCam = QtMultimedia.availableCameras[i];
                    if(tmpCam.displayName === CamName) {
                        camera.deviceId = tmpCam.deviceId;
                        camera.cameraState = Camera.ActiveState;
                        break;
                    }
                }
            }
        }

        VideoOutput {
            id: videoOutput;
            width: 720;
            height: 405;
            source: camera;
            autoOrientation: true;
            anchors {
                top: content.top;
                bottom: content.bottom;
                left: content.left;
                right: content.right;
            }
            fillMode: VideoOutput.PreserveAspectFit;
            focus : visible; // to receive focus and capture key events when visible
            MouseArea {
                anchors.fill: parent;
                onDoubleClicked: {
                    root.showVideoFullScreen();
                }
            }
        }

        Keys.onEscapePressed: {
            if (event.key === Qt.Key_Escape) {
                if(isFullScreen) {
                    root.showVideoFullScreen();
                }
            }
        }
    }

    Rectangle {
        id: footer;
        height: 64;
        width: root.width;
        color: "transparent";
        anchors.top: content.bottom;

        MouseArea {
            anchors.fill: parent;
            onClicked: {
                console.log("footer onClicked....");
            }
            onDoubleClicked: {
                console.log("footer onDoubleClicked....");
            }
        }

        Rectangle {
            height: footer.height;
            width: footer.width;
            color: "#ffffff";
            visible: footer.visible;
            anchors {
               top: footer.top;
               bottom: footer.bottom;
               left: footer.left;
               right: footer.right;
            }
            z: 1;
        }

        Rectangle {
            id: recBlock;
            width: footer.width;
            height: 33;
            z: 5;
            visible: footer.visible;
            anchors.verticalCenter: footer.verticalCenter;


            Text {
                font.pixelSize: 24;
                font.family: Fonts.bold;
                font.bold: true;
                text: deviceObject.deviceName;
                visible: footer.visible;
                anchors {
                    verticalCenter: recBlock.verticalCenter;
                    horizontalCenter: recBlock.horizontalCenter;
                }
            }

            Image {
                id: zoom;
                source: "qrc:/src/images/icon_full_screen_2021-04-20/icon_full_screen@3x.png";
                height: 32;
                width: 32;
                visible: footer.visible;
                anchors {
                    right: recBlock.right;
                    rightMargin: 18;
                }

                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
                        root.showVideoFullScreen();
                    }
                }
            }
        }

        onVisibleChanged: {
//            console.log("Footer visible: " + visible);
        }
    }

    onVisibilityChanged: {
//        console.log("prev onVisibilityChanged: "+visible);
        if(visible === false) {
            var rowComponent;
            var functionComponent;

            rowComponent = deviceObject.componentItem;
            functionComponent = rowComponent.children[1].children[3].children[0];
            functionComponent.children[0].opacity = 1;
            deviceObject.isPrevOpen = false;
            control.datas[row].prevComponent = null;

            if(WindowControl.windowRoot.visibility === 0) {
                WindowControl.windowRoot.show();
            }

            root.destroy();
        }
    }

    onHeightChanged: {
        content.height = root.height - footer.height;
    }

    Component.onCompleted: {
        x = Screen.width / 2 - width / 2;
        y = Screen.height / 2 - height / 2;
    }

    function showVideoFullScreen() {
        if(isFullScreen === false) {
            console.log("test1......");
            root.flags = Qt.FramelessWindowHint;



            showFullScreen();
            isFullScreen = true;

            content.height = root.height;
            footer.height = 0;
            footer.width = 0;
            footer.visible = false;

            videoOutput.anchors.leftMargin = 0;
            videoOutput.anchors.rightMargin = 0;


        } else {
            console.log("test2......");
            isFullScreen = false;

            footer.visible = true;

            root.flags = mFlags;
            root.width = 720;
            root.height = 405 + footer.height;
            root.x = Screen.width / 2 - width / 2;
            root.y = Screen.height / 2 - height / 2;

            content.height = root.height - footer.height;

            footer.height = 64;
            footer.width = root.width;
            footer.anchors.top = content.bottom;
        }
    }
}
