import QtQuick 2.14;
import QtQuick.Controls 2.14;
import "../../utils/QMLObject.js" as QMLObject;
import aten.com.HttpRequest 1.0;

Page {
    property var objQML: new QMLObject.QMLObject();
    property var modelTableControl: objQML.createQMLObject(
        "qrc:/src/themes/home-page/ModelTableControl.qml",
        this
    );

    header: Header {
        id: header;
        color: "#094576";
        height: 48;
        width: win.width;

        // 讓視窗移動
        MouseArea {
            anchors.fill: parent;
            onPressed: {
                win.previousX = mouseX;
                win.previousY = mouseY;
            }

            onMouseXChanged: {
                var dx = mouseX - win.previousX;
                win.setX(win.x + dx);
            }

            onMouseYChanged: {
                var dy = mouseY - win.previousY;
                win.setY(win.y + dy);
            }
        }
    }

    Content {
        id: content;
        color: "#f4f4f4";
        anchors.fill: parent;
    }

    footer: Footer {
        id: footer;
        color: "#e6e6e6";
        height: 30;
        width: win.width;
    }

    Component.onCompleted: {
//        modelTableControl.run();
    }

    HttpRequest {
        id: httpRequest;
    }
}

