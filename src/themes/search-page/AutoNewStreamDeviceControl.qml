import QtQuick 2.14;
import "../../utils/";
import "../../utils/QMLObject.js" as QMLObject;
import "../../utils/Restful.js" as Restful;
import "../../utils/uuid_v4.js" as UUID;
import "../../models";

Item {
    property var objQML: new QMLObject.QMLObject();
    property var httpRequest: new Restful.Http();

    function handlerAddButtonClicked(deviceData) {
        console.log("addNewDevice1.....");
        addNewDevice(deviceData).then((res) => {
            console.log("test.....");
            if(res["status"] === 200) {
                root.destroy();
            }
        }).catch((error) => {
            console.log("error: addNewDevice1.....");
            console.log("deviceDataItem root:" + root);
            deviceDataItem = objQML.createQMLObject(
                "qrc:/src/themes/components/DisconnectIPConnenct.qml",
                root
            );
            console.log("deviceDataItem:" + deviceDataItem);
            deviceDataItem.show();
        });
    }

    function handlerStreamKeyRefeshClick(deviceData) {

    }

    function handlerStreamKeyRefeshEnter(deviceData) {

    }

    function handlerStreamKeyRefeshRotation(deviceData) {

    }

    function handlerStreamKeyRefeshExit(deviceData) {

    }

    function addNewDevice(deviceData) {
        var data = "{\"authorization\": \""+deviceData.password+"\"}";
        return new Promise((resolve, reject) => {
            login(data).then((res) => {
                if(res.status === 200) {
                    console.log("login.....");
                    rootModelManager.deviceDataBase.insertData(deviceData).then((res) => {
                        if(res.status === 200) {
                            console.log("addNewDevice.....");
                            var action = {
                                "type": "ADD",
                                "deviceData": deviceData
                            };
                            rootModelManager.dispatch("DeviceDataModel", action);
                            resolve({
                                "status": 200,
                                "response": "addNewDevice Success",
                            })
                        } else {
                            reject({
                                "status": 500,
                                "response": "addNewDevice Failed",
                            })
                        }
                    });
                } else {
                    console.log("fail login......");
                    reject({
                        "status": 500,
                        "response": "Login Failed",
                    })
                }
            })
            .catch((error) => {
                console.log("test fail login......");
                reject(error);
            });
        });
    }

    function login(data) {
        return new Promise((resolve, reject) => {
            var url = "http://127.0.0.1:8000/api/v2.0/auth/login";
            // var url = "https://" + deviceData.mIPV4Address;
            console.log("data:", data);
            httpRequest.post(
                // url + "/auth/login",
                url,
                data,
                function(response, status) {
                    var res = {
                        "status": status,
                        "response": response
                    }
                    console.log("res:", JSON.stringify(res));
                    resolve(res);
                },
                function(response, status) {
                    var error = {
                        "status": status,
                        "errorMessage": response
                    }
                    console.log("error:", JSON.stringify(error));
                    reject(error);
                }
            )
        });
    }

}
