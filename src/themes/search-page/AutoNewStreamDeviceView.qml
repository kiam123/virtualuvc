import QtQuick 2.14;
import QtQuick.Window 2.14;
import QtQuick.Controls 2.14;
import QtQuick.Layouts 1.1;
import Clipboard 1.0;

import "../components" as Components;
import "../../../src/constants/";
import "../../utils"
import "../../utils/QMLObject.js" as QMLObject;
import "../../utils/uuid_v4.js" as UUID;

Rectangle {
    id: rootStreamDevice;
    width: WindowConfig.winWidth;
    height: WindowConfig.winHeight;
    color: "transparent";

    property var deviceData: null;
    property var deviceItemSettingControl: null;

    // header
    property string textTitleName: Language.get(Lang.Tag.AUTO_ADD_TEXT);
    property string btnAddName: Language.get(Lang.Tag.ADD_TEXT);

    // content
    property string textName: Language.get(Lang.Tag.NAME);
    property string textFieldName: deviceData.deviceName;
    property string placeholderTextName: Language.get(Lang.Tag.DEVICE_NAME);
    property var handlerTextNameChange;

    property string textIPV4: Language.get(Lang.Tag.IP);
    property string textIPV4Content: deviceData.mIPV4Address;
    property var handlerTextIPV4Change;

    property string textUserName: Language.get(Lang.Tag.USERNAME);
    property string textUserNameContent: deviceData.userName;

    property string textPassword: Language.get(Lang.Tag.PASSWORD);
    property string textFieldPassword: "";
    property string placeholderTextPassword: Language.get(Lang.Tag.PASSWORD);
    property string toolTipPasswordShow: Language.get(Lang.Tag.TOOLTIP_SHOW_PASSWORD);
    property string toolTipPasswordHidden: Language.get(Lang.Tag.TOOLTIP_HIDDEN_PASSWORD);
    property string imgPasswordShow: "qrc:/src/images/icon_eye_2021-05-04/icon_eye@3x.png";
    property string imgPasswordHidden: "qrc:/src/images/icon_eye_2021-05-04 (1)/icon_eye@3x.png";
    property bool isShowPassword: false;
    property var handlerPasswordChange: null;
    property var handlerShowPasswordClick: null;
    property var handlerShowPasswordEnter: null;
    property var handlerShowPasswordExit: null;

    property string textStreamKey: Language.get(Lang.Tag.STREAM_KEY);
    property string toolTipCopy: Language.get(Lang.Tag.TOOLTIP_COPY);
    property string toolTipRefresh: Language.get(Lang.Tag.TOOLTIP_REFRESH);
    property string imgStreamKeyRefeshImg: "qrc:/src/images/icon_refresh_2021-04-01/icon_refresh@3x.png";
    property string imgStreamKeyCopyImg: "qrc:/src/images/icon_copy_2021-04-25/icon_copy@3x.png";
    property var handlerStreamKeyRefeshClick: null;
    property var handlerStreamKeyRefeshEnter: null;
    property var handlerStreamKeyRefeshExit: null;
    property var handlerStreamKeyRefeshRotation: null;

    property string textModel: Language.get(Lang.Tag.MODEL);
    property string textModelContent: deviceData.modelName;

    property string textNetworkInteface: Language.get(Lang.Tag.NETWORKINTERFACE);
    property string textNetworkIntefaceContent: UUID.uuidv4().substring(0, 20);

    property var handlerAddButtonClicked: deviceItemSettingControl.handlerAddButtonClicked;
    // 用於防止使用到父親的Event，原因是Rectangle會吃到父親的event，除了用Window來檔，但因為要用到主頁的event
    MouseArea {
        anchors.fill: parent;
        hoverEnabled: true;
        onClicked: {}
    }

    Rectangle {
        id: blockAnimation;
        width: rootStreamDevice.width;

        // Header
        Rectangle {
            id: header;
            color: "#f4f4f4";
    //        color: "#094576"
            width: rootStreamDevice.width;
            height: 64;
            z: 10;
            anchors.top: blockAnimation.bottom;
            anchors.left: blockAnimation.left;
            anchors.leftMargin: 1;
            anchors.right: blockAnimation.right;
            anchors.rightMargin: 2;

            Rectangle {
                color: "transparent";
                height: back.height > textTitle.height? back.height: textTitle.height;
                width: textTitle.width + back.width;
                anchors.left: header.left;
                anchors.leftMargin: 8;
                anchors.verticalCenter: parent.verticalCenter;

                Image {
                    id: back;
                    height: 32;
                    width: 32;
                    z: 10;
                    anchors.verticalCenter: parent.verticalCenter;
                    source: "qrc:/src/images/icon_chevron-left_2021-04-27/icon_chevron-left@3x.png";
                }

                Text {
                    id: textTitle;
                    text: textTitleName;
                    color: "#292929";
                    font.pixelSize: 15;
                    font.family: Fonts.bold;
                    font.bold: true;
                    anchors {
                        verticalCenter: parent.verticalCenter;
                        left: back.right;
                    }
                }

                MouseArea {
                    anchors.fill: parent;
                    hoverEnabled: true;
                    onClicked: {
                        rootStreamDevice.destroy();
                    }

                    onEntered: {
                        back.opacity = 0.5;
                        textTitle.opacity = 0.5;
                    }

                    onExited: {
                        back.opacity = 1
                        textTitle.opacity = 1
                    }
                }
            }

            Button {
                id: add
                property var isSelected: false;
                property var backgroundColor: "#d6d6d6";
                text: btnAddName;
                font.family: Fonts.bold;
                font.bold: true;
                font.pixelSize: 15;
                height: 32;
                z: 10;

                anchors {
                    right: header.right;
                    rightMargin: 8;
                    verticalCenter: header.verticalCenter;
                }

                contentItem: Text {
                    text: add.text;
                    font: add.font;
                    opacity: enabled ? 1.0 : 0.3;
                    color: "#ffffff";
                    horizontalAlignment: Text.AlignHCenter;
                    verticalAlignment: Text.AlignVCenter;
                    elide: Text.ElideRight;
                }

                background: Rectangle {
                    implicitWidth: 100;
                    implicitHeight: 40;
//                    opacity: enabled ? 1 : 0.3
                    opacity: 1;
                    radius: 2;
//                    color: add.down ? "#6a9a25" : "#85c12f" //"#90d132"
//                    color: add.isSelected == true? "#85c12f": add.backgroundColor;
                    color: "#85c12f";
                }

                onIsSelectedChanged: {
                    if(add.isSelected == false) {
                        add.background.color = add.backgroundColor;
                    } else {
                        add.background.color = "#85c12f";
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(handlerAddButtonClicked instanceof Function) {
                            console.log("onAddButtonClicked2....");
                            deviceData.deviceName = txtInputName.text;
                            deviceData.password = txtInputPassword.text;
                            handlerAddButtonClicked(deviceData);
                        }
                    }

                    onPressed: {
                        if(add.isSelected == true) {
                            add.background.color = "#6a9a25";
                        }
                    }

                    onReleased: {
                        if(add.isSelected == true) {
                            add.background.color = "#85c12f";
                        }
                    }
                }
            }
        }

        // Content
        Rectangle {
            id: content;
            color: "#ffffff";
            width: rootStreamDevice.width;
            height: rootStreamDevice.height - header.height - footerBlock.height;
            anchors.top: header.bottom;
            anchors.left: parent.left;
            anchors.leftMargin: 1;
            anchors.right: parent.right;
            anchors.rightMargin: 2;
            z: 100;

            // Name
            Rectangle {
                id: recNameRoot;
                width: content.width;
                height: 32;
                anchors.top: content.top;
                anchors.topMargin: 16;
                anchors.left: content.left;
                anchors.leftMargin: 40;
                anchors.right: content.right;
                anchors.rightMargin: 1;
                z: 100;

                Components.TextComponent {
                    id: txtName;
                    width: 140;
                    anchors.verticalCenter: recNameRoot.verticalCenter;
                    text: textName;
                    font.pixelSize: 15;
                }

                Components.TextFieldComponent {
                    id: txtInputName;
                    width: 342;
                    height: 30;
                    rightPadding: 65;
                    anchors.left: txtName.right;
                    anchors.leftMargin: 4;
                    anchors.verticalCenter: recNameRoot.verticalCenter;

                    KeyNavigation.tab: txtInputPassword;
                    text: textFieldName;
                    placeholderText: placeholderTextName;

                    onTextChanged: {
                        if(handlerTextNameChange instanceof Function) {
                            handlerTextNameChange(txtInputName.text);
                        }
                    }
                }
            }

            // IP Address
            Rectangle {
                id: recIPAddressRoot;
                width: content.width;
                height: 32;
                anchors.top: recNameRoot.bottom;
                anchors.topMargin: 15;
                anchors.left: content.left;
                anchors.leftMargin: 40;
                anchors.right: content.right;
                anchors.rightMargin: 1;

                Components.TextComponent {
                    id: txtIPAddress;
                    text: textIPV4;
                    width: 140;
                    anchors.verticalCenter: recIPAddressRoot.verticalCenter;
                    font.pixelSize: 15;
                    font.family: Fonts.regular;
                    color: "#292929";
                }

                Rectangle {
                    id: blockIPAddress;
                    color: "#f8f8f8";
                    width: 342;
                    height: 32;
                    anchors.left: txtIPAddress.right;
                    anchors.leftMargin: 4;

                    Components.TextComponent {
                        id: txtIPAddressComponent;
                        width: 342;
                        text: textIPV4Content;
                        color: "#292929";
                        font.pixelSize: 15;

                        anchors.verticalCenter: blockIPAddress.verticalCenter;
                        anchors.left: blockIPAddress.left;
                        anchors.leftMargin: 12;
                    }
                }
            }

            // UserName
            Rectangle {
                id: recUserName;
                width: content.width;
                height: 32;
                anchors.top: recIPAddressRoot.bottom;
                anchors.topMargin: 15;
                anchors.left: content.left;
                anchors.leftMargin: 40;
                anchors.right: parent.right;
                anchors.rightMargin: 1;

                Components.TextComponent {
                    id: txtUserName;
                    text: textUserName;
                    width: 140;
                    color: "#292929";
                    font.pixelSize: 15;

                    anchors.verticalCenter: recUserName.verticalCenter;
                }

                Rectangle {
                    id: blockUserName;
                    color: "#f8f8f8";
                    width: 342;
                    height: 32;
                    anchors.left: txtUserName.right;
                    anchors.leftMargin: 4;

                    Components.TextComponent {
                        id: txtFixedUserName;
                        width: 342;
                        text: textUserNameContent;
                        color: "#292929";
                        font.pixelSize: 15;

                        anchors.verticalCenter: blockUserName.verticalCenter;
                        anchors.left: blockUserName.left;
                        anchors.leftMargin: 12;
                    }
                }
            }

            // Password
            Rectangle {
                id: recPassword;
                width: content.width;
                height: 32;
                anchors.top: recUserName.bottom;
                anchors.topMargin: 15;
                anchors.left: content.left;
                anchors.leftMargin: 40;
                anchors.right: content.right;
                anchors.rightMargin: 1;


                Components.TextComponent {
                    id: txtPassword;
                    width: 140;
                    text: textPassword;
                    font.pixelSize: 15;
                    color: "#292929";
                    anchors.verticalCenter: recPassword.verticalCenter
                }

                //密码输入框
                TextField {
                    id: txtInputPassword;
                    color: "black";
                    width: 342;
                    height: 30;
                    maximumLength:30;
                    font.family: Fonts.regular;
                    font.pixelSize: 15;
                    placeholderText: placeholderTextPassword;
                    text: textFieldPassword;

                    anchors.left: txtPassword.right;
                    anchors.leftMargin: 4;
                    anchors.verticalCenter: parent.verticalCenter;
                    rightPadding: 65;

                    horizontalAlignment: TextInput.AlignLeft;
                    verticalAlignment: TextInput.AlignVCenter;

                    selectByMouse: true;
                    selectedTextColor: "white";
                    selectionColor: "black";
                    autoScroll: true;
                    echoMode: isShowPassword
                                ? TextField.Normal
                                : TextInput.Password;
                    passwordMaskDelay: 1000;
                    clip: true;
                    focus: true;
                    KeyNavigation.tab: txtInputName;
                    MouseArea {
                        anchors.fill: parent;
                        cursorShape: Qt.IBeamCursor;
                        acceptedButtons: Qt.NoButton;
                    }

                    onTextChanged: {
                        if(handlerPasswordChange instanceof Function) {
                            handlerPasswordChange(txtInputPassword.text);
                        }
                    }
                }

                Image {
                    id: imgShowPassword
                    width: 32
                    height: 32
                    anchors.right: txtInputPassword.right
                    source: isShowPassword? imgPasswordShow: imgPasswordHidden;
                    z:10

                    ToolTip {
                        id: toolTipPassword

                        contentItem: Text {
                            text: isShowPassword? toolTipPasswordShow: toolTipPasswordHidden
                            font.pixelSize: 12
                            font.family: Fonts.regular
                            wrapMode: Text.WordWrap
                            color: "#292929"
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true

                        onClicked: {
                            isShowPassword = !isShowPassword;
//                            if(onShowPasswordClick instanceof Function) {
//                                onShowPasswordClick();
//                            }
                        }

                        onEntered: {
                            if(handlerShowPasswordEnter instanceof Function) {
                                handlerShowPasswordEnter();
                            }
                            toolTipPassword.visible = true
                            imgShowPassword.opacity = 0.5;
                        }

                        onExited: {
                            if(handlerShowPasswordExit instanceof Function) {
                                handlerShowPasswordExit();
                            }

                            toolTipPassword.visible = false;
                            imgShowPassword.opacity = 1;
                        }
                    }
                }
            }

            // Stream Key
            Rectangle {
                id: recStreamKey
                height: 32
                anchors.top: recPassword.bottom
                anchors.topMargin: 15
                anchors.left: content.left
                anchors.leftMargin: 40
                anchors.right: content.right
                anchors.rightMargin: 1

                Components.TextComponent {
                    id: txtStreamKey
                    width: 140
                    anchors.verticalCenter: recStreamKey.verticalCenter
//                    text: app.get(Appication.TEXT.STREAM_KEY)
                    text: textStreamKey;
                    wrapMode: Text.WordWrap
                    font.pixelSize: 15
                    color: "#292929"
                }

                Rectangle {
                    id: blockRandomStreamKey
                    color: "#f8f8f8"
                    width: 298
                    height: 32
                    anchors.left: txtStreamKey.right
                    anchors.leftMargin: 4

                    Text {
                        id: txtFixedRandomStreamKey
                        color: "#292929"
                        width: 213
                        text: textNetworkIntefaceContent;
                        font.pixelSize: 15
                        font.family: Fonts.regular
                        anchors.verticalCenter: blockRandomStreamKey.verticalCenter
                        anchors.left: blockRandomStreamKey.left
                        anchors.leftMargin: 12
                    }

                    Clipboard {
                        id: clipboard
                    }

                    Image {
                        id: imgCopy;
                        width: 32;
                        height: 32;
                        source: imgStreamKeyCopyImg;
                        anchors.right: blockRandomStreamKey.right;
                        anchors.verticalCenter: blockRandomStreamKey.verticalCenter;

                        ToolTip {
                            id: tooltipCopy;

                            contentItem: Text {
                                text: toolTipCopy;
                                font.pixelSize: 12;
                                font.family: Fonts.regular;
                                wrapMode: Text.WordWrap;
                                color: "#292929";
                                verticalAlignment: Text.AlignVCenter;
                            }
                        }

                        MouseArea {
                            anchors.fill: parent;
                            hoverEnabled: true;
                            onClicked: {
                                clipboard.setText(txtFixedRandomStreamKey.text);
                                toast.show(Language.get(Lang.Tag.TOOLTIP_SHOW_PASSWORD), 500);
                            }

                            onEntered: {
                                tooltipCopy.visible = true;
                            }

                            onExited: {
                                tooltipCopy.visible = false;
                            }
                        }
                    }
                }

                Image {
                    id: imgRefresh;
                    width: 32;
                    height: 32;
                    source: imgStreamKeyRefeshImg;
                    anchors.left: blockRandomStreamKey.right;
                    anchors.leftMargin: 12;
                    anchors.verticalCenter: recStreamKey.verticalCenter;

                    ToolTip {
                        id: tooltipRefresh;

                        contentItem: Text {
                            text: toolTipRefresh;
                            font.pixelSize: 12;
                            font.family: Fonts.regular;
                            wrapMode: Text.WordWrap;
                            color: "#292929";
                            verticalAlignment: Text.AlignVCenter;
                        }
                    }

                    MouseArea {
                        anchors.fill: imgRefresh;
                        hoverEnabled: true;

                        onClicked: {
                            if(handlerStreamKeyRefeshClick instanceof Function) {
                                handlerStreamKeyRefeshClick(deviceData, refreshRotation);
                            }
                        }

                        onEntered: {
                            if(handlerStreamKeyRefeshEnter instanceof Function) {
                                handlerStreamKeyRefeshEnter();
                            }
                            imgRefresh.opacity = 0.5
                            tooltipRefresh.visible = true;
                        }

                        onExited: {
                            if(handlerStreamKeyRefeshExit instanceof Function) {
                                handlerStreamKeyRefeshExit();
                            }
                            imgRefresh.opacity = 1;
                            tooltipRefresh.visible = false;
                        }
                    }

                    SequentialAnimation {
                        id: refreshRotation
                        running: false;
                        RotationAnimation {
                            target: imgRefresh
                            from: 0;
                            to: 180;
                            duration: 600;
                        }
                        PropertyAction  {
                            target: imgRefresh
                            property: "source"
                            value: "qrc:/images/icon_loading_2021-05-04/icon_loading@3x.png"
                        }
                        RotationAnimation {
                            target: imgRefresh
                            from: 0;
                            to: 360;
        //                    running: true;
                            duration: 800;
                        }
                        PropertyAction  {
                            target: imgRefresh
                            property: "source"
                            value: "qrc:/images/icon_refresh_2021-05-03/icon_refresh@3x.png"
                        }

                        onRunningChanged: {
                            if(!refreshRotation.running) {
                                txtFixedRandomStreamKey.text = UUID.uuidv4().substring(0,20);
                            }
                        }
                    }
                }
            }

            // Mode
            Rectangle {
                id: recModel;
                width: content.width;
                height: 32;
                anchors.top: recStreamKey.bottom;
                anchors.topMargin: 15;
                anchors.left: content.left;
                anchors.leftMargin: 40;
                anchors.right: parent.right;
                anchors.rightMargin: 1;

                Components.TextComponent {
                    id: txtModel;
                    text: textModel;
                    width: 140;
                    color: "#292929";
                    font.pixelSize: 15;

                    anchors.verticalCenter: recModel.verticalCenter;
                }

                Rectangle {
                    id: blockModel;
                    color: "#f8f8f8";
                    width: 342;
                    height: 32;
                    anchors.left: txtModel.right;
                    anchors.leftMargin: 4;

                    Components.TextComponent {
                        id: txtFixedModel;
                        width: 342;
                        text: textModelContent;
                        color: "#292929";
                        font.pixelSize: 15;

                        anchors.verticalCenter: blockModel.verticalCenter;
                        anchors.left: blockModel.left;
                        anchors.leftMargin: 12;
                    }
                }
            }

            // Network Adapter
            Rectangle {
                id: recNetworkCard;
                height: 32;
                anchors.top: recModel.bottom;
                anchors.topMargin: 15;
                anchors.left: content.left;
                anchors.leftMargin: 40;
                anchors.right: content.right;
                anchors.rightMargin: 1;

                Components.TextComponent {
                    id: txtNetworkCard;
                    width: 140;
                    text: textNetworkInteface;
                    font.pixelSize: 15;
                    color: "#292929";
                    anchors.verticalCenter: recNetworkCard.verticalCenter;
                }

                Rectangle {
                    id: blockFixedNetworkCard;
                    width: 342;
                    height: 64;
                    anchors.left: txtNetworkCard.right;
                    anchors.leftMargin: 4;

                    Text {
                        id: txtNetworkCardContent;
                        text: "txtNetworkCardContent";
                        font.pixelSize: 15;
                        font.family: Fonts.regular;
                        color: "#292929";
                        anchors.left: blockFixedNetworkCard.left;
                        anchors.leftMargin: 12;
                        anchors.verticalCenter: blockFixedNetworkCard.verticalCenter;
                    }
                }
            }
        }

        Rectangle {
            id: footerBlock;
            height: 34;
            width: rootStreamDevice.width;
            color: "transparent";
            anchors.bottom: content.bottom;
        }

        Components.ToastManager {
            id: toast;
            anchors.bottom: footerBlock.top;
        }
    }
}
