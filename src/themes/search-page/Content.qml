import QtQuick 2.14
import QtQuick.Controls 2.14;
import "../../utils";
import "../../utils/QMLObject.js" as QMLObject;
Rectangle {
    id: rootContent;
    height: 328;
    width: 548;

    Rectangle {
        id: textBlock;
        width: 548;
        height: textLan.height;
        anchors {
            left: rootContent.left;
            leftMargin: 12;
            top: rootContent.top;
            topMargin: 8;
            right: rootContent.right;
            rightMargin: 1;
        }
        Text {
            id: textLan;
            width: 486;
//            text: app.get(Appication.TEXT.DEVICE_SUPPORT_NOTIFY)
            text: "Make sure your PC and device are connected in the same network environment. Only ATEN UC9020 / UC9040 are supported.";
            font.pixelSize: 12;
            font.family: Fonts.regular;
            wrapMode: Text.WordWrap;
            opacity: 0.6;
            color: "#292929";
            z:2;
            anchors {
                verticalCenter: parent.verticalCenter;
            }
        }

        Image {
            id: imgRefresh;
            property alias refreshRotation: refreshRotation;
            property alias rotationRefresh: rotationRefresh;
            source: "qrc:/src/images/icon_refresh_2021-05-03/icon_refresh@3x.png";
            width: 32;
            height: 32;

            z:10;
            anchors {
                right: parent.right;
                rightMargin: 15.7;
                verticalCenter: parent.verticalCenter;
            }

            ToolTip {
                id: tooltipRefresh;
//                text: app.get(Appication.TEXT.TOOLTIP_REFRESH);
                text: "REFRESH";
            }

            MouseArea {
                anchors.fill: parent;
                hoverEnabled: true;

                onClicked: {
                    if(refreshRotation.running == false) {
//                        deviceTable.loadingRotation.running = true;
                        refreshRotation.running = true;
//                        deviceTable.control.refresh();
                    }
                }

                onEntered: {
                    imgRefresh.opacity = 0.5;
                    tooltipRefresh.visible = true;
                }

                onExited: {
                    imgRefresh.opacity = 1;
                    tooltipRefresh.visible = false;
                }
            }

            SequentialAnimation {
                id: refreshRotation;
                running: false;

                PropertyAction  {
                    target: imgRefresh;
                    property: "source";
                    value: "qrc:/src/images/icon_loading_2021-05-04/icon_loading@3x.png";
                }

                RotationAnimation {
                    target: imgRefresh;
                    from: 0;
                    to: 180;
                    duration: 100;
                }
                RotationAnimation {
                    id: rotationRefresh;
                    target: imgRefresh;
                    from: 0;
                    to: 360;
                    duration: 1200;
                    loops: Animation.Infinite;
                }
            }
        }

    }

    ModelTableView {
        id: modelTableView;
        width: 532;
        height: 318;
        headerColumnWidthArr: [40, 220, 120, 152];
        columnWidthArr: [40, 220, 120, 152];
        headerTitle: ["", "DeviceName", "Model", "IPAddress"];
//        deviceDatas: rootModelManager.deviceDatas;
        deviceDatas: root.deviceDatas;

        anchors.left: parent.left;
        anchors.leftMargin: 8;
        anchors.right: parent.right;
        anchors.rightMargin: 8;
        anchors.top: textBlock.bottom;
        anchors.topMargin: 5;

        handlerItemHoved: modelTableControl.handlerItemHoved;
        handleColumClicked: modelTableControl.handleColumClicked;
        handleColumDoubleClicked: modelTableControl.handleColumDoubleClicked;
        handlePlayClicked: modelTableControl.handlePlayClicked;
        handlePreviewClicked: modelTableControl.handlePreviewClicked;
        handleSettingClicked: modelTableControl.handleSettingClicked;
        handleDeletedClicked: modelTableControl.handleDeletedClicked;
        handlerSorting: modelTableControl.handlerSorting;

    }
}
