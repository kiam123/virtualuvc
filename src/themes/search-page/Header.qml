import QtQuick 2.14
import QtQuick.Controls 2.14;
import "../../utils";

Rectangle {
    id: rootHeader;

//    property var titleName: app.get(Appication.TEXT.AUTO_ADD_TEXT)
    property string titleName: "自動新增串流裝置";
    property bool isSelected: false;

    property var onBack;
    property var handleNextClick;
    property var defaultBackgroundColor: "#d6d6d6";
    property var backgroundColor: defaultBackgroundColor;

    Rectangle {
        height: imgBack.height > textBack.height? imgBack.height: textBack.height;
        width: imgBack.width + textBack.width;
        anchors.verticalCenter: parent.verticalCenter;
        anchors.left: parent.left;
        anchors.leftMargin: 8;
        color: "transparent";

        Image {
            id: imgBack;
            height: 32;
            width: 32;
            z: 10;
            anchors.verticalCenter: parent.verticalCenter;
            source: "qrc:/src/images/icon_chevron-left_2021-04-27/icon_chevron-left@3x.png";
        }

        Text {
            id: textBack;
            text: titleName;
            color: "#292929";
            font.pixelSize: 15;
            font.family: Fonts.bold;
            font.bold: true;
            anchors {
                left: imgBack.right;
                verticalCenter: parent.verticalCenter;
            }
        }

        MouseArea {
            anchors.fill: parent;
            hoverEnabled: true;
            onClicked: {
                rootHeader.onBack();
            }

            onEntered: {
                imgBack.opacity = 0.5;
                textBack.opacity = 0.5;
            }

            onExited: {
                imgBack.opacity = 1;
                textBack.opacity = 1;
            }
        }
    }

    Button {
        id: next;
        visible: true;
        width: 102;
        height: 32;
        hoverEnabled: true;
        anchors {
            right: parent.right;
            rightMargin: 8;
            verticalCenter: parent.verticalCenter;
        }

        Text {
            id: txtNext;
//            text: app.get(Appication.TEXT.NEXT);
            text: "NEXT";
            font.family: Fonts.bold;
            font.bold: true;
            font.pixelSize: 15;
            opacity: 1;
            color: "#ffffff";
            horizontalAlignment: Text.AlignHCenter;
            verticalAlignment: Text.AlignVCenter;
            anchors.verticalCenter: parent.verticalCenter;
            anchors.horizontalCenter: parent.horizontalCenter;
        }

        background: Rectangle {
            id: recBackgroundColor;
            implicitWidth: 100;
            implicitHeight: 40;
            opacity: 1;
            radius: 2;
            color: backgroundColor;

            property var selectIndex: root.selectIndex;

            onSelectIndexChanged: {
                if(selectIndex >= 0) {
                    backgroundColor = "#85c12f";
                } else {
                    backgroundColor = rootHeader.defaultBackgroundColor;
                }
            }
        }


        onPressed: {
//            console.log("onPressed selectIndex:", root.selectIndex);
            backgroundColor = root.selectIndex > -1?
                                "#6a9a25": rootHeader.backgroundColor;
//            console.log("onPressed recBackgroundColor.color:", recBackgroundColor.color);
        }

        onDownChanged: {
//            console.log("onDownChanged selectIndex:", root.selectIndex);
            backgroundColor = root.selectIndex > -1?
                                "#85c12f": rootHeader.backgroundColor;
//            console.log("onDownChanged recBackgroundColor.color:", recBackgroundColor.color);
        }

        onClicked: {
            handleNextClick();
//            if(isSelected == true) {
//                console.log("deviceObject: "+rootHeader.deviceObject+"..............................--=/==/==");
//                win = CreateComponent.createComponentObjects(
//                    "qrc:/themes/common/DeviceInfoWindow.qml",
//                     rootParent,
//                     {
//                         "deviceObject": rootHeader.deviceObject,
//                         "devicePageType": DeviceEnum.DevicePageType.AutoAdd,
//                         "textTitleName": titleName,
//                         "z": 100
//                     }
//                );
//                win.onAutoAddAction.connect(next.onAutoAddAction);
//            } else {
//                return;
//            }
        }

        Component.onCompleted: {
            if(txtNext.width > width) {
                width = txtNext.width + 32;
            }
        }
    }

    Component.onCompleted: {
        console.log("recBackgroundColor.color:", recBackgroundColor.color);
//        var size = rootHeader.content.children.length-1;
//        console.log("content : "+rootHeader.content.children[size] +".................");
//        rootHeader.content.children[size].children[0].onTableSelect.connect(tableItemSelected);
    }

    function onAutoAddAction() {
        rootHeader.onAutoAddClick();
    }
}
