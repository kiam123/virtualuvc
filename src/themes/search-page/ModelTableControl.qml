import QtQuick 2.12;
import QtQuick.Controls 2.12;
import Qt.labs.qmlmodels 1.0;
import QtQuick.Layouts 1.4;
import QtQuick.LocalStorage 2.12;
import Qt.labs.settings 1.0;
import "../../utils/QMLObject.js" as QMLObject;
import "../../models";

Item {
    function handlerItemHoved(position) {
//        var action;
//        if(deviceDatas[position].isItemHover === false) {
//            action = {
//                "type": "IS_HOVER",
//                "index": position,
//                "isItemHover": true
//            };

//            rootModelManager.dispatch("DeviceDataModel", action);
////            console.log("deviceDatas[position].isItemHover: "+deviceDatas[position].isItemHover);
//        } else {
//            action = {
//                "type": "IS_HOVER",
//                "index": position,
//                "isItemHover": false
//            };

//            rootModelManager.dispatch("DeviceDataModel", action);
////            console.log("deviceDatas[position].isItemHover: "+deviceDatas[position].isItemHover);
//        }
    }

    function handleColumClicked(position) {
        console.log("handleColumClicked...........", position);
        if(position === selectIndex) {
            selectIndex = -1;
        } else {
            selectIndex = position;
        }
        console.log("handleColumClicked selectIndex:", selectIndex);

    }

    function handlePlayClicked(position) {
        console.log("handlePlayClicked...........", position);
    }

    function handlePreviewClicked(position) {
        console.log("handlePreviewClicked...........", position);

        var win = objQML.createQMLObject(
            "qrc:/src/themes/home-page/PrevComponent.qml",
            this
        );
        win.show();
    }

    function handleSettingClicked(position) {
        console.log("handleSettingClicked...........", position);
    }

    function handleDeletedClicked(position) {
        console.log("handleDeletedClicked...........", position);
        var action = {
            "type": "DELETE_INDEX",
            "index": position
        }
        rootModelManager.dispatch("DeviceDataModel", action);
    }

    function handlerSorting(deviceDatas, index) {
        if(index === 1) {
            deviceDatas.sort(sortDeviceName);
        } else if(index === 2) {
            deviceDatas.sort(sortModelName);
        } else if(index === 3) {
            deviceDatas.sort(sortIpAddress);
        }
    }

    function sortDeviceName(a, b) {
        var isBiggerModel = a.modelName.toLowerCase() > b.modelName.toLowerCase();
        var isSmallModel = a.modelName.toLowerCase() < b.modelName.toLowerCase();

        var isBiggerDevice = a.deviceName.toLowerCase() > b.deviceName.toLowerCase();
        var isSmallDevice = a.deviceName.toLowerCase() < b.deviceName.toLowerCase();

        var isBiggerIpAddress = ipToNum(a.mIPV4Address) > ipToNum(b.mIPV4Address);
        var isSmallIpAddress = ipToNum(a.mIPV4Address) < ipToNum(b.mIPV4Address);

        if(isBiggerDevice) {
            return 1;
        }
        if(isSmallDevice) {
            return -1;
        }

        if(isBiggerModel) {
            return 1;
        }
        if(isSmallModel) {
            return -1;
        }

        if(isBiggerIpAddress) {
            return 1;
        }
        if(isSmallIpAddress) {
            return -1;
        }
    }

    function sortModelName(a, b) {
        var isBiggerModel = a.modelName.toLowerCase() > b.modelName.toLowerCase();
        var isSmallModel = a.modelName.toLowerCase() < b.modelName.toLowerCase();

        var isBiggerDevice = a.deviceName.toLowerCase() > b.deviceName.toLowerCase();
        var isSmallDevice = a.deviceName.toLowerCase() < b.deviceName.toLowerCase();

        var isBiggerIpAddress = ipToNum(a.mIPV4Address) > ipToNum(b.mIPV4Address);
        var isSmallIpAddress = ipToNum(a.mIPV4Address) < ipToNum(b.mIPV4Address);

        if(isBiggerModel) {
            return 1;
        }
        if(isSmallModel) {
            return -1;
        }

        if(isBiggerDevice) {
            return 1;
        }
        if(isSmallDevice) {
            return -1;
        }

        if(isBiggerIpAddress) {
            return 1;
        }
        if(isSmallIpAddress) {
            return -1;
        }
    }

    function sortIpAddress(a, b) {
        var isBiggerModel = a.modelName.toLowerCase() > b.modelName.toLowerCase();
        var isSmallModel = a.modelName.toLowerCase() < b.modelName.toLowerCase();

        var isBiggerDevice = a.deviceName.toLowerCase() > b.deviceName.toLowerCase();
        var isSmallDevice = a.deviceName.toLowerCase() < b.deviceName.toLowerCase();

        var isBiggerIpAddress = ipToNum(a.mIPV4Address) > ipToNum(b.mIPV4Address);
        var isSmallIpAddress = ipToNum(a.mIPV4Address) < ipToNum(b.mIPV4Address);

        if(isBiggerIpAddress) {
            return 1;
        }
        if(isSmallIpAddress) {
            return -1;
        }

        if(isBiggerModel) {
            return 1;
        }
        if(isSmallModel) {
            return -1;
        }

        if(isBiggerDevice) {
            return 1;
        }
        if(isSmallDevice) {
            return -1;
        }
    }

    function ipToNum(arg) {
        return parseInt(arg.replace(/\./g, ""), 10);
    }
}
