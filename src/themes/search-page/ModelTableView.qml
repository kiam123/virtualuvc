import QtQuick 2.12;
import QtQuick.Controls 2.12;
import "../../models/" as Models;
import "../../utils";

Rectangle {
    id: modelTableView;
    width: table.width;
    height: 318;
    property var deviceDatas;

    // table
    property var headerTitle;
    property var headerColumnWidthArr;
    property var columnWidthArr;

    property var modelObejctName: "modelItemObject";

    property var handleColumDoubleClicked;
    property var handleColumClicked;
    property var handlePlayClicked;
    property var handlePreviewClicked;
    property var handleSettingClicked;
    property var handleDeletedClicked;
    property var handlerItemHoved;
    property var handlerSorting;

    // Header
    Rectangle {
        id: headerComponent;
        height: 48;
        z: 2;
        anchors {
            left: parent.left;
            right: parent.right;
        }

        Row {
            anchors.fill: parent;
            clip: true;
            spacing: 0;

            Repeater {
                id: rectRepeater;
                model: 4;

                Rectangle {
                    id: recHeader;
                    width: headerColumnWidthArr[index];
                    height: 48;
                    color: "#E5E5E5";

                    Text {
                        text: headerTitle[index];
                        color: "#292929";
                        font.pixelSize: 18;
//                        font.family: Fonts.bold;
                        font.bold: true;
                        anchors.left: parent.left;
                        anchors.leftMargin: 10;
                        anchors.verticalCenter: parent.verticalCenter;

                    }

                    Image {
                        id: imgArrow;
                        width: 32;
                        height: 32;
                        anchors {
                            right: parent.right;
                            rightMargin: 8;
                            verticalCenter: parent.verticalCenter;
                        }
                        Component.onCompleted: {
                            source = index === 0? "": source;
                        }
                    }

                    Rectangle {
                        height: 1;
                        width: parent.width;
                        anchors.bottom: parent.bottom;
                        color: "#E5E5E5";
                    }

                    Rectangle {
                        height: 1;
                        width: parent.width;
                        anchors.top: parent.top;
                        color: "#E5E5E5";
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(index !== 0) {
                                for(var i = 0; i < rectRepeater.count; i++) {
                                    if(rectRepeater.itemAt(i).data[1].source !== "") {
                                        rectRepeater.itemAt(i).data[1].source = "";
                                    }
                                }
                                rectRepeater.itemAt(index).data[1].source = "qrc:/src/images/control_order_z-a_2021-04-07/z-a@3x.png";
                                handlerSorting(deviceDatas, index);
                                var temp = JSON.parse(JSON.stringify(deviceDatas));
                                deviceDatas = [];
                                deviceDatas = temp;
                            }
                        }
                    }

                    Component.onCompleted: {
                        console.log("mHeaderColumnWidthArr[index]:", headerColumnWidthArr[index], ".............");
                    }
                }
            }
        }
    }

    // Item
    ListView {
        id: mListView;
        visible: false;
        width: parent.width;
        height: 318;
        anchors.top: headerComponent.bottom;
        ScrollBar.vertical: ScrollBar {}
        clip: true;
        model: deviceDatas.length;

        delegate: Rectangle {
            id: recItem;
            height: 48;
//            color: "transparent";
            color: "red"

            Rectangle {
                id: modelItem;

                color: deviceDatas[index].isItemHover? "red": "white";
//                color: "white"
                width: mListView.width;
                height: 48;

                SelectRadioButton {
                    id: radioButton;
                    width: columnWidthArr[0];
                    height: 48;
//                    image: "qrc:/src/images/icon_radio_button_2021-04-01/icon_radio_button@3x.png";
                    image: selectIndex !== index? "qrc:/src/images/icon_radio_button_2021-04-01 (1)/icon_radio_button@3x.png":
                                                  "qrc:/src/images/icon_radio_button_2021-04-01/icon_radio_button@3x.png";

                    MouseArea {
                        anchors.fill: radioButton;
                        onClicked: {
                            console.log("mImgPlay.........");
                            handlePlayClicked(index);
                        }
                    }
                }

                Rectangle {
                    id: recDeviceName;

                    width: headerColumnWidthArr[1];
                    height: parent.height;
                    anchors.left: radioButton.right;
                    color: modelItem.color;

                    Text {
                        id: mTextDeviceName;

                        font.pixelSize: 14;
                        anchors.left: recDeviceName.left;
                        anchors.leftMargin: 5;
                        anchors.verticalCenter: recDeviceName.verticalCenter;
                        text: deviceDatas !== undefined && deviceDatas.length > 0 && deviceDatas[index]?
                                  deviceDatas[index].deviceName: "";

                        Component.onCompleted: {
                            console.log("deviceDatas["+index+"].deviceName: "+ deviceDatas[index].deviceName);
                        }
                    }
                }

                Rectangle {
                    id: recModelName;

                    width: headerColumnWidthArr[2];
                    height: parent.height;
                    anchors.left: recDeviceName.right;
                    color: modelItem.color;

                    Text {
                        id: mTextModelName;

                        font.pixelSize: 14;
                        anchors.left: recModelName.left;
                        anchors.leftMargin: 12;
                        anchors.verticalCenter: recModelName.verticalCenter;
                        text: deviceDatas !== undefined && deviceDatas.length > 0 && deviceDatas[index]?
                                  deviceDatas[index].modelName: "";
                    }
                }

                Rectangle {
                    id: recIPAddress;

                    width: headerColumnWidthArr[2];
                    height: parent.height;
                    anchors.left: recModelName.right;
                    color: modelItem.color;

                    Text {
                        id: mTextIPAddress;

                        font.pixelSize: 14;
                        anchors.left: recIPAddress.left;
                        anchors.leftMargin: 12;
                        anchors.verticalCenter: recIPAddress.verticalCenter;
                        text: deviceDatas !== undefined &&
                              deviceDatas.length > 0 &&
                              deviceDatas[index]
                                ? deviceDatas[index].mIPV4Address
                                : "";
                    }
                }

                Rectangle {
                    height: 1;
                    width: parent.width;
                    color: "#E5E5E5";

                    anchors.bottom: parent.bottom;
                    z: 2;
                }

                MouseArea {
                    anchors.fill: parent;
                    hoverEnabled: true;
                    onClicked: {
                        handleColumClicked(index);
                    }
                    onHoveredChanged: {
                        handlerItemHoved(index);
                    }
                }

                Component.onCompleted: {

                }
            }
        }
    }

    Rectangle {
        id: recNotice;
        visible: false;
        width: parent.width;
        height: 318;
        color: "#f4f4f4";
        anchors {
            top: headerComponent.bottom;
            topMargin: headerComponent.rowHeight;
        }

        Text {
            id: txtNoticeOne;
            text: Language.get(Lang.Tag.HOME_PAGE_NO_DEVICE_NOTIFY);
            width: 384;
            wrapMode: Text.WordWrap;
            font.pixelSize: 16;
            font.family: Fonts.regular;
            anchors.top: recNotice.top;
            anchors.topMargin: 118;
            anchors.horizontalCenter: recNotice.horizontalCenter;
            horizontalAlignment: Text.AlignHCenter;
        }
    }

    Rectangle {
        id: recSearch;
        visible: true;
        width: parent.width;
        height: 318;
        anchors.top: headerComponent.bottom;

        Image {
            id: imgLoadingBackground;
            source: "qrc:/src/images/loading_mask_2021-06-17/loading_mask@3x.png";
            z: 20;

            height: 100;
            width: 100;
            anchors.verticalCenter: parent.verticalCenter;
            anchors.horizontalCenter: parent.horizontalCenter;
        }

        Image {
            id: imgLoading;
            property var isFirst: true;
            property var loadingCount: 0;
            signal timeOutLoading();
            source: "qrc:/src/images/loading_color_2021-06-18/loading_color@3x.png";
            z: 10;

            height: 100;
            width: 100;
            anchors.verticalCenter: parent.verticalCenter;
            anchors.horizontalCenter: parent.horizontalCenter;

            SequentialAnimation {
                id: loadingRotation;
                running: true;
                RotationAnimation {
                    target: imgLoading;
                    from: 0;
                    to: 360;
                    duration: 1000;
                    loops: Animation.Infinite;
                }

                onRunningChanged: {
                    console.log("loadingRotation............")
                    if(running == true) {
                        recSearch.visible = true;
                        recNotice.visible = false;
//                        control.delay(3000, imgLoading.timeOutLoading);
                    } else {
                        timer.stop();
                    }
                }
            }


            onTimeOutLoading: {
                if(loadingRotation.running === true) {
                    control.setToDefault();
                    loadingRotation.running = false;
                }
            }
        }
    }

    Timer {
        id: timer
    }

    function delay(delayTime, cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }
}
