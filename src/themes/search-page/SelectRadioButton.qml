import QtQuick 2.0
import QtQuick.Controls 2.14

Rectangle {
    id: radioComponent;
    width: 40;
    height: 40;
    visible: true;
    color: "transparent";
    z: 10;

    property var image: "qrc:/images/icon_radio_button_2021-04-01 (1)/icon_radio_button@3x.png";

    Image {
        property var row;
        width: 32;
        height: 32;
        anchors.verticalCenter: radioComponent.verticalCenter;
        anchors.horizontalCenter: radioComponent.horizontalCenter;
        source: image;
        signal clicked(var row);
    }
}
