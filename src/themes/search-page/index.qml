import QtQuick 2.14
import QtQuick.Controls 2.14;
import "../../utils/";
import "../../utils/QMLObject.js" as QMLObject;
import "../../utils/uuid_v4.js" as UUID;

Rectangle {
    id: root;
    height: win.height;
    width: win.width;
    radius: 2;
    color: "transparent";

    property var objQML: new QMLObject.QMLObject();

    property string backgroundColor: "white";
    property string headerBackgroundColor: "white";

    property var deviceDatas;

    property int selectIndex: -1;

    property var autoNewStreamDeviceControl: objQML.createQMLObject(
        "qrc:/src/themes/search-page/AutoNewStreamDeviceControl.qml",
        this
    );
    property var modelTableControl: objQML.createQMLObject(
        "qrc:/src/themes/search-page/ModelTableControl.qml",
        this
    );

    Header {
        id: header;
        height: 64;
        width: winRoot.width;
        color: headerBackgroundColor;

        onBack: root.onBack;
        handleNextClick: root.handleNextClick;
    }

    Content {
        id: content;
        height: winRoot.height - header.height - footer.height;
        width: winRoot.width;
        color: backgroundColor;
        anchors.top: header.bottom;
    }

    Footer {
        id: footer;
        height: 30;
        width: winRoot.width;
        color: backgroundColor;
        anchors.top: content.bottom;
    }

    Component.onCompleted: {
        var objQML = new QMLObject.QMLObject();
        var temp1 = objQML.createQMLObject("qrc:/src/models/DeviceDataItem.qml", this);
        temp1.setAll(
            "UC9020",
            "UC9020",
            "adminstator",
            "123456",
            "123456",
            "10.3.56.123",
            "123",
            "123",
            "455",
            "789"
        );

        var temp2 = objQML.createQMLObject("qrc:/src/models/DeviceDataItem.qml", this);
        temp2.setAll(
            "VP2120",
            "VP2120",
            "adminstator",
            "123456",
            "",
            "10.3.56.124",
            "123",
            "123",
            "455",
            "789"
        );

        var temp3 = objQML.createQMLObject("qrc:/src/models/DeviceDataItem.qml", this);
        temp3.setAll(
            "UC9021",
            "UC9021",
            "adminstator",
            "123456",
            "",
            "10.3.56.125",
            "123",
            "123",
            "455",
            "789"
        );
        deviceDatas = [temp1, temp2, temp3];
    }

    function handleNextClick() {
        if(selectIndex == - 1) {
            return ;
        }

        objQML.createQMLObject(
            "qrc:/src/themes/search-page/AutoNewStreamDeviceView.qml",
            root,
            {
                "deviceData": deviceDatas[selectIndex],
                "deviceItemSettingControl": autoNewStreamDeviceControl
            }
        );
    }

    function resetToDefault() {

    }

    function onBack() {
        root.destroy();
    }
}
