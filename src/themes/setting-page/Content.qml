import QtQuick 2.14
import QtQuick.Controls 2.14
import Qt.labs.settings 1.0
import PlatformControlModule 1.0
//import VcamMain.CamState 1.0
import "../../utils";
import "../../utils/QMLObject.js" as CreateComponent;

Rectangle {
    Text {
        id: textRTMP;
        color: "#292929";
        text: Language.get(Lang.Tag.SETTING_PAGE_NAME);
        opacity: 0.38;
        font.pixelSize: 13;
        font.family: Fonts.bold;
        font.bold: true;
        anchors.left: parent.left;
        anchors.leftMargin: 24;
        anchors.top: parent.top;
        anchors.topMargin: 24;
    }

    Rectangle {
        id: recBottom;
        color: "#292929";
        width: parent.width;
        anchors.top: textRTMP.bottom;
        anchors.topMargin: 12;
        anchors.left: parent.left;
        anchors.leftMargin: 24;
        anchors.right: parent.right;
        anchors.rightMargin: 24;
        opacity: 0.16;
        height: 1;
    }


    Rectangle {
        id: recStopPush;
        height: 23;
        width: parent.width;
        anchors.top: recBottom.bottom;
        anchors.topMargin: 22.3;

        Rectangle {
            id: textStopPush;
            width: 350;
//            width: children[0].width
            height: children[0].height;
            anchors.verticalCenter: parent.verticalCenter;
            anchors.left: recStopPush.left;
            anchors.leftMargin: 24;

            Text {
                color: "#292929";
                width: 350;
                text: Language.get(Lang.Tag.SETTING_PAGE_STOP_WARNING);
                wrapMode: Text.WordWrap;
                font.pixelSize: 15;
                font.family: Fonts.regular;
            }
        }


        Switch {
            id: stopStream;
            property color checkedColor: "#094576";
            checked: rootModelManager.settings.settingStopStreamWarningCheck;
            anchors.verticalCenter: recStopPush.verticalCenter;
            anchors.left: textStopPush.right;
            anchors.leftMargin: 55;
            width: 38;
            height: 20;
            indicator: Rectangle {
                width: 38;
                height: 20;
                radius: height / 2;
                color: stopStream.checked ? stopStream.checkedColor : "#9e9e9e";
                border.width: 2;
                border.color: stopStream.checked ? stopStream.checkedColor : "#E5E5E5";

                Rectangle {
                    property var temp: "value";
                    x: stopStream.checked ? parent.width - width - 2 : 1;
                    width: stopStream.checked ? parent.height - 4 : parent.height - 2;
                    height: width;
                    radius: width / 2;
                    anchors.verticalCenter: parent.verticalCenter;
                    color: "white";
                    border.color: "#D5D5D5";

                    Behavior on x {
                        NumberAnimation { duration: 200 }
                    }
                }



                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
//                        if(stopStream.checked) {
//                            var indicatorWidth = stopStream.indicator.children[0].width;
//                            stopStream.indicator.color = "#9e9e9e";
//                            stopStream.indicator.border.color = "#E5E5E5";
//                            stopStream.indicator.children[0].x = 0;
//                            stopStream.indicator.children[0].width = stopStream.indicator.children[0].parent.height - 4;
//                            stopStream.checked = false;
//                            rootModelManager.settings.settingStopStreamWarningCheck = stopStream.checked;
//                            rootModelManager.settings.sync();
//                        } else {
//                            stopStream.indicator.color = stopStream.checkedColor;
//                            stopStream.indicator.border.color = stopStream.checkedColor;
//                            stopStream.indicator.children[0].x = 20;
//                            stopStream.indicator.children[0].width = stopStream.indicator.children[0].parent.height - 2;
//                            stopStream.checked = true;
//                            rootModelManager.settings.settingStopStreamWarningCheck = stopStream.checked
//                        }
                    }
                }
            }
        }

        Text {
            text: stopStream.checked == true
                  ? Language.get(Lang.Tag.SETTING_PAGE_ON)
                  : Language.get(Lang.Tag.SETTING_PAGE_OFF);
            width: 17;
            height: 16;
            font.pixelSize: 12;
            font.family: Fonts.regular;
            renderType: Text.NativeRendering;//不確定是否blur
            anchors.verticalCenter: parent.verticalCenter;
            anchors.left: stopStream.right;
            anchors.leftMargin: 2;
        }
    }

    Rectangle {
        id: recChaneWarning;
        height: 23;
        width: parent.width;
        anchors.top: recStopPush.bottom;
        anchors.topMargin: 22.3;



        Rectangle {
            id: txtChaneWarning;
            width: 350;
            height: children[0].height;
            anchors.verticalCenter: recChaneWarning.verticalCenter;
            anchors.left: recChaneWarning.left;
            anchors.leftMargin: 24;

            Text {
                color: "#292929";
                width: 350;
                wrapMode: Text.WordWrap;
                text: Language.get(Lang.Tag.SETTING_PAGE_EXCHANGE_WARNING);
                font.pixelSize: 15;
                font.family: Fonts.regular;
            }
        }

        Switch {
            id: switchLowFlow;
            property color checkedColor: "#094576";
            width: 38;
            height: 20;
            checked: rootModelManager.settings.settingSwitchStreamWarningCheck;
            anchors.verticalCenter: recChaneWarning.verticalCenter;
            anchors.left: txtChaneWarning.right;
            anchors.leftMargin: 55;
            checkable: rootModelManager.settings.settingSwitchStreamWarningCheck;
            indicator: Rectangle {
                width: 38;
                height: 20;
                radius: height / 2;
                color: switchLowFlow.checked ? switchLowFlow.checkedColor : "#9e9e9e";
                border.width: 2;
                border.color: switchLowFlow.checked ? switchLowFlow.checkedColor : "#E5E5E5";

                Rectangle {
                    property var temp: "value";
                    x: switchLowFlow.checked ? parent.width - width - 2 : 1;
                    width: switchLowFlow.checked ? parent.height - 4 : parent.height - 2;
                    height: width;
                    radius: width / 2;
                    anchors.verticalCenter: parent.verticalCenter;
                    color: "white";
                    border.color: "#D5D5D5";

                    Behavior on x {
                        NumberAnimation { duration: 200 }
                    }
                }

                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
//                        if(switchLowFlow.checked) {
//                            var indicatorWidth = switchLowFlow.indicator.children[0].width;
//                            switchLowFlow.indicator.color = "#9e9e9e";
//                            switchLowFlow.indicator.border.color = "#E5E5E5";
//                            switchLowFlow.indicator.children[0].x = 0;
//                            switchLowFlow.indicator.children[0].width = switchLowFlow.indicator.children[0].parent.height - 4;
//                            switchLowFlow.checked = false;
//                            rootModelManager.settings.settingSwitchStreamWarningCheck = switchLowFlow.checked;
//                            rootModelManager.settings.sync();
//                        } else {
//                            switchLowFlow.indicator.color = switchLowFlow.checkedColor;
//                            switchLowFlow.indicator.border.color = switchLowFlow.checkedColor;
//                            switchLowFlow.indicator.children[0].x = 20;
//                            switchLowFlow.indicator.children[0].width = switchLowFlow.indicator.children[0].parent.height - 2;
//                            switchLowFlow.checked = true;
//                            rootModelManager.settings.settingSwitchStreamWarningCheck = switchLowFlow.checked;
//                        }
                    }
                }
            }
        }

        Text {
            text: switchLowFlow.checked == true
                    ? Language.get(Lang.Tag.SETTING_PAGE_ON)
                    : Language.get(Lang.Tag.SETTING_PAGE_OFF);
            width: 17;
            height: 16;
            font.pixelSize: 12;
            font.family: Fonts.regular;
            renderType: Text.NativeRendering;//不確定是否blur
            anchors.verticalCenter: parent.verticalCenter;
            anchors.left: switchLowFlow.right;
            anchors.leftMargin: 2;
        }
    }

    Rectangle {
        id: recMirror;
        height: 23;
        width: parent.width;
        anchors.top: recChaneWarning.bottom;
        anchors.topMargin: 22.3;

        Rectangle {
            id: txtMirror;
            width: 350;
            height: children[0].height;
            anchors.verticalCenter: recMirror.verticalCenter;
            anchors.left: recMirror.left;
            anchors.leftMargin: 24;
            Text {
                color: "#292929";
                width: 350;
                wrapMode: Text.WordWrap;
                text: Language.get(Lang.Tag.SETTING_PAGE_FLIP);
                font.pixelSize: 15;
                font.family: Fonts.regular;
            }
        }

        Switch {
            id: switchMirror;
            property color checkedColor: "#094576";
            width: 38;
            height: 20;
            checked: rootModelManager.settings.settingSwitchMirrorCheck;
            anchors.verticalCenter: recMirror.verticalCenter;
            anchors.left: txtMirror.right;
            anchors.leftMargin: 55;
            checkable: rootModelManager.settings.settingSwitchMirrorCheck;

            indicator: Rectangle {
                width: 38;
                height: 20;
                radius: height / 2;
                color: switchMirror.checked ? switchMirror.checkedColor : "#9e9e9e";
                border.width: 2;
                border.color: switchMirror.checked ? switchMirror.checkedColor : "#E5E5E5";

                Rectangle {
                    property var temp: "value";
                    x: switchMirror.checked ? parent.width - width - 2 : 1;
                    width: switchMirror.checked ? parent.height - 4 : parent.height - 2;
                    height: width;
                    radius: width / 2;
                    anchors.verticalCenter: parent.verticalCenter;
                    color: "white";
                    border.color: "#D5D5D5";

                    Behavior on x {
                        NumberAnimation { duration: 200 }
                    }
                }

                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
//                        if(switchMirror.checked) {
//                            var indicatorWidth = switchMirror.indicator.children[0].width;
//                            switchMirror.indicator.color = "#9e9e9e";
//                            switchMirror.indicator.border.color = "#E5E5E5";
//                            switchMirror.indicator.children[0].x = 0;
//                            switchMirror.indicator.children[0].width = switchMirror.indicator.children[0].parent.height - 4;
//                            switchMirror.checked = false;
//                            rootModelManager.settings.settingSwitchMirrorCheck = switchMirror.checked;
//                            rootModelManager.settings.sync();
//                           Vcam.hflip = false
//                        } else {
//                            switchMirror.indicator.color = switchMirror.checkedColor;
//                            switchMirror.indicator.border.color = switchMirror.checkedColor;
//                            switchMirror.indicator.children[0].x = 20;
//                            switchMirror.indicator.children[0].width = switchMirror.indicator.children[0].parent.height - 2;
//                            switchMirror.checked = true;
//                            rootModelManager.settings.settingSwitchMirrorCheck = switchMirror.checked;
//                           Vcam.hflip = true;
//                        }
                    }
                }
            }
        }

        Text {
            text: switchMirror.checked == true
                    ? Language.get(Lang.Tag.SETTING_PAGE_ON)
                    : Language.get(Lang.Tag.SETTING_PAGE_OFF);
            width: 17;
            height: 16;
            font.pixelSize: 12;
            font.family: Fonts.regular;
            renderType: Text.NativeRendering;//不確定是否blur
            anchors.verticalCenter: parent.verticalCenter;
            anchors.left: switchMirror.right;
            anchors.leftMargin: 2;
        }
    }


    Rectangle {
        id: recAutostart;
        height: 23;
        width: parent.width;
        anchors.top: recMirror.bottom;
        anchors.topMargin: 22.3;

        Rectangle {
            id: txtAutostart;
            width: 350;
            height: children[0].height;
            anchors.verticalCenter: recAutostart.verticalCenter;
            anchors.left: recAutostart.left;
            anchors.leftMargin: 24;
            Text {
                color: "#292929";
                width: 350;
                wrapMode: Text.WordWrap;
                text: Language.get(Lang.Tag.SETTING_AUTO_START);
                font.pixelSize: 15;
                font.family: Fonts.regular;
            }
        }

        Switch {
            id: switchAutostart;
            property color checkedColor: "#094576";
            width: 38;
            height: 20;
            checked: rootModelManager.settings.settingSwitchAutoStartCheck;
            anchors.verticalCenter: recAutostart.verticalCenter;
            anchors.left: txtAutostart.right;
            anchors.leftMargin: 55;
            checkable: rootModelManager.settings.settingSwitchAutoStartCheck;


            indicator: Rectangle {
                width: 38;
                height: 20;
                radius: height / 2;
                color: switchAutostart.checked ? switchAutostart.checkedColor : "#9e9e9e";
                border.width: 2;
                border.color: switchAutostart.checked ? switchAutostart.checkedColor : "#E5E5E5";

                Rectangle {
                    property var temp: "value";
                    x: switchAutostart.checked ? parent.width - width - 2 : 1;
                    width: switchAutostart.checked ? parent.height - 4 : parent.height - 2;
                    height: width;
                    radius: width / 2;
                    anchors.verticalCenter: parent.verticalCenter;
                    color: "white";
                    border.color: "#D5D5D5";

                    Behavior on x {
                        NumberAnimation { duration: 200; }
                    }
                }

                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
//                        if(switchAutostart.checked) {
//                            var indicatorWidth = switchAutostart.indicator.children[0].width;
//                            switchAutostart.indicator.color = "#9e9e9e";
//                            switchAutostart.indicator.border.color = "#E5E5E5";
//                            switchAutostart.indicator.children[0].x = 0;
//                            switchAutostart.indicator.children[0].width = switchAutostart.indicator.children[0].parent.height - 4;
//                            switchAutostart.checked = false;
//                            rootModelManager.settings.settingSwitchAutoStartCheck = switchAutostart.checked;
//                            rootModelManager.settings.sync();
//                            PlatformControl.setAutoStart(false)
//                        } else {
//                            switchAutostart.indicator.color = switchAutostart.checkedColor;
//                            switchAutostart.indicator.border.color = switchAutostart.checkedColor;
//                            switchAutostart.indicator.children[0].x = 20;
//                            switchAutostart.indicator.children[0].width = switchAutostart.indicator.children[0].parent.height - 2;
//                            switchAutostart.checked = true;
//                            rootModelManager.settings.settingSwitchAutoStartCheck = switchAutostart.checked;
//                            PlatformControl.setAutoStart(true)
//                        }
                    }
                }
            }
        }

        Text {
            text: switchAutostart.checked == true
                    ? Language.get(Lang.Tag.SETTING_PAGE_ON)
                    : Language.get(Lang.Tag.SETTING_PAGE_OFF);
            width: 17;
            height: 16;
            font.pixelSize: 12;
            font.family: Fonts.regular;
            renderType: Text.NativeRendering;//不確定是否blur
            anchors.verticalCenter: parent.verticalCenter;
            anchors.left: switchAutostart.right;
            anchors.leftMargin: 2;
        }
    }


    Text {
        id: textAbout;
        color: "#292929";
        text: Language.get(Lang.Tag.SETTING_PAGE_ABOUT);
        opacity: 0.38;
        font.pixelSize: 13;
        font.family: Fonts.bold;
        font.bold: true;
        anchors.top: recAutostart.bottom;
        anchors.topMargin: 16;
        anchors.left: parent.left;
        anchors.leftMargin: 24;
    }

    Rectangle {
        id: recVideoBottom;
        color: "#292929";
        width: parent.width;
        anchors.top: textAbout.bottom;
        anchors.topMargin: 16;
        anchors.left: parent.left;
        anchors.leftMargin: 24;
        anchors.right: parent.right;
        anchors.rightMargin: 24;
        opacity: 0.16;
        height: 1;
    }

    Rectangle {
        id: recAPVersion;
        width: parent.width;
        height: 32;
        anchors.top: recVideoBottom.bottom;
        anchors.topMargin: 16;
        anchors.left: parent.left;
        anchors.leftMargin: 24;
        anchors.right: parent.right;
//        anchors.rightMargin: 24

        Text {
            id: textAPVersion;
            color: "#292929";
            text: Language.get(Lang.Tag.SETTING_PAGE_APP_VERSION_TEXT) + " :";
            font.pixelSize: 15;
            font.family: Fonts.regular;
            anchors.verticalCenter: recAPVersion.verticalCenter;
        }

        Text {
            id: textFixedAPVersion;
            color: "#292929";
            text: Language.get(Lang.Tag.SETTING_PAGE_APP_VERSION_NUMBER);
            font.pixelSize: 15;
            font.family: Fonts.regular;
            anchors.left: textAPVersion.right;
            anchors.leftMargin: 2;
            anchors.verticalCenter: recAPVersion.verticalCenter;
        }

        /*

        // 目前升級還沒支援
        Button {
            id: next
            text: "Upgrade"
            font.pixelSize: 15
            font.family: Fonts.bold
            font.bold: true
            width: 102
            height: 32
            z:4
            anchors {
                left: textFixedAPVersion.right
                leftMargin: 20
                verticalCenter: recAPVersion.verticalCenter
            }

            contentItem: Text {
                text: next.text
                font: next.font
                opacity: enabled ? 1 : 0.3
                color: "#008cff"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            background: Rectangle {

                implicitWidth: 100
                implicitHeight: 40
                opacity: next.down ? 0.3 : 1
                radius: 4
                color: "white"
                border.color: "#008cff"
            }

            onClicked: {
                isOkay = true;
                root.destroy()
            }
        }*/
    }

    Rectangle {
        id: recSupportDescrition;
        width: parent.width;
        height: 20;
        anchors.top: recAPVersion.bottom;
        anchors.topMargin: 12;
        anchors.left: parent.left;
        anchors.leftMargin: 24;
        anchors.right: parent.right;

        Text {
            id: textSupportDescrition;
            color: "#292929";
            text: Language.get(Lang.Tag.SETTING_PAGE_APP_SUPPORT_DESCRITION);
            font.pixelSize: 15;
            font.family: Fonts.regular;
            anchors.top: recSupportDescrition.top;
        }
    }

    Rectangle {
        id: recDescritionOne;
        width: parent.width;
        height: 28;
        anchors.top: recSupportDescrition.bottom;
        anchors.topMargin: 12;
        anchors.left: parent.left;
        anchors.leftMargin: 24;
        anchors.right: parent.right;

        Rectangle {
            id: imgListOne;
            width: 6;
            height: 6;
            radius: height / 2;
            color: "black";
            anchors.left: parent.left;
            anchors.leftMargin: 9;
            anchors.topMargin: 16;
            anchors.verticalCenter: parent.verticalCenter;
        }

        Text {
            id: textDescritionOne;
            color: "#292929";
            text: Language.get(Lang.Tag.SETTING_PAGE_APP_DESCRITION_ONE) + ":";
            font.pixelSize: 12;
            font.family: Fonts.regular;
            anchors.topMargin: 16;
            anchors.left: imgListOne.right;
            anchors.leftMargin: 8;
            anchors.verticalCenter: parent.verticalCenter;
        }

        Button {
            id: btnLink;
            text: "www.vb-cable.com";
            font.pixelSize: 12;
            font.family: Fonts.bold;
            font.bold: true;

            height: 28;
            z:4;
            anchors {
                topMargin: 16;
                leftMargin: 4;
                left: textDescritionOne.right;
            }

            contentItem: Text {
                id: contentItemText;
                text: btnLink.text;
                font: btnLink.font;
                opacity: enabled ? 1 : 0.3;
                color: "#008cff";
                horizontalAlignment: Text.AlignHCenter;
                verticalAlignment: Text.AlignVCenter;
                elide: Text.ElideRight;
            }

            background: Rectangle {
                implicitWidth: contentItemText.width;
                implicitHeight: btnLink.height;
                radius: 2;
                color: btnLink.down ? "#61adfd" : "white";
                border.color: "#008cff";
            }

            onClicked: {
                Qt.openUrlExternally("https://www.vb-cable.com");
            }
        }
    }

    Rectangle {
        id: recDescritionTwo;
        width: parent.width;
        height: 20;
        anchors.top: recDescritionOne.bottom;
        anchors.topMargin: 12;
        anchors.left: parent.left;
        anchors.leftMargin: 24;
        anchors.right: parent.right;


        Rectangle {
            id: imgListTwo;
            width: 6;
            height: 6;
            radius: height / 2;
            color: "black";
            anchors.left: parent.left;
            anchors.leftMargin: 9;
            anchors.topMargin: 16;
            anchors.verticalCenter: parent.verticalCenter;
        }

        Text {
            id: textDescritionTwo;
            color: "#292929";
            text: Language.get(Lang.Tag.SETTING_PAGE_APP_DESCRITION_TWO);
            font.pixelSize: 12;
            font.family: Fonts.regular;
            height: 40;
            width: 440;
            wrapMode: Text.WordWrap;
            anchors.top: parent.top;
            anchors.left: imgListTwo.right;
            anchors.leftMargin: 8;
        }
    }

//    Settings {
//        id: setting;
//        category: 'setting';
//        property bool settingStopStreamWarningCheck: true;
//        property bool settingSwitchStreamWarningCheck: true;
//        property bool settingSwitchMirrorCheck: false;
//        property bool settingSwitchAutoStartCheck: true;
//    }

//    function onStopPushDisableClick(isOkay) {
//        if(isOkay === true) {
//            var indicatorWidth = stopStream.indicator.children[0].width;
//            stopStream.indicator.color = "#9e9e9e";
//            stopStream.indicator.border.color = "#E5E5E5";
//            stopStream.indicator.children[0].x = 0;
//            stopStream.indicator.children[0].width = stopStream.indicator.children[0].parent.height - 4;
//            stopStream.checked = false;
//            rootModelManager.settings.settingStopStreamWarningCheck = stopStream.checked;
//            rootModelManager.settings.sync();
//        }
//    }

//    function onSwitchLowFlowDisableClick(isOkay) {
//        if(isOkay === true) {
//            var indicatorWidth = switchLowFlow.indicator.children[0].width;
//            switchLowFlow.indicator.color = "#9e9e9e";
//            switchLowFlow.indicator.border.color = "#E5E5E5";
//            switchLowFlow.indicator.children[0].x = 0;
//            switchLowFlow.indicator.children[0].width = switchLowFlow.indicator.children[0].parent.height - 4;
//            switchLowFlow.checked = false;
//            rootModelManager.settings.settingSwitchStreamWarningCheck = switchLowFlow.checked;
//            rootModelManager.settings.sync();
//        }

//    }

//    function onSwitchMirrorDisableClick(isOkay) {
//        if(isOkay === true) {
//            var indicatorWidth = switchMirror.indicator.children[0].width;
//            switchMirror.indicator.color = "#9e9e9e";
//            switchMirror.indicator.border.color = "#E5E5E5";
//            switchMirror.indicator.children[0].x = 0;
//            switchMirror.indicator.children[0].width = switchMirror.indicator.children[0].parent.height - 4;
//            switchMirror.checked = false;
//            rootModelManager.settings.settingSwitchMirrorCheck = switchMirror.checked;
//            rootModelManager.settings.sync();
//        }
//    }
}
