import QtQuick 2.0
import QtQuick.Controls 2.14
import QtQuick.Window 2.12;
import "../../utils";

Rectangle {
    id: header;
//    property StackView stack: null;
    color: "#f4f4f4";
    width: root.width;
    height: 64;

    signal onSettingClose()

    Rectangle {
        height: imgBack.height > txtSetting.height? imgBack.height: txtSetting.height;
        width: txtSetting.width + imgBack.width;
        anchors.left: header.left;
        anchors.leftMargin: 8;
        anchors.verticalCenter: parent.verticalCenter;
        color: "transparent";
        z: 10;

        Image {
            id: imgBack;
            height: 32;
            width: 32;
            z: 50;
            anchors.verticalCenter: parent.verticalCenter;
            source: "qrc:/src/images/icon_chevron-left_2021-04-27/icon_chevron-left@3x.png";
        }

        Text {
            id: txtSetting;
            text: Language.get(Lang.Tag.SETTING_PAGE_NAME);
            color: "#292929";
            font.pixelSize: 24;
            font.family: Fonts.bold;
            font.bold: true;

            anchors {
                left: imgBack.right;
                verticalCenter: parent.verticalCenter;
            }
        }

        MouseArea {
            anchors.fill: parent;
            hoverEnabled: true;
            onClicked: {
                stackView.pop();
            }

            onEntered: {
                imgBack.opacity = 0.5;
                txtSetting.opacity = 0.5;
            }

            onExited: {
                imgBack.opacity = 1;
                txtSetting.opacity = 1;
            }

        }
    }
}
