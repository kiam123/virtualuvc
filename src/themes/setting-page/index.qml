import QtQuick 2.14;
import QtQuick.Controls 2.14;

Rectangle {
    id: root;
    height: win.height;
    width: win.width;
//    radius: 2;
//    color: "transparent";
    property var stackView: null;
    property int previousX;
    property int previousY;

    Header {
        id: header;
        height: 64;

        // 讓視窗移動
        MouseArea {
            anchors.fill: parent

            onPressed: {
                previousX = mouseX;
                previousY = mouseY;
            }

            onMouseXChanged: {
                var dx = mouseX - previousX;
                win.setX(win.x + dx);
            }

            onMouseYChanged: {
                var dy = mouseY - previousY;
                win.setY(win.y + dy);
            }
        }
    }

    ScrollView {
        width: root.width;
        height: win.height - 64;
        contentHeight: content.height;

        clip: true;
        ScrollBar.vertical.policy: ScrollBar.AsNeeded;
        anchors.top: header.bottom;

        Content {
            id: content;
            width: root.width;
            height: win.height - 64;
        }
    }
}
