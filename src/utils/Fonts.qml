pragma Singleton
import QtQuick 2.12

QtObject {
    id: fonts;
    readonly property FontLoader fontNotoSansBlack: FontLoader {
        source: fontSource(Fonts.FontType.Black);
    }
    readonly property FontLoader fontNotoSansBold: FontLoader {
        source: fontSource(Fonts.FontType.Bold);
    };
    readonly property FontLoader fontNotoSansLight: FontLoader {
        source: fontSource(Fonts.FontType.Light);
    };
    readonly property FontLoader fontNotoSansMedium: FontLoader {
        source: fontSource(Fonts.FontType.Regular);
    };
    readonly property FontLoader fontNotoSansRegular: FontLoader {
        source: fontSource(Fonts.FontType.Regular);
    };
    readonly property FontLoader fontNotoSansThin: FontLoader {
        source: fontSource(Fonts.FontType.Thin);
    };

    readonly property string black: fonts.fontNotoSansBlack.name;
    readonly property string bold: fonts.fontNotoSansBold.name;
    readonly property string light: fonts.fontNotoSansLight.name;
    readonly property string medium: fonts.fontNotoSansMedium.name;
    readonly property string regular: fonts.fontNotoSansRegular.name;
    readonly property string thin: fonts.fontNotoSansThin.name;

    function fontSource(fontType) {
        if(Qt.locale().name === "zh_CN") {
            switch(fontType) {
                case Fonts.FontType.Black:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_SC/NotoSansSC-Black.otf";
                case Fonts.FontType.Bold:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_SC/NotoSansSC-Bold.otf";
                case Fonts.FontType.Light:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_SC/NotoSansSC-Light.otf";
                case Fonts.FontType.Medium:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_SC/NotoSansSC-Medium.otf";
                case Fonts.FontType.Regular:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_SC/NotoSansSC-Regular.otf";
                case Fonts.FontType.Thin:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_SC/NotoSansSC-Thin.otf";
            }
        } else {
            // 除了簡體字以外的文字
            switch(fontType) {
                case Fonts.FontType.Black:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_TC/NotoSansTC-Black.otf";
                case Fonts.FontType.Bold:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_TC/NotoSansTC-Bold.otf";
                case Fonts.FontType.Light:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_TC/NotoSansTC-Light.otf";
                case Fonts.FontType.Medium:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_TC/NotoSansTC-Medium.otf";
                case Fonts.FontType.Regular:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_TC/NotoSansTC-Regular.otf";
                case Fonts.FontType.Thin:
                    return "qrc:/new/prefix1/src/translate/Noto_Sans_TC/NotoSansTC-Thin.otf";
            }
        }
    }

    enum FontType {
        Black,
        Bold,
        Light,
        Medium,
        Regular,
        Thin
    }
}
