import QtQuick 2.0

Item {
    enum Tag {
        APP_FULL_NAME,
        APP_NAME,

        // common
        ADD_TEXT,
        SAVE_TEXT,
        AUTO_ADD_TEXT,
        MANUAL_ADD_TEXT,
        SETTING_TEXT,
        DEVICE_NAME,
        DEVICE_SUPPORT_NOTIFY,
        NAME,
        IP,
        IP_RULE,
        INVALID_IP_ADDRESS,
        USERNAME,
        USERNAME_TEXT,
        PASSWORD,
        STREAM_KEY,
        MODEL,
        NETWORKINTERFACE,
        NONETWORKINTERFACE,
        UNKNOW,
        NEXT,


        // systemtrayicon
        SYSTEM_TRAY_ICON_QUIT,
        SYSTEM_TRAY_NOTIFICATION_TITLE,
        SYSTEM_TRAY_NOTIFICATION_MESSAGE,
        SYSTEM_TRAY_ICON_VERSION,

        // home page
        HOME_PAGE_BUTTON_ADD_TEXT,
        HOME_PAGE_FOOTER_TEXT,
        HOME_PAGE_DEVICE_NAME_TEXT,
        HOME_PAGE_MODEL_NAME_TEXT,
        HOME_PAGE_FUNCTION_NAME_TEXT,
        HOME_PAGE_NO_DEVICE_NOTIFY,
        HOME_PAGE_IS_PLAYING,

        // search page
        SEARCH_PAGE_DEVICE_NAME_TEXT,
        SEARCH_PAGE_MODEL_NAME_TEXT,
        SEARCH_PAGE_IP_ADDRESS_TEXT,
        SEARCH_PAGE_DESCRITION_ONE,
        SEARCH_PAGE_DESCRITION_TWO,
        SEARCH_PAGE_DESCRITION_THREE,
        SEARCH_PAGE_DESCRITION_FOUR,
        SEARCH_PAGE_DESCRITION_FIVE,
        SEARCH_PAGE_DESCRITION_SIX,

        // Setings page
        SETTING_PAGE_NAME,
        SETTING_PAGE_STOP_WARNING,
        SETTING_PAGE_EXCHANGE_WARNING,
        SETTING_PAGE_FLIP,
        SETTING_AUTO_START,
        SETTING_PAGE_ABOUT,
        SETTING_PAGE_ON,
        SETTING_PAGE_OFF,
        SETTING_PAGE_APP_VERSION_TEXT,
        SETTING_PAGE_APP_VERSION_NUMBER,
        SETTING_PAGE_APP_SUPPORT_DESCRITION,
        SETTING_PAGE_APP_DESCRITION_ONE,
        SETTING_PAGE_APP_DESCRITION_TWO,

        // Save Window
        SAVE_WINDOW_SUCCESS_TEXT,

        // Connect Device Window
        CONNECT_DEVICE_WINDOW_CONNECT,

        // ExchangeWarningDialog.qml - 3.1_warning_ExchangUVC
        ECHANGE_WARNING_TITLE,
        ECHANGE_WARNING_MESSAGE,
        ECHANGE_WARNING_CHECK_BOX_MESSAGE,
        ECHANGE_WARNING_CANCEL,
        ECHANGE_WARNING_OK_TEXT,

        // StopWarningDialog.qml - 3.1_warning_StopUVC
        STOP_WARNING_TITLE,
        STOP_WARNING_MESSAGE,
        STOP_WARNING_CHECK_BOX_MESSAGE,
        STOP_WARNING_CANCEL,
        STOP_WARNING_OK_TEXT,

        // DelWarningDialog.qml - 3.1_warning_RemoveDevice
        REMOVE_WARNING_TITLE,
        REMOVE_WARNING_MESSAGE,  // 還沒翻譯
        REMOVE_WARNING_CANCEL,
        REMOVE_WARNING_OK_TEXT,

        // DisconnectIPDialog.qml - Dialog IP disconnect – 2
        DISCONNECT_WARNING_TITLE,  // 還沒翻譯
        DISCONNECT_WARNING_MESSAGE,
        DISCONNECT_WARNING_OK_TEXT,


        // DeviceModelErrorDialog.qml - Dialog IP disconnect – 3
        MODEL_WARNING_TITLE,
        MODEL_WARNING_MESSAGE,
        MODEL_WARNING_OK_TEXT,

        // AccountIncorrectDialog.qml - Dialog Account inconnect
        LOGIN_WARNING_TITLE,
        LOGIN_WARNING_MESSAGE,
        LOGIN_WARNING_OK_TEXT,

        //  QuitDialog.qml
        QUIT_WARNING_TITLE,  // 還沒翻譯
        QUIT_WARNING_MESSAGE,  // 還沒翻譯
        QUIT_WARNING_CANCEL,
        QUIT_WARNING_OK_TEXT,

        // ConnectVCamErrorDialog.qml
        CONNECT_VCAM_ERROR_WARNING_TITLE,// 還沒翻譯
        CONNECT_VCAM_ERROR_WARNING_MESSAGE,// 還沒翻譯
        CONNECT_VCAM_ERROR_WARNING_OK_TEXT,// 還沒翻譯

        TOOLTIP_COPY,
        TOOLTIP_REFRESH,
        TOOLTIP_HIDDEN_PASSWORD,
        TOOLTIP_SHOW_PASSWORD,  // 還沒翻譯
        TOOLTIP_PREV,
        TOOLTIP_STREAM_DEVICE_SETTING,
        TOOLTIP_SETTINGS,
        TOOLTIP_DELETE,
        TOOLTIP_CLOSE,
        TOOLTIP_MINIMIZED, // 還沒翻譯
        TOOLTIP_PLAY,
        TOOLTIP_STOP
    }
}
