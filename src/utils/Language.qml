pragma Singleton
import QtQuick 2.14

QtObject {
    // qsTr可以將所有文字export出來，可以用作送翻譯的方式
    // qsTr可以取得多國語言的翻譯後的文字
    function get(command) {
        switch(command) {
            case Lang.Tag.APP_FULL_NAME:
                return Qt.application.name;
            case Lang.Tag.APP_NAME:
                return "Stream to USB Capture";

            case Lang.Tag.ADD_TEXT:
                return qsTr("Add");
            case Lang.Tag.SAVE_TEXT:
                return qsTr("Save");
            case Lang.Tag.AUTO_ADD_TEXT:
                return qsTr("Add a Stream Device Automatically");
            case Lang.Tag.MANUAL_ADD_TEXT:
                return qsTr("Add a Stream Device Manually");
            case Lang.Tag.SETTING_TEXT:
                return qsTr("Stream Device Settings");

            case Lang.Tag.DEVICE_SUPPORT_NOTIFY:
                return qsTr("Make sure your PC and device are connected in the same network environment. Only ATEN UC9020 / UC9040 are supported.");
            case Lang.Tag.DEVICE_NAME:
                return qsTr("Device Name");
            case Lang.Tag.NAME:
                return qsTr("Name");
            case Lang.Tag.IP:
                return qsTr("IP Address");
            case Lang.Tag.IP_RULE:
                return qsTr("Please input number or .");
            case Lang.Tag.INVALID_IP_ADDRESS:
                return qsTr("Invalid IP Address");
            case Lang.Tag.USERNAME:
                return qsTr("Username");
            case Lang.Tag.USERNAME_TEXT:
                return "administrator";
            case Lang.Tag.PASSWORD:
                return qsTr("Password");
            case Lang.Tag.STREAM_KEY:
                return qsTr("Stream Key");
            case Lang.Tag.MODEL:
                return qsTr("Model");
            case Lang.Tag.NETWORKINTERFACE:
                return qsTr("Network Adapter");
            case Lang.Tag.NONETWORKINTERFACE:
                return qsTr("Network Adapter Not Found");
            case Lang.Tag.UNKNOW:
                return qsTr("network adapter unknown");
            case Lang.Tag.NEXT:
                return qsTr("Next");

            case Lang.Tag.SYSTEM_TRAY_ICON_QUIT:
                return qsTr("Quit");
            case Lang.Tag.SYSTEM_TRAY_NOTIFICATION_TITLE:
                return qsTr("Application is still running");
            case Lang.Tag.SYSTEM_TRAY_NOTIFICATION_MESSAGE:
                return qsTr("ATEN Stream to USB Capture Application is still running");
            case Lang.Tag.SYSTEM_TRAY_ICON_VERSION:
                return qsTr("Ver");

            case Lang.Tag.HOME_PAGE_BUTTON_ADD_TEXT:
                return qsTr("New Stream");
            case Lang.Tag.HOME_PAGE_FOOTER_TEXT:
                return qsTr("ATEN International Co., Ltd. All rights reserved.");
            case Lang.Tag.HOME_PAGE_DEVICE_NAME_TEXT:
                return qsTr("Name");
            case Lang.Tag.HOME_PAGE_MODEL_NAME_TEXT:
                return qsTr("Model");
            case Lang.Tag.HOME_PAGE_FUNCTION_NAME_TEXT:
                return qsTr("Function");
            case Lang.Tag.HOME_PAGE_NO_DEVICE_NOTIFY:
                return qsTr("Click \"+ New Stream\" to find the steam device");
            case Lang.Tag.HOME_PAGE_IS_PLAYING:
                return qsTr("is playing");

            case Lang.Tag.SEARCH_PAGE_DEVICE_NAME_TEXT:
                return qsTr("Device Name");
            case Lang.Tag.SEARCH_PAGE_MODEL_NAME_TEXT:
                return qsTr("Model");
            case Lang.Tag.SEARCH_PAGE_IP_ADDRESS_TEXT:
                return qsTr("IP Address");
            case Lang.Tag.SEARCH_PAGE_DESCRITION_ONE:
                return qsTr("Sorry, no stream device is found.");
            case Lang.Tag.SEARCH_PAGE_DESCRITION_TWO:
                return qsTr("Please check the following conditions.");
            case Lang.Tag.SEARCH_PAGE_DESCRITION_THREE:
                return qsTr("Make sure your device is powered.");
            case Lang.Tag.SEARCH_PAGE_DESCRITION_FOUR:
                return qsTr("Make sure your PC and device are connected in the same network environment.");
            case Lang.Tag.SEARCH_PAGE_DESCRITION_FIVE:
                return qsTr("Only ATEN UC9020 / UC9040 are supported.");
            case Lang.Tag.SEARCH_PAGE_DESCRITION_SIX:
                return qsTr("If you are still having trouble connecting, please contact our technical support.");

            case Lang.Tag.SETTING_PAGE_NAME:
                return qsTr("Settings");
            case Lang.Tag.SETTING_PAGE_STOP_WARNING:
                return qsTr("Stop Stream Warning Message");
            case Lang.Tag.SETTING_PAGE_EXCHANGE_WARNING:
                return qsTr("Switch Stream Warning Message");
            case Lang.Tag.SETTING_PAGE_FLIP:
                return qsTr("Mirror Stream Video");
            case Lang.Tag.SETTING_AUTO_START:
                return qsTr("Auto Start the Application");
            case Lang.Tag.SETTING_PAGE_ABOUT:
                return qsTr("About");
            case Lang.Tag.SETTING_PAGE_ON:
                return qsTr("On");
            case Lang.Tag.SETTING_PAGE_OFF:
                return qsTr("Off");
            case Lang.Tag.SETTING_PAGE_APP_VERSION_TEXT:
                return qsTr("APP Version");
            case Lang.Tag.SETTING_PAGE_APP_VERSION_NUMBER:
                return Qt.application.version;
            case Lang.Tag.SETTING_PAGE_APP_SUPPORT_DESCRITION:
                return qsTr("This application is supported by VB-CABLE.");
            case Lang.Tag.SETTING_PAGE_APP_DESCRITION_ONE:
                return qsTr("The origin of VB-CABLE");
            case Lang.Tag.SETTING_PAGE_APP_DESCRITION_TWO:
                return qsTr("VB-CABLE is a donationware, all participations are welcome.");

            case Lang.Tag.SAVE_WINDOW_SUCCESS_TEXT:
                return qsTr("Successful");

            case Lang.Tag.CONNECT_DEVICE_WINDOW_CONNECT:
                return qsTr("Connecting with the Stream Device...");

            // ExchangeWarningDialog.qml - 3.1_warning_ExchangUVC
            case Lang.Tag.ECHANGE_WARNING_TITLE:
                return qsTr("Play another Stream");
            case Lang.Tag.ECHANGE_WARNING_MESSAGE:
                return qsTr("Are you sure you want to stop this current stream and start a new one?");
            case Lang.Tag.ECHANGE_WARNING_CHECK_BOX_MESSAGE:
                return qsTr("Do not show this message again.");
            case Lang.Tag.ECHANGE_WARNING_CANCEL:
                return qsTr("Cancel");
            case Lang.Tag.ECHANGE_WARNING_OK_TEXT:
                return qsTr("Play the New One");

            // StopWarningDialog.qml - 3.1_warning_StopUVC
            case Lang.Tag.STOP_WARNING_TITLE:
                return qsTr("Stop the Stream");
            case Lang.Tag.STOP_WARNING_MESSAGE:
                return qsTr("Are you sure you want to stop this stream?");
            case Lang.Tag.STOP_WARNING_CHECK_BOX_MESSAGE:
                return qsTr("Do not show this message again.");
            case Lang.Tag.STOP_WARNING_CANCEL:
                return qsTr("Cancel");
            case Lang.Tag.STOP_WARNING_OK_TEXT:
                return qsTr("Stop");

            // DelWarningDialog.qml - 3.1_warning_RemoveDevice
            case Lang.Tag.REMOVE_WARNING_TITLE:
                return qsTr("Remove the Stream Device");
            case Lang.Tag.REMOVE_WARNING_MESSAGE:
                return qsTr("Are you sure you want to remove this stream device?");
            case Lang.Tag.REMOVE_WARNING_CANCEL:
                return qsTr("Cancel");
            case Lang.Tag.REMOVE_WARNING_OK_TEXT:
                return qsTr("Remove");

            // DisconnectIPDialog.qml - Dialog IP disconnect – 2
            case Lang.Tag.DISCONNECT_WARNING_TITLE:
                return qsTr("Can't Find the Stream Device");
            case Lang.Tag.DISCONNECT_WARNING_MESSAGE:
                return qsTr("Please check the IP address and the device connection.");
            case Lang.Tag.DISCONNECT_WARNING_OK_TEXT:
                return qsTr("OK");

            // DeviceModelErrorDialog.qml - Dialog IP disconnect – 3
            case Lang.Tag.MODEL_WARNING_TITLE:
                return qsTr("Can't Connect with the Device");
            case Lang.Tag.MODEL_WARNING_MESSAGE:
                return qsTr("Only ATEN UC9020 / UC9040 are supported.");
            case Lang.Tag.MODEL_WARNING_OK_TEXT:
                return qsTr("OK");

            // AccountIncorrectDialog.qml - Dialog Account inconnect
            case Lang.Tag.LOGIN_WARNING_TITLE:
                return qsTr("Login Error");
            case Lang.Tag.LOGIN_WARNING_MESSAGE:
                return qsTr("The Username or the Password is incorrect.");
            case Lang.Tag.LOGIN_WARNING_OK_TEXT:
                return qsTr("OK");

            case Lang.Tag.QUIT_WARNING_TITLE:
                return qsTr("Quit");
            case Lang.Tag.QUIT_WARNING_MESSAGE:
                return qsTr("You are still streaming, are you sure you want to quit?");
            case Lang.Tag.QUIT_WARNING_CANCEL:
                return qsTr("Cancel");
            case Lang.Tag.QUIT_WARNING_OK_TEXT:
                return qsTr("Quit");


            case Lang.Tag.CONNECT_VCAM_ERROR_WARNING_TITLE:
                return qsTr("Connection Error");
            case Lang.Tag.CONNECT_VCAM_ERROR_WARNING_MESSAGE:
                return qsTr("Please check the connection of the device.");
            case Lang.Tag.CONNECT_VCAM_ERROR_WARNING_OK_TEXT:
                return qsTr("OK");


            case Lang.Tag.TOOLTIP_COPY:
                return qsTr("Copy");
            case Lang.Tag.TOOLTIP_REFRESH:
                return qsTr("Refresh");
            case Lang.Tag.TOOLTIP_HIDDEN_PASSWORD:
                return qsTr("Hidden Password");
            case Lang.Tag.TOOLTIP_SHOW_PASSWORD:
                return qsTr("Show Password");
            case Lang.Tag.TOOLTIP_PREV:
                return qsTr("Preview");
            case Lang.Tag.TOOLTIP_STREAM_DEVICE_SETTING:
                return qsTr("Stream Device Settings");
            case Lang.Tag.TOOLTIP_SETTINGS:
                return qsTr("Settings");
            case Lang.Tag.TOOLTIP_DELETE:
                return qsTr("Remove");
            case Lang.Tag.TOOLTIP_CLOSE:
                return qsTr("Close");
            case Lang.Tag.TOOLTIP_MINIMIZED:
                return qsTr("Minimized");
            case Lang.Tag.TOOLTIP_PLAY:
                return qsTr("Start");
            case Lang.Tag.TOOLTIP_STOP:
                return qsTr("Stop");
        }
    }
}

