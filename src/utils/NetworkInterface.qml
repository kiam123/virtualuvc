pragma Singleton
import QtQuick 2.0

Item {
    function isSameIPV4Address(IPV4Address, targetIPV4Address) {
        return IPV4Address === targetIPV4Address;
    }
}
