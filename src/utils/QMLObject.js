function QMLObject() { }

QMLObject.prototype.createQMLObject = function(componentFile, container, componentProperty = null) {
    let tmpComponent = Qt.createComponent(componentFile);
    let object = null;

    if(typeof(componentProperty) === "undefined" || componentProperty === null) {
        componentProperty = {};
    }
    if(tmpComponent.status === Component.Ready) {
        object = tmpComponent.createObject(container, componentProperty);
    }

    if(object === null) {
        console.log(tmpComponent.errorString());
    }
    console.log("create: "+ object);
    return object;
}
