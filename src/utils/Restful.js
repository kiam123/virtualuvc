function Http() {
    this.http = new XMLHttpRequest();
}


// GET
Http.prototype.get = function(url, success, failure) {
    console.log("get.......");
    this.http.open("GET", url, false);// <request type, url, async>
    this.http.onreadystatechange = function() {
        this.handleResponse(this.http, success, failure);
    }.bind(this)
    this.http.send();
}

// POST
Http.prototype.post = function(url, arg, success, failure) {
    this.http.open("POST", url, false); // <request type, url, async>
    this.http.setRequestHeader("Content-Type", "application/json");
    this.http.setRequestHeader("Content-Length", arg.length);
    this.http.onreadystatechange = function() {
        this.handleResponse(this.http, success, failure);
    }.bind(this);
    console.log("send...")
    this.http.send(arg);
}

// PATCH
Http.prototype.patch = function(url, arg, success, failure) {
    this.http.open("PATCH", url, false);// <request type, url, async>
    this.http.setRequestHeader("Content-Type", "application/json");
    this.http.setRequestHeader("Content-Length", arg.length);
    this.http.onreadystatechange = function() {
        console.log("send patch...")
        this.handleResponse(this.http, success, failure);
    }.bind(this);
    this.http.send(arg);
}


// 處理返回值
Http.prototype.handleResponse = function(http, success, failure) {
    if (http.readyState === XMLHttpRequest.DONE) {
        if(http.status ===  200) {
            if (success !== null && success !== undefined) {
                success(http.responseText, http.status);
            }
        }
        else {
            if (failure !== null && failure !== undefined) {
                failure(http.responseText, http.status);
            }
        }
    }
}

/*將json物件轉成字串*/
Http.prototype.urlQuery = function(jsonObject) {
    var query = "";
    var i = 0;
    for(var iter in jsonObject) {

        if(i>0) {
            query += "&";
        }
        if (Array.isArray(jsonObject[iter]))
        {
            query += iter +"=" + "[" +encodeURI(jsonObject[iter]) + "]";
        }
        else
        {
            query += iter +"=" + encodeURI(jsonObject[iter]);
        }

        //console.log("item1", iter,query)
        i++;
    }
    //console.log("url query:", query);
    return query;
}
